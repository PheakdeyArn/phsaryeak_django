-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: eshop_local_mysql_prod:3306
-- Generation Time: Nov 09, 2021 at 05:43 PM
-- Server version: 10.5.5-MariaDB-1:10.5.5+maria~focal
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshop_local`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_mobile` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `state` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `name`, `type`, `mobile`, `alternate_mobile`, `address`, `landmark`, `area_id`, `city_id`, `pincode`, `country_code`, `state`, `country`, `latitude`, `longitude`, `is_default`) VALUES
(14, 4, 'Voathnak', 'Home', '12979873', NULL, 'Borei Keila Building D, Borei Keila Building D, Khan 7 Makara, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.565776868794384', '104.90972768515348', 1),
(18, 3, 'Hongna', 'Home', '087914999', NULL, 'Test', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5550538', '104.9120683', 1),
(25, 7, 'pheakdey', 'Home', '085896069', NULL, 'Phnom Penh', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.516484866556969', '104.89413667470217', 1),
(30, 13, 'Kevin', 'Office', '967988599', NULL, 'pp', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5548155', '104.91180359999998', 1),
(47, 11, 'chithav', 'Office', '11922024', NULL, 'Building D, Building D, Khan Tuol Kouk, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.569522040500877', '104.88987162709236', 1),
(50, 16, 'odinson', 'Home', '85896069', NULL, '71, 71 St 318, Khan Chamkar Mon, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.555608616244001', '104.90659318864346', 0),
(52, 16, 'odinson', 'Office', '85896069', NULL, 'Saint 173, #1E0, Khan Chamkar Mon, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.555156955426488', '104.91212122142315', 0),
(54, 11, 'chithav', 'Office', '11922024', NULL, 'Saint 173, #1E0, Khan Chamkar Mon, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.55515498454133', '104.91213764995337', 0),
(55, 10, 'Rethy', 'Home', '16775063', NULL, '376, 376 St 193, Khan Chamkar Mon, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.55307437904086', '104.91156734526157', 0),
(59, 17, 'Hongna', 'Home', '87914999', NULL, '313, 313 Preah Sihanouk Blvd (274), Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 12253, 0, 'Phnom Penh', 'Cambodia', '11.5556871', '104.9122218', 1),
(61, 18, 'Lin', 'Home', '316000072', NULL, '500, 534 Kampuchea Krom Blvd (128), Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5676877', '104.9064347', 1),
(62, 10, 'Rethy', 'Home', '16775063', NULL, '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.55736400962833', '104.91842810064554', 1),
(63, 11, 'chithav', 'Home', '11922024', NULL, 'Unnamed Road, Unnamed Road, Khan Sensok, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.568883505735437', '104.88001350313425', 0),
(64, 19, 'Ded Ses', 'Home', '85949242', NULL, '804, 804 Kampuchea Krom Blvd (128), Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5659457', '104.88906480000003', 1),
(69, 2, 'Chhenglin2', 'Home', '89243003', NULL, 'Street 122, Street 122, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.56804099235753', '104.90606881678106', 1),
(71, 16, 'odinson', 'Home', '85896069', NULL, '1058, 1058 Huff Ave, , Mountain View, California, United States', NULL, 0, 0, 94043, 0, 'California', 'United States', '37.41868571646587', '-122.08339937031268', 1),
(73, 21, 'Hong', 'Home', '69850168', NULL, 'Street P-14, No.132, Khan Chbar Ampov, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5284388', '104.95956990000002', 1),
(74, 21, 'Hong', 'Home', '69850168', NULL, 'Gg', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5284608', '104.9595802', 0),
(75, 5, 'Chingching', 'Home', '69860168', NULL, 'Street P-10B, Street P-10B, Khan Chbar Ampov, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5284728', '104.9595884', 1),
(76, 5, 'Chingching', 'Home', '69860168', NULL, 'Street P-10B, Street P-10B, Khan Chbar Ampov, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5284692', '104.959589', 0),
(77, 22, 'Likun', 'Home', '963885245', NULL, 'Phnom Penh, Borey Piphop Thmey, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5557849', '104.88804970000001', 1),
(78, 10, 'Rethy ', 'Home', '16775063', NULL, '145, 145 St 146, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5648081', '104.8972449', 0),
(79, 10, 'Rethy ', 'Home', '16775063', NULL, '145, 145 St 146, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.5648168', '104.8972394', 0),
(80, 23, 'hongna', 'Home', '99914999', NULL, 'Street 173, Street 173, , Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.555193572165926', '104.9120754731789', 1),
(81, 24, 'Kun', 'Office', '70905565', NULL, 'Khan Tuol Kouk, , Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia', NULL, 0, 0, 0, 0, 'Phnom Penh', 'Cambodia', '11.555845859806876', '104.88801739810647', 1);

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `minimum_free_delivery_order_amount` double NOT NULL DEFAULT 100,
  `delivery_charges` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `city_id`, `minimum_free_delivery_order_amount`, `delivery_charges`) VALUES
(1, 'Angkor', 1, 1000000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `attribute_set_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `attribute_set_id`, `name`, `type`, `date_created`, `status`) VALUES
(1, 1, 'asd', '1', '2021-08-04 23:10:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_set`
--

CREATE TABLE `attribute_set` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `filterable` int(11) DEFAULT 0,
  `value` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `attribute_id`, `filterable`, `value`, `status`) VALUES
(2, 1, 0, '111', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add categories', 7, 'add_categories'),
(26, 'Can change categories', 7, 'change_categories'),
(27, 'Can delete categories', 7, 'delete_categories'),
(28, 'Can view categories', 7, 'view_categories'),
(29, 'Can add product', 8, 'add_product'),
(30, 'Can change product', 8, 'change_product'),
(31, 'Can delete product', 8, 'delete_product'),
(32, 'Can view product', 8, 'view_product'),
(33, 'Can add product variant', 9, 'add_productvariant'),
(34, 'Can change product variant', 9, 'change_productvariant'),
(35, 'Can delete product variant', 9, 'delete_productvariant'),
(36, 'Can view product variant', 9, 'view_productvariant');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$260000$iRr2FrGm0VhW0EAimmFgNt$gPTGHuE4mmmKhXNdd5zfBPJwhz3VLeupDzdKdTa34y0=', '2021-06-18 06:07:46.965459', 1, 'vlim', '', '', 'voathnak.lim@gmail.com', 1, 1, '2021-06-18 05:59:41.302029');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `is_saved_for_later` int(11) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_variant_id`, `qty`, `is_saved_for_later`, `date_created`) VALUES
(6, 6, 223, 1, 0, '2021-06-20 14:57:29'),
(12, 2, 245, 0, 0, '2021-06-22 04:24:18'),
(13, 2, 133, 0, 0, '2021-06-22 08:37:02'),
(14, 2, 137, 0, 0, '2021-06-22 08:37:42'),
(17, 2, 252, 0, 0, '2021-06-23 03:06:41'),
(18, 3, 253, 1, 0, '2021-06-23 03:14:24'),
(19, 3, 137, 1, 0, '2021-06-23 03:43:23'),
(20, 3, 98, 1, 0, '2021-06-23 03:44:41'),
(21, 3, 252, 1, 0, '2021-06-23 03:45:37'),
(22, 3, 239, 1, 0, '2021-06-23 03:45:54'),
(23, 3, 254, 1, 0, '2021-06-23 03:47:06'),
(24, 3, 118, 1, 0, '2021-06-23 03:51:37'),
(25, 2, 254, 0, 0, '2021-06-23 03:53:05'),
(26, 2, 253, 3, 0, '2021-06-23 03:57:05'),
(27, 2, 1, 0, 0, '2021-06-23 04:02:52'),
(28, 7, 254, 1, 0, '2021-06-23 04:18:27'),
(30, 7, 253, 0, 0, '2021-06-23 04:21:59'),
(31, 7, 173, 0, 0, '2021-06-23 04:22:31'),
(32, 7, 137, 0, 0, '2021-06-26 04:33:46'),
(33, 7, 250, 0, 0, '2021-06-26 04:43:31'),
(34, 7, 135, 0, 0, '2021-06-26 04:54:50'),
(35, 7, 133, 0, 0, '2021-06-26 06:59:15'),
(39, 7, 199, 0, 0, '2021-06-27 03:54:03'),
(40, 5, 380, 0, 0, '2021-06-27 14:25:42'),
(41, 5, 252, 1, 0, '2021-06-27 14:55:52'),
(42, 5, 103, 0, 0, '2021-06-27 14:56:19'),
(43, 5, 141, 0, 0, '2021-06-27 14:58:34'),
(44, 7, 220, 3, 0, '2021-07-03 07:09:35'),
(45, 8, 252, 0, 0, '2021-07-04 15:29:02'),
(48, 10, 142, 0, 0, '2021-07-07 04:42:59'),
(49, 10, 208, 0, 0, '2021-07-07 04:44:41'),
(50, 10, 141, 0, 0, '2021-07-07 05:20:56'),
(51, 10, 203, 0, 0, '2021-07-07 05:24:43'),
(52, 10, 250, 0, 0, '2021-07-07 05:55:19'),
(64, 10, 252, 1, 0, '2021-07-10 03:50:01'),
(77, 11, 199, 0, 0, '2021-07-10 09:50:44'),
(80, 3, 199, 1, 0, '2021-07-14 05:51:06'),
(81, 16, 141, 0, 0, '2021-07-18 05:18:23'),
(83, 2, 2, 2, 0, '2021-07-18 14:23:01'),
(84, 5, 382, 1, 0, '2021-07-18 14:27:30'),
(89, 10, 2, 1, 0, '2021-07-19 02:17:12'),
(93, 11, 397, 0, 0, '2021-07-24 04:27:07'),
(95, 10, 380, 1, 0, '2021-07-26 08:09:51'),
(96, 10, 361, 0, 0, '2021-07-26 08:10:27'),
(97, 10, 9, 0, 0, '2021-07-27 09:43:07'),
(98, 19, 3, 0, 0, '2021-07-29 01:56:47'),
(99, 19, 6, 0, 0, '2021-07-29 01:57:25'),
(100, 19, 18, 0, 0, '2021-07-29 01:57:31'),
(101, 19, 2, 0, 0, '2021-07-29 01:57:44'),
(102, 10, 403, 0, 0, '2021-07-31 02:48:50'),
(108, 2, 374, 0, 0, '2021-08-08 15:42:55'),
(109, 10, 374, 0, 0, '2021-08-28 06:24:01'),
(113, 10, 363, 0, 0, '2021-08-31 09:38:34'),
(115, 10, 427, 1, 0, '2021-08-31 09:47:57'),
(116, 16, 348, 0, 0, '2021-09-01 04:14:01'),
(117, 16, 357, 0, 0, '2021-09-01 08:40:27'),
(119, 10, 426, 1, 0, '2021-09-01 14:38:48'),
(120, 10, 5, 0, 0, '2021-09-01 15:00:18'),
(121, 10, 10, 0, 0, '2021-09-01 15:04:52'),
(124, 16, 369, 1, 1, '2021-09-04 07:05:45'),
(126, 11, 3, 3, 1, '2021-09-06 02:25:15'),
(127, 11, 385, 0, 0, '2021-09-06 02:28:29'),
(129, 11, 363, 0, 0, '2021-09-08 08:13:04'),
(130, 11, 325, 1, 0, '2021-09-08 08:13:36'),
(131, 16, 388, 1, 0, '2021-09-11 06:51:24'),
(137, 11, 398, 0, 0, '2021-09-12 07:44:22'),
(138, 16, 394, 0, 0, '2021-09-12 07:50:45'),
(139, 16, 394, 0, 0, '2021-09-12 07:50:45'),
(140, 16, 260, 0, 0, '2021-09-12 07:52:05'),
(142, 10, 402, 0, 0, '2021-09-15 09:44:07'),
(143, 10, 399, 0, 0, '2021-09-15 09:44:12'),
(144, 11, 369, 1, 0, '2021-09-16 06:28:30'),
(145, 10, 400, 0, 0, '2021-09-18 01:19:56'),
(146, 10, 393, 0, 0, '2021-09-18 01:20:17'),
(148, 10, 357, 0, 0, '2021-09-21 03:00:09'),
(149, 10, 6, 0, 0, '2021-09-21 03:00:12'),
(155, 22, 277, 1, 0, '2021-09-25 12:13:57'),
(176, 10, 369, 1, 0, '2021-10-17 02:17:34'),
(183, 2, 369, 1, 0, '2021-10-25 10:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `slug` varchar(20) DEFAULT NULL,
  `banner` varchar(500) DEFAULT NULL,
  `row_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `banner`, `row_order`, `status`, `parent_id`, `image`) VALUES
(1, 'Comics Book', 'comics-book-1', 'uploads/media/2021/Comic_book_(4).png', 0, 1, NULL, 'uploads/media/2021/Comic_book_(4).png'),
(2, 'Kid Book (2-5 Ages)', 'kid-book-2-5-ages', 'uploads/media/2021/215.png', 1, 1, NULL, 'uploads/media/2021/215.png'),
(3, 'Knowledge', 'knowledge', 'uploads/media/2021/Knowledge.png', 2, 1, NULL, 'uploads/media/2021/Knowledge.png'),
(4, 'Light Story', 'light-story', 'uploads/media/2021/54.png', 3, 1, NULL, 'uploads/media/2021/54.png'),
(5, 'Fantasy, Adult & Fiction', 'fantasy-adult-fictio', 'uploads/media/2021/Fiction.png', 0, 1, NULL, 'uploads/media/2021/Fiction.png'),
(6, 'Early Reader', 'early-reader', 'uploads/media/2021/Early_Reader_(8).png', 0, 1, NULL, 'uploads/media/2021/Early_Reader_(8).png'),
(7, 'Fun Toy', 'fun-toy-1', 'uploads/media/2021/214.png', 4, 0, NULL, 'uploads/media/2021/214.png'),
(8, 'DIY', 'diy', 'uploads/media/2021/53.png', 5, 0, NULL, 'uploads/media/2021/53.png'),
(9, 'Creative Toy', 'creative-toy', 'uploads/media/2021/Wow_revised_(1).png', 0, 0, NULL, 'uploads/media/2021/Wow_revised_(1).png'),
(10, 'Girl Toy', 'girl-toy-1', 'uploads/media/2021/Wow_revised_(3).png', 6, 0, NULL, 'uploads/media/2021/Wow_revised_(3).png'),
(11, 'DIY', 'diy-1', '', 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(12, 'Dancing', 'Dancing', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(13, 'Painting', 'Painting', NULL, 0, 0, 9, 'uploads/media/2021/testingjan.jpg'),
(14, 'Wooden', 'wooden', 'uploads/media/2021/63.png', 7, 0, NULL, 'uploads/media/2021/63.png'),
(15, 'Learning', 'Learning', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(16, 'Sport', 'Sport', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(17, 'Stem', 'Stem', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(18, 'Sample', 'Sample', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(19, 'Dig', 'Dig', NULL, 0, 0, 9, 'uploads/media/2021/testingjan.jpg'),
(20, 'Book', 'book-1', 'uploads/media/2021/16.png', 8, 0, NULL, 'uploads/media/2021/16.png'),
(21, 'Fun Toy 2', 'Fun Toy 2', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(22, 'Learning 2', 'Learning 2', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(23, 'STEM', 'stem-1', 'uploads/media/2021/45.png', 9, 0, NULL, 'uploads/media/2021/45.png'),
(24, 'Wooden 2', 'Wooden 2', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(25, 'Other 2', 'Other 2', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(26, 'Painting', 'Painting', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg'),
(27, 'Dig', 'Dig', NULL, 0, 0, NULL, 'uploads/media/2021/testingjan.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Siem Reap'),
(2, 'Battambang'),
(3, 'Phnom Penh');

-- --------------------------------------------------------

--
-- Table structure for table `client_api_keys`
--

CREATE TABLE `client_api_keys` (
  `id` int(11) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_api_keys`
--

INSERT INTO `client_api_keys` (`id`, `name`, `secret`, `status`) VALUES
(1, 'Jaydeep Goswami', '65c9cd19cd138f19ddf2f6320c7a802ee936c548', 1),
(3, 'voathnak lim', '4d537f7a9e7a1211fb25484addbd6052102d8f3f', 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_boy_notifications`
--

CREATE TABLE `delivery_boy_notifications` (
  `id` int(11) NOT NULL,
  `delivery_boy_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(7, 'eshop', 'categories'),
(8, 'eshop', 'product'),
(9, 'eshop', 'productvariant'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2021-06-18 05:56:01.899297'),
(2, 'auth', '0001_initial', '2021-06-18 05:56:02.492409'),
(3, 'admin', '0001_initial', '2021-06-18 05:56:02.925180'),
(4, 'admin', '0002_logentry_remove_auto_add', '2021-06-18 05:56:02.966578'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2021-06-18 05:56:02.985728'),
(6, 'contenttypes', '0002_remove_content_type_name', '2021-06-18 05:56:03.041349'),
(7, 'auth', '0002_alter_permission_name_max_length', '2021-06-18 05:56:03.085346'),
(8, 'auth', '0003_alter_user_email_max_length', '2021-06-18 05:56:03.109904'),
(9, 'auth', '0004_alter_user_username_opts', '2021-06-18 05:56:03.121840'),
(10, 'auth', '0005_alter_user_last_login_null', '2021-06-18 05:56:03.156546'),
(11, 'auth', '0006_require_contenttypes_0002', '2021-06-18 05:56:03.161840'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2021-06-18 05:56:03.176364'),
(13, 'auth', '0008_alter_user_username_max_length', '2021-06-18 05:56:03.199570'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2021-06-18 05:56:03.219399'),
(15, 'auth', '0010_alter_group_name_max_length', '2021-06-18 05:56:03.241440'),
(16, 'auth', '0011_update_proxy_permissions', '2021-06-18 05:56:03.257467'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2021-06-18 05:56:03.282995'),
(18, 'eshop', '0001_initial', '2021-06-18 05:56:03.471440'),
(19, 'eshop', '0002_auto_20210523_1102', '2021-06-18 05:56:03.633307'),
(20, 'eshop', '0003_auto_20210523_1333', '2021-06-18 05:56:03.979047'),
(21, 'eshop', '0004_auto_20210523_1349', '2021-06-18 05:56:04.010420'),
(22, 'eshop', '0005_alter_product_date_added', '2021-06-18 05:56:04.047349'),
(23, 'eshop', '0006_auto_20210529_1133', '2021-06-18 05:56:04.056675'),
(24, 'eshop', '0007_productvariant', '2021-06-18 05:56:04.112237'),
(25, 'eshop', '0008_productvariant_stock', '2021-06-18 05:56:04.128187'),
(26, 'eshop', '0009_alter_categories_name', '2021-06-18 05:56:04.152537'),
(27, 'eshop', '0010_alter_categories_options', '2021-06-18 05:56:04.158780'),
(28, 'sessions', '0001_initial', '2021-06-18 05:56:04.190307');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('bz146h1nd6jl91wv46uy03dr4548p6q6', '.eJxVjEEOwiAQRe_C2hCnFCou3fcMZJgZpGogKe3KeHfbpAvd_vfef6uA65LD2mQOE6urAnX63SLSU8oO-IHlXjXVssxT1LuiD9r0WFlet8P9O8jY8lZ7su4syRtwvSGLhodowXsCigKIdPHAybI433EvQMAdpk2SBEhuUJ8v8pE48g:1lu7fO:595J2Rgg1o8kMlOVWjKN76ngYEkli4b2B4tt2MM08Cc', '2021-07-02 06:07:46.969522');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `product_id`) VALUES
(1, 7, 254),
(8, 7, 199),
(31, 16, 357),
(19, 16, 260),
(35, 21, 358),
(28, 11, 357),
(21, 11, 260),
(23, 10, 344);

-- --------------------------------------------------------

--
-- Table structure for table `fund_transfers`
--

CREATE TABLE `fund_transfers` (
  `id` int(11) NOT NULL,
  `delivery_boy_id` int(11) NOT NULL,
  `opening_balance` double NOT NULL,
  `closing_balance` double NOT NULL,
  `amount` double NOT NULL,
  `status` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'delivery_boy', 'Delivery Boys');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `language` varchar(128) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  `is_rtl` tinyint(4) NOT NULL DEFAULT 0,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language`, `code`, `is_rtl`, `created_on`) VALUES
(1, 'English', 'en', 0, '2021-02-26 14:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_directory` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `title`, `name`, `extension`, `type`, `sub_directory`, `size`, `date_created`) VALUES
(5, '1', '1.png', 'png', 'image', 'uploads/media/2021/', '825896', '2021-06-20 09:23:09'),
(6, '2', '2.png', 'png', 'image', 'uploads/media/2021/', '899908', '2021-06-20 09:23:10'),
(8, '4', '4.png', 'png', 'image', 'uploads/media/2021/', '818423', '2021-06-20 09:23:12'),
(9, '5', '5.png', 'png', 'image', 'uploads/media/2021/', '749291', '2021-06-20 09:23:13'),
(10, '6', '6.png', 'png', 'image', 'uploads/media/2021/', '856328', '2021-06-20 09:23:14'),
(11, '7', '7.png', 'png', 'image', 'uploads/media/2021/', '635069', '2021-06-20 09:23:15'),
(12, '8', '8.png', 'png', 'image', 'uploads/media/2021/', '844828', '2021-06-20 09:23:15'),
(13, '9', '9.png', 'png', 'image', 'uploads/media/2021/', '766403', '2021-06-20 09:23:16'),
(14, '10', '10.png', 'png', 'image', 'uploads/media/2021/', '731340', '2021-06-20 09:23:17'),
(15, '11', '11.png', 'png', 'image', 'uploads/media/2021/', '753265', '2021-06-20 09:23:18'),
(16, '12', '12.png', 'png', 'image', 'uploads/media/2021/', '793304', '2021-06-20 09:23:20'),
(17, '21', '21.png', 'png', 'image', 'uploads/media/2021/', '915690', '2021-06-20 09:24:53'),
(18, '22', '22.png', 'png', 'image', 'uploads/media/2021/', '926874', '2021-06-20 09:24:54'),
(19, '23', '23.png', 'png', 'image', 'uploads/media/2021/', '958713', '2021-06-20 09:24:55'),
(20, '24', '24.png', 'png', 'image', 'uploads/media/2021/', '752636', '2021-06-20 09:24:56'),
(21, '25', '25.png', 'png', 'image', 'uploads/media/2021/', '861717', '2021-06-20 09:24:57'),
(22, '26', '26.png', 'png', 'image', 'uploads/media/2021/', '767686', '2021-06-20 09:24:58'),
(23, '27', '27.png', 'png', 'image', 'uploads/media/2021/', '749438', '2021-06-20 09:24:59'),
(24, '28', '28.png', 'png', 'image', 'uploads/media/2021/', '621522', '2021-06-20 09:25:00'),
(25, '29', '29.png', 'png', 'image', 'uploads/media/2021/', '970476', '2021-06-20 09:25:01'),
(26, '30', '30.png', 'png', 'image', 'uploads/media/2021/', '772830', '2021-06-20 09:25:01'),
(27, '31', '31.png', 'png', 'image', 'uploads/media/2021/', '1043897', '2021-06-20 09:25:38'),
(28, '32', '32.png', 'png', 'image', 'uploads/media/2021/', '863953', '2021-06-20 09:25:39'),
(29, '33', '33.png', 'png', 'image', 'uploads/media/2021/', '1010806', '2021-06-20 09:25:40'),
(30, '34', '34.png', 'png', 'image', 'uploads/media/2021/', '911666', '2021-06-20 09:25:41'),
(31, '35', '35.png', 'png', 'image', 'uploads/media/2021/', '733596', '2021-06-20 09:25:42'),
(32, '36', '36.png', 'png', 'image', 'uploads/media/2021/', '1111912', '2021-06-20 09:25:43'),
(33, '37', '37.png', 'png', 'image', 'uploads/media/2021/', '807137', '2021-06-20 09:25:43'),
(34, '38', '38.png', 'png', 'image', 'uploads/media/2021/', '781581', '2021-06-20 09:25:44'),
(35, '39', '39.png', 'png', 'image', 'uploads/media/2021/', '881906', '2021-06-20 09:25:45'),
(36, '40', '40.png', 'png', 'image', 'uploads/media/2021/', '719105', '2021-06-20 09:25:46'),
(37, '41', '41.png', 'png', 'image', 'uploads/media/2021/', '968325', '2021-06-20 09:25:47'),
(38, '42', '42.png', 'png', 'image', 'uploads/media/2021/', '908135', '2021-06-20 09:25:48'),
(39, '18', '18.png', 'png', 'image', 'uploads/media/2021/', '1040346', '2021-06-20 11:18:29'),
(41, '13', '13.png', 'png', 'image', 'uploads/media/2021/', '866419', '2021-06-20 11:25:49'),
(42, '210', '210.png', 'png', 'image', 'uploads/media/2021/', '892585', '2021-06-20 11:25:50'),
(43, '310', '310.png', 'png', 'image', 'uploads/media/2021/', '853082', '2021-06-20 11:25:51'),
(44, '43', '43.png', 'png', 'image', 'uploads/media/2021/', '933610', '2021-06-20 11:25:51'),
(45, '51', '51.png', 'png', 'image', 'uploads/media/2021/', '876409', '2021-06-20 11:25:52'),
(46, '61', '61.png', 'png', 'image', 'uploads/media/2021/', '758576', '2021-06-20 11:25:53'),
(47, '81', '81.png', 'png', 'image', 'uploads/media/2021/', '969420', '2021-06-20 11:25:54'),
(48, '91', '91.png', 'png', 'image', 'uploads/media/2021/', '985096', '2021-06-20 11:25:55'),
(49, '101', '101.png', 'png', 'image', 'uploads/media/2021/', '909407', '2021-06-20 11:25:56'),
(50, '111', '111.png', 'png', 'image', 'uploads/media/2021/', '840208', '2021-06-20 11:25:57'),
(51, '121', '121.png', 'png', 'image', 'uploads/media/2021/', '918244', '2021-06-20 11:25:58'),
(52, '131', '131.png', 'png', 'image', 'uploads/media/2021/', '952311', '2021-06-20 11:25:59'),
(53, '15', '15.png', 'png', 'image', 'uploads/media/2021/', '903581', '2021-06-20 11:26:52'),
(54, '19', '19.png', 'png', 'image', 'uploads/media/2021/', '897590', '2021-06-20 11:26:53'),
(55, '20', '20.png', 'png', 'image', 'uploads/media/2021/', '877446', '2021-06-20 11:26:54'),
(56, '211', '211.png', 'png', 'image', 'uploads/media/2021/', '914714', '2021-06-20 11:26:55'),
(57, '221', '221.png', 'png', 'image', 'uploads/media/2021/', '815158', '2021-06-20 11:26:56'),
(58, '22-', '22-.png', 'png', 'image', 'uploads/media/2021/', '752583', '2021-06-20 11:26:57'),
(59, '22--', '22--.png', 'png', 'image', 'uploads/media/2021/', '832774', '2021-06-20 11:26:58'),
(60, '23-', '23-.png', 'png', 'image', 'uploads/media/2021/', '769851', '2021-06-20 11:26:59'),
(61, '23--', '23--.png', 'png', 'image', 'uploads/media/2021/', '846055', '2021-06-20 11:26:59'),
(62, '23---', '23---.png', 'png', 'image', 'uploads/media/2021/', '855126', '2021-06-20 11:27:00'),
(63, '241', '241.png', 'png', 'image', 'uploads/media/2021/', '936863', '2021-06-20 11:27:01'),
(64, '24-', '24-.png', 'png', 'image', 'uploads/media/2021/', '819456', '2021-06-20 11:27:02'),
(65, '251', '251.png', 'png', 'image', 'uploads/media/2021/', '879869', '2021-06-20 11:27:21'),
(66, '261', '261.png', 'png', 'image', 'uploads/media/2021/', '827652', '2021-06-20 11:27:22'),
(67, 'IMG_20210615_173336_636', 'IMG_20210615_173336_636.png', 'png', 'image', 'uploads/media/2021/', '1236643', '2021-06-21 09:15:47'),
(68, 'IMG_20210616_125319_531', 'IMG_20210616_125319_531.png', 'png', 'image', 'uploads/media/2021/', '1109260', '2021-06-21 09:24:45'),
(69, 'IMG_20210616_125320_863', 'IMG_20210616_125320_863.png', 'png', 'image', 'uploads/media/2021/', '1152550', '2021-06-21 09:25:20'),
(70, 'IMG_20210616_125322_301', 'IMG_20210616_125322_301.png', 'png', 'image', 'uploads/media/2021/', '1288750', '2021-06-21 09:27:30'),
(71, 'IMG_20210616_125324_073', 'IMG_20210616_125324_073.png', 'png', 'image', 'uploads/media/2021/', '897234', '2021-06-21 09:28:11'),
(72, 'IMG_20210616_125326_943', 'IMG_20210616_125326_943.png', 'png', 'image', 'uploads/media/2021/', '1201656', '2021-06-21 09:28:51'),
(73, 'IMG_20210616_125315_371', 'IMG_20210616_125315_371.png', 'png', 'image', 'uploads/media/2021/', '1200768', '2021-06-21 09:29:30'),
(74, 'IMG_20210616_125313_735', 'IMG_20210616_125313_735.png', 'png', 'image', 'uploads/media/2021/', '1177170', '2021-06-21 09:30:11'),
(75, 'IMG_20210615_173325_539', 'IMG_20210615_173325_539.png', 'png', 'image', 'uploads/media/2021/', '1091315', '2021-06-21 09:44:27'),
(76, 'IMG_20210615_173256_388', 'IMG_20210615_173256_388.png', 'png', 'image', 'uploads/media/2021/', '905156', '2021-06-21 09:45:32'),
(77, 'IMG_20210615_173348_857', 'IMG_20210615_173348_857.png', 'png', 'image', 'uploads/media/2021/', '1286476', '2021-06-21 09:46:27'),
(78, 'IMG_20210615_173330_266', 'IMG_20210615_173330_266.png', 'png', 'image', 'uploads/media/2021/', '1206852', '2021-06-21 09:47:51'),
(79, 'IMG_20210615_173327_879', 'IMG_20210615_173327_879.png', 'png', 'image', 'uploads/media/2021/', '1097943', '2021-06-21 09:48:34'),
(80, 'IMG_20210615_173318_200', 'IMG_20210615_173318_200.png', 'png', 'image', 'uploads/media/2021/', '1237223', '2021-06-21 09:49:30'),
(81, 'IMG_20210615_173310_858', 'IMG_20210615_173310_858.png', 'png', 'image', 'uploads/media/2021/', '995413', '2021-06-21 09:50:02'),
(82, 'IMG_20210615_173306_173', 'IMG_20210615_173306_173.png', 'png', 'image', 'uploads/media/2021/', '895780', '2021-06-21 09:50:24'),
(83, 'IMG_20210618_145451_809', 'IMG_20210618_145451_809.png', 'png', 'image', 'uploads/media/2021/', '994636', '2021-06-21 09:51:02'),
(84, 'IMG_20210618_145825_534', 'IMG_20210618_145825_534.png', 'png', 'image', 'uploads/media/2021/', '893596', '2021-06-21 09:51:29'),
(85, 'IMG_20210615_173417_295', 'IMG_20210615_173417_295.png', 'png', 'image', 'uploads/media/2021/', '869305', '2021-06-21 09:51:56'),
(86, 'IMG_20210613_161515_830', 'IMG_20210613_161515_830.png', 'png', 'image', 'uploads/media/2021/', '1182987', '2021-06-21 09:52:30'),
(87, 'IMG_20210613_160511_668', 'IMG_20210613_160511_668.png', 'png', 'image', 'uploads/media/2021/', '970667', '2021-06-21 09:53:06'),
(88, 'IMG_20210611_114332_089', 'IMG_20210611_114332_089.png', 'png', 'image', 'uploads/media/2021/', '1172265', '2021-06-21 09:53:49'),
(89, 'IMG_20210611_114330_258', 'IMG_20210611_114330_258.png', 'png', 'image', 'uploads/media/2021/', '818345', '2021-06-21 09:54:17'),
(91, 'IMG_20210611_084840_429', 'IMG_20210611_084840_429.png', 'png', 'image', 'uploads/media/2021/', '1030090', '2021-06-21 09:55:42'),
(94, 'IMG_20210611_084833_172', 'IMG_20210611_084833_172.png', 'png', 'image', 'uploads/media/2021/', '1167054', '2021-06-21 09:57:48'),
(98, 'IMG_20210611_084823_439', 'IMG_20210611_084823_439.png', 'png', 'image', 'uploads/media/2021/', '944463', '2021-06-21 10:00:25'),
(99, 'IMG_20210611_084821_546', 'IMG_20210611_084821_546.png', 'png', 'image', 'uploads/media/2021/', '1010481', '2021-06-21 10:01:09'),
(101, 'IMG_20210611_084817_376', 'IMG_20210611_084817_376.png', 'png', 'image', 'uploads/media/2021/', '1152122', '2021-06-21 10:02:16'),
(103, 'IMG_20210611_084814_155', 'IMG_20210611_084814_155.png', 'png', 'image', 'uploads/media/2021/', '845714', '2021-06-21 10:03:14'),
(104, 'IMG_20210611_084812_389', 'IMG_20210611_084812_389.png', 'png', 'image', 'uploads/media/2021/', '812444', '2021-06-21 10:03:38'),
(106, 'IMG_20210611_084807_895', 'IMG_20210611_084807_895.png', 'png', 'image', 'uploads/media/2021/', '984595', '2021-06-21 10:04:50'),
(108, 'IMG_20210611_084804_900', 'IMG_20210611_084804_900.png', 'png', 'image', 'uploads/media/2021/', '968959', '2021-06-21 10:05:53'),
(116, 'IMG_20210611_084752_381', 'IMG_20210611_084752_381.png', 'png', 'image', 'uploads/media/2021/', '1112973', '2021-06-21 10:10:31'),
(119, 'IMG_20210611_084744_730', 'IMG_20210611_084744_730.png', 'png', 'image', 'uploads/media/2021/', '1046670', '2021-06-21 10:12:20'),
(124, 'IMG_20210611_084734_712', 'IMG_20210611_084734_712.png', 'png', 'image', 'uploads/media/2021/', '849737', '2021-06-22 08:32:08'),
(126, 'IMG_20210611_084728_255', 'IMG_20210611_084728_255.png', 'png', 'image', 'uploads/media/2021/', '1124255', '2021-06-22 08:33:29'),
(127, 'IMG_20210611_084725_988', 'IMG_20210611_084725_988.png', 'png', 'image', 'uploads/media/2021/', '1012639', '2021-06-22 08:33:58'),
(128, 'IMG_20210611_084721_548', 'IMG_20210611_084721_548.png', 'png', 'image', 'uploads/media/2021/', '942918', '2021-06-22 08:34:41'),
(129, 'IMG_20210611_084720_251', 'IMG_20210611_084720_251.png', 'png', 'image', 'uploads/media/2021/', '1159646', '2021-06-22 08:35:22'),
(132, 'IMG_20210611_084715_059', 'IMG_20210611_084715_059.png', 'png', 'image', 'uploads/media/2021/', '1182987', '2021-06-22 08:37:35'),
(134, 'IMG_20210611_084709_506', 'IMG_20210611_084709_506.png', 'png', 'image', 'uploads/media/2021/', '1193048', '2021-06-22 08:38:42'),
(140, 'IMG_20210611_084659_804', 'IMG_20210611_084659_804.png', 'png', 'image', 'uploads/media/2021/', '969806', '2021-06-22 08:42:01'),
(141, 'IMG_20210611_084656_000', 'IMG_20210611_084656_000.png', 'png', 'image', 'uploads/media/2021/', '1035415', '2021-06-22 08:42:26'),
(144, 'IMG_20210611_084651_173', 'IMG_20210611_084651_173.png', 'png', 'image', 'uploads/media/2021/', '820758', '2021-06-22 08:44:24'),
(145, 'IMG_20210611_084648_196', 'IMG_20210611_084648_196.png', 'png', 'image', 'uploads/media/2021/', '1230321', '2021-06-22 08:44:55'),
(146, 'IMG_20210611_084646_962', 'IMG_20210611_084646_962.png', 'png', 'image', 'uploads/media/2021/', '867629', '2021-06-22 08:45:24'),
(149, 'IMG_20210611_084641_217', 'IMG_20210611_084641_217.png', 'png', 'image', 'uploads/media/2021/', '839637', '2021-06-22 08:47:54'),
(150, 'IMG_20210611_084640_080', 'IMG_20210611_084640_080.png', 'png', 'image', 'uploads/media/2021/', '800968', '2021-06-22 08:48:29'),
(151, 'IMG_20210611_084638_174', 'IMG_20210611_084638_174.png', 'png', 'image', 'uploads/media/2021/', '1163348', '2021-06-22 08:49:07'),
(152, 'IMG_20210611_084636_526', 'IMG_20210611_084636_526.png', 'png', 'image', 'uploads/media/2021/', '1133302', '2021-06-22 08:49:33'),
(154, 'IMG_20210611_084633_891', 'IMG_20210611_084633_891.png', 'png', 'image', 'uploads/media/2021/', '1074039', '2021-06-22 08:52:03'),
(158, 'IMG_20210611_084626_197', 'IMG_20210611_084626_197.png', 'png', 'image', 'uploads/media/2021/', '1001710', '2021-06-22 08:54:23'),
(160, 'IMG_20210611_084624_298', 'IMG_20210611_084624_298.png', 'png', 'image', 'uploads/media/2021/', '863462', '2021-06-22 08:55:35'),
(161, 'IMG_20210611_084621_638', 'IMG_20210611_084621_638.png', 'png', 'image', 'uploads/media/2021/', '1083241', '2021-06-22 08:56:11'),
(162, 'IMG_20210611_084620_324', 'IMG_20210611_084620_324.png', 'png', 'image', 'uploads/media/2021/', '846222', '2021-06-22 08:57:04'),
(164, 'IMG_20210611_084615_675', 'IMG_20210611_084615_675.png', 'png', 'image', 'uploads/media/2021/', '1072812', '2021-06-22 09:53:52'),
(165, 'IMG_20210611_084614_879', 'IMG_20210611_084614_879.png', 'png', 'image', 'uploads/media/2021/', '1191154', '2021-06-22 09:54:44'),
(167, 'IMG_20210611_084611_021', 'IMG_20210611_084611_021.png', 'png', 'image', 'uploads/media/2021/', '745284', '2021-06-22 09:56:03'),
(168, 'IMG_20210611_084607_650', 'IMG_20210611_084607_650.png', 'png', 'image', 'uploads/media/2021/', '1215186', '2021-06-22 09:57:14'),
(169, 'IMG_20210611_084605_934', 'IMG_20210611_084605_934.png', 'png', 'image', 'uploads/media/2021/', '1117334', '2021-06-22 09:58:03'),
(170, 'IMG_20210611_084559_391', 'IMG_20210611_084559_391.png', 'png', 'image', 'uploads/media/2021/', '638652', '2021-06-22 09:58:37'),
(171, 'IMG_20210611_084551_229', 'IMG_20210611_084551_229.png', 'png', 'image', 'uploads/media/2021/', '868225', '2021-06-22 09:59:57'),
(174, 'IMG_20210611_084531_908', 'IMG_20210611_084531_908.png', 'png', 'image', 'uploads/media/2021/', '964658', '2021-06-22 10:01:44'),
(175, 'IMG_20210611_084530_464', 'IMG_20210611_084530_464.png', 'png', 'image', 'uploads/media/2021/', '961364', '2021-06-22 10:02:20'),
(176, 'IMG_20210611_084500_616', 'IMG_20210611_084500_616.png', 'png', 'image', 'uploads/media/2021/', '855498', '2021-06-22 10:02:48'),
(178, 'IMG_20210611_084446_177', 'IMG_20210611_084446_177.png', 'png', 'image', 'uploads/media/2021/', '893596', '2021-06-22 10:04:01'),
(179, 'IMG_20210610_141528_312', 'IMG_20210610_141528_312.jpg', 'jpg', 'image', 'uploads/media/2021/', '105592', '2021-06-22 10:15:00'),
(180, 'IMG_20210610_141526_378', 'IMG_20210610_141526_378.jpg', 'jpg', 'image', 'uploads/media/2021/', '101132', '2021-06-22 10:15:28'),
(181, 'IMG_20210610_141523_964', 'IMG_20210610_141523_964.jpg', 'jpg', 'image', 'uploads/media/2021/', '112950', '2021-06-22 10:16:04'),
(182, 'IMG_20210610_141519_219', 'IMG_20210610_141519_219.jpg', 'jpg', 'image', 'uploads/media/2021/', '106154', '2021-06-22 10:16:31'),
(183, 'IMG_20210610_141516_871', 'IMG_20210610_141516_871.jpg', 'jpg', 'image', 'uploads/media/2021/', '102591', '2021-06-22 10:16:56'),
(184, 'IMG_20210610_141514_736', 'IMG_20210610_141514_736.jpg', 'jpg', 'image', 'uploads/media/2021/', '90080', '2021-06-22 10:17:25'),
(187, 'IMG_20210610_105311_442', 'IMG_20210610_105311_442.png', 'png', 'image', 'uploads/media/2021/', '995857', '2021-06-22 10:19:51'),
(188, 'IMG_20210610_105116_032', 'IMG_20210610_105116_032.png', 'png', 'image', 'uploads/media/2021/', '816584', '2021-06-22 10:20:22'),
(189, 'FB_IMG_1617690939291', 'FB_IMG_1617690939291.jpg', 'jpg', 'image', 'uploads/media/2021/', '72848', '2021-06-22 10:21:43'),
(190, 'IMG_20210616_130345', 'IMG_20210616_130345.jpg', 'jpg', 'image', 'uploads/media/2021/', '64559', '2021-06-22 10:32:14'),
(191, 'IMG_20210616_130336', 'IMG_20210616_130336.jpg', 'jpg', 'image', 'uploads/media/2021/', '63372', '2021-06-22 10:32:42'),
(192, 'IMG_20210616_130325', 'IMG_20210616_130325.jpg', 'jpg', 'image', 'uploads/media/2021/', '60205', '2021-06-22 10:33:15'),
(193, 'IMG_20210616_122659', 'IMG_20210616_122659.jpg', 'jpg', 'image', 'uploads/media/2021/', '70425', '2021-06-22 10:33:57'),
(194, 'IMG_20210518_093536', 'IMG_20210518_093536.jpg', 'jpg', 'image', 'uploads/media/2021/', '56108', '2021-06-22 10:35:29'),
(195, 'FB_IMG_1622346494562', 'FB_IMG_1622346494562.jpg', 'jpg', 'image', 'uploads/media/2021/', '61373', '2021-06-22 10:37:42'),
(196, 'FB_IMG_1621303956728', 'FB_IMG_1621303956728.jpg', 'jpg', 'image', 'uploads/media/2021/', '67823', '2021-06-22 10:38:15'),
(197, 'FB_IMG_1621303959143', 'FB_IMG_1621303959143.jpg', 'jpg', 'image', 'uploads/media/2021/', '64301', '2021-06-22 10:38:38'),
(198, 'FB_IMG_1621303961553', 'FB_IMG_1621303961553.jpg', 'jpg', 'image', 'uploads/media/2021/', '51812', '2021-06-22 10:39:08'),
(199, 'FB_IMG_1621303963840', 'FB_IMG_1621303963840.jpg', 'jpg', 'image', 'uploads/media/2021/', '78972', '2021-06-22 10:40:00'),
(200, 'FB_IMG_1621303965985', 'FB_IMG_1621303965985.jpg', 'jpg', 'image', 'uploads/media/2021/', '62097', '2021-06-22 10:40:26'),
(201, 'FB_IMG_1617771647504', 'FB_IMG_1617771647504.jpg', 'jpg', 'image', 'uploads/media/2021/', '49522', '2021-06-22 10:42:19'),
(202, 'IMG_20210623_134624', 'IMG_20210623_134624.jpg', 'jpg', 'image', 'uploads/media/2021/', '77218', '2021-06-23 07:06:30'),
(203, 'IMG_20210623_134608', 'IMG_20210623_134608.jpg', 'jpg', 'image', 'uploads/media/2021/', '75472', '2021-06-23 07:06:58'),
(204, 'IMG_20210623_134550', 'IMG_20210623_134550.jpg', 'jpg', 'image', 'uploads/media/2021/', '73534', '2021-06-23 07:07:24'),
(205, 'IMG_20210623_134533', 'IMG_20210623_134533.jpg', 'jpg', 'image', 'uploads/media/2021/', '70880', '2021-06-23 07:07:44'),
(206, 'IMG_20210623_134509', 'IMG_20210623_134509.jpg', 'jpg', 'image', 'uploads/media/2021/', '75851', '2021-06-23 07:08:04'),
(207, 'IMG_20210623_134445', 'IMG_20210623_134445.jpg', 'jpg', 'image', 'uploads/media/2021/', '71045', '2021-06-23 07:08:29'),
(208, 'FB_IMG_1623306005579', 'FB_IMG_1623306005579.jpg', 'jpg', 'image', 'uploads/media/2021/', '87471', '2021-06-23 07:09:13'),
(209, 'FB_IMG_1623306008062', 'FB_IMG_1623306008062.jpg', 'jpg', 'image', 'uploads/media/2021/', '83631', '2021-06-23 07:09:56'),
(210, 'FB_IMG_1623306013024', 'FB_IMG_1623306013024.jpg', 'jpg', 'image', 'uploads/media/2021/', '82337', '2021-06-23 07:10:22'),
(211, 'FB_IMG_1623306015571', 'FB_IMG_1623306015571.jpg', 'jpg', 'image', 'uploads/media/2021/', '90346', '2021-06-23 07:11:01'),
(212, 'IMG_20210616_125941', 'IMG_20210616_125941.jpg', 'jpg', 'image', 'uploads/media/2021/', '65267', '2021-06-23 07:23:33'),
(213, 'IMG_20210616_125924', 'IMG_20210616_125924.jpg', 'jpg', 'image', 'uploads/media/2021/', '61922', '2021-06-23 07:24:02'),
(214, 'IMG_20210616_130055', 'IMG_20210616_130055.jpg', 'jpg', 'image', 'uploads/media/2021/', '68389', '2021-06-23 07:24:28'),
(215, 'IMG_20210616_130008', 'IMG_20210616_130008.jpg', 'jpg', 'image', 'uploads/media/2021/', '65947', '2021-06-23 07:24:55'),
(216, 'IMG_20210616_130020', 'IMG_20210616_130020.jpg', 'jpg', 'image', 'uploads/media/2021/', '72180', '2021-06-23 07:25:17'),
(217, 'IMG_20210615_173417_2951', 'IMG_20210615_173417_2951.png', 'png', 'image', 'uploads/media/2021/', '869305', '2021-06-23 07:26:40'),
(218, 'IMG_20210615_173256_3881', 'IMG_20210615_173256_3881.png', 'png', 'image', 'uploads/media/2021/', '905156', '2021-06-23 07:44:37'),
(219, 'IMG_20210615_173306_1731', 'IMG_20210615_173306_1731.png', 'png', 'image', 'uploads/media/2021/', '895780', '2021-06-23 07:45:15'),
(220, 'IMG_20210623_162905_982', 'IMG_20210623_162905_982.png', 'png', 'image', 'uploads/media/2021/', '939042', '2021-06-23 09:29:47'),
(221, 'IMG_20210623_162838_318', 'IMG_20210623_162838_318.png', 'png', 'image', 'uploads/media/2021/', '957722', '2021-06-23 10:19:14'),
(222, 'IMG_20210623_172113_685', 'IMG_20210623_172113_685.png', 'png', 'image', 'uploads/media/2021/', '995857', '2021-06-23 10:21:21'),
(223, 'IMG_20210624_113825_480', 'IMG_20210624_113825_480.png', 'png', 'image', 'uploads/media/2021/', '948164', '2021-06-24 04:39:54'),
(224, 'IMG_20210624_155148_252', 'IMG_20210624_155148_252.png', 'png', 'image', 'uploads/media/2021/', '881496', '2021-06-24 08:52:21'),
(226, 'IMG_20210611_084629_682', 'IMG_20210611_084629_682.png', 'png', 'image', 'uploads/media/2021/', '967121', '2021-06-24 09:55:15'),
(227, 'IMG_20210611_084836_256', 'IMG_20210611_084836_256.png', 'png', 'image', 'uploads/media/2021/', '1097518', '2021-06-24 10:36:46'),
(228, 'IMG_20210624_174800_980', 'IMG_20210624_174800_980.png', 'png', 'image', 'uploads/media/2021/', '1074039', '2021-06-24 10:48:21'),
(230, 'IMG_20210625_141326_693', 'IMG_20210625_141326_693.png', 'png', 'image', 'uploads/media/2021/', '1076670', '2021-06-25 07:15:23'),
(233, 'IMG_20210625_150723_379', 'IMG_20210625_150723_379.png', 'png', 'image', 'uploads/media/2021/', '971675', '2021-06-25 08:40:11'),
(234, 'IMG_20210625_142045', 'IMG_20210625_142045.jpg', 'jpg', 'image', 'uploads/media/2021/', '157790', '2021-06-25 08:40:31'),
(235, 'IMG_20210611_084740_856', 'IMG_20210611_084740_856.png', 'png', 'image', 'uploads/media/2021/', '968261', '2021-06-25 08:46:39'),
(236, 'IMG_20210611_084736_791', 'IMG_20210611_084736_791.png', 'png', 'image', 'uploads/media/2021/', '1177842', '2021-06-25 08:47:11'),
(237, 'IMG_20210611_084834_527', 'IMG_20210611_084834_527.png', 'png', 'image', 'uploads/media/2021/', '1170354', '2021-06-25 08:47:39'),
(238, 'IMG_20210611_084829_156', 'IMG_20210611_084829_156.png', 'png', 'image', 'uploads/media/2021/', '1101447', '2021-06-25 08:48:09'),
(239, 'IMG_20210611_084837_535', 'IMG_20210611_084837_535.png', 'png', 'image', 'uploads/media/2021/', '1043049', '2021-06-25 08:48:46'),
(240, 'IMG_20210611_084825_082', 'IMG_20210611_084825_082.png', 'png', 'image', 'uploads/media/2021/', '1097119', '2021-06-25 08:49:14'),
(241, 'IMG_20210611_084827_214', 'IMG_20210611_084827_214.png', 'png', 'image', 'uploads/media/2021/', '1143141', '2021-06-25 08:51:09'),
(242, 'IMG_20210611_084800_811', 'IMG_20210611_084800_811.png', 'png', 'image', 'uploads/media/2021/', '1220496', '2021-06-25 08:53:26'),
(243, 'IMG_20210611_084703_885', 'IMG_20210611_084703_885.png', 'png', 'image', 'uploads/media/2021/', '1044309', '2021-06-25 08:54:51'),
(244, 'IMG_20210611_084753_556', 'IMG_20210611_084753_556.png', 'png', 'image', 'uploads/media/2021/', '1150138', '2021-06-25 08:56:17'),
(245, 'IMG_20210611_084756_986', 'IMG_20210611_084756_986.png', 'png', 'image', 'uploads/media/2021/', '1256832', '2021-06-25 08:58:03'),
(246, 'IMG_20210611_084820_119', 'IMG_20210611_084820_119.png', 'png', 'image', 'uploads/media/2021/', '1156238', '2021-06-25 09:07:27'),
(247, 'IMG_20210611_084708_395', 'IMG_20210611_084708_395.png', 'png', 'image', 'uploads/media/2021/', '1124342', '2021-06-25 09:08:51'),
(248, 'IMG_20210611_084706_055', 'IMG_20210611_084706_055.png', 'png', 'image', 'uploads/media/2021/', '888492', '2021-06-25 09:13:27'),
(249, 'IMG_20210611_084755_720', 'IMG_20210611_084755_720.png', 'png', 'image', 'uploads/media/2021/', '1100619', '2021-06-25 09:14:56'),
(250, 'IMG_20210611_084548_740', 'IMG_20210611_084548_740.png', 'png', 'image', 'uploads/media/2021/', '1025739', '2021-06-25 09:16:28'),
(251, 'IMG_20210611_084624_829', 'IMG_20210611_084624_829.png', 'png', 'image', 'uploads/media/2021/', '1127631', '2021-06-25 09:18:33'),
(252, 'IMG_20210611_084628_330', 'IMG_20210611_084628_330.png', 'png', 'image', 'uploads/media/2021/', '824599', '2021-06-25 09:20:06'),
(253, 'IMG_20210611_084654_654', 'IMG_20210611_084654_654.png', 'png', 'image', 'uploads/media/2021/', '926014', '2021-06-25 09:32:02'),
(254, 'IMG_20210611_084644_197', 'IMG_20210611_084644_197.png', 'png', 'image', 'uploads/media/2021/', '938168', '2021-06-25 09:35:15'),
(255, 'IMG_20210611_084642_895', 'IMG_20210611_084642_895.png', 'png', 'image', 'uploads/media/2021/', '1117233', '2021-06-25 09:35:45'),
(256, 'IMG_20210611_084810_470', 'IMG_20210611_084810_470.png', 'png', 'image', 'uploads/media/2021/', '896096', '2021-06-25 09:37:06'),
(257, 'IMG_20210611_084758_841', 'IMG_20210611_084758_841.png', 'png', 'image', 'uploads/media/2021/', '972113', '2021-06-25 09:37:39'),
(258, 'IMG_20210611_084631_465', 'IMG_20210611_084631_465.png', 'png', 'image', 'uploads/media/2021/', '1043364', '2021-06-25 09:41:59'),
(259, 'IMG_20210611_084632_826', 'IMG_20210611_084632_826.png', 'png', 'image', 'uploads/media/2021/', '999599', '2021-06-25 09:42:29'),
(260, 'IMG_20210611_084802_407', 'IMG_20210611_084802_407.png', 'png', 'image', 'uploads/media/2021/', '981735', '2021-06-25 09:43:53'),
(261, 'IMG_20210611_084718_533', 'IMG_20210611_084718_533.png', 'png', 'image', 'uploads/media/2021/', '1125678', '2021-06-25 09:44:21'),
(262, 'IMG_20210611_084635_331', 'IMG_20210611_084635_331.png', 'png', 'image', 'uploads/media/2021/', '996535', '2021-06-25 09:49:23'),
(263, 'IMG_20210611_084710_779', 'IMG_20210611_084710_779.png', 'png', 'image', 'uploads/media/2021/', '828572', '2021-06-25 09:50:38'),
(264, 'IMG_20210611_084741_797', 'IMG_20210611_084741_797.png', 'png', 'image', 'uploads/media/2021/', '1067393', '2021-06-25 09:51:11'),
(265, 'IMG_20210625_101218_225', 'IMG_20210625_101218_225.png', 'png', 'image', 'uploads/media/2021/', '991159', '2021-06-25 09:52:13'),
(266, 'IMG_20210611_084702_558', 'IMG_20210611_084702_558.png', 'png', 'image', 'uploads/media/2021/', '987292', '2021-06-25 09:54:19'),
(267, 'IMG_20210611_084653_348', 'IMG_20210611_084653_348.png', 'png', 'image', 'uploads/media/2021/', '944381', '2021-06-25 09:54:50'),
(268, 'IMG_20210611_084706_791', 'IMG_20210611_084706_791.png', 'png', 'image', 'uploads/media/2021/', '932672', '2021-06-25 09:59:06'),
(269, 'IMG_20210611_084806_691', 'IMG_20210611_084806_691.png', 'png', 'image', 'uploads/media/2021/', '916786', '2021-06-25 10:00:20'),
(270, 'IMG_20210611_084803_370', 'IMG_20210611_084803_370.png', 'png', 'image', 'uploads/media/2021/', '1075474', '2021-06-25 10:01:18'),
(271, 'IMG_20210611_084746_272', 'IMG_20210611_084746_272.png', 'png', 'image', 'uploads/media/2021/', '1124542', '2021-06-25 10:01:59'),
(272, 'IMG_20210611_084748_505', 'IMG_20210611_084748_505.png', 'png', 'image', 'uploads/media/2021/', '1016870', '2021-06-25 10:03:51'),
(273, 'IMG_20210611_084612_839', 'IMG_20210611_084612_839.png', 'png', 'image', 'uploads/media/2021/', '936388', '2021-06-25 10:04:35'),
(274, 'IMG_20210611_084815_413', 'IMG_20210611_084815_413.png', 'png', 'image', 'uploads/media/2021/', '769380', '2021-06-25 10:05:29'),
(275, 'IMG_20210623_162838_3181', 'IMG_20210623_162838_3181.png', 'png', 'image', 'uploads/media/2021/', '957722', '2021-06-25 10:25:42'),
(276, 'IMG_20210625_173458', 'IMG_20210625_173458.jpg', 'jpg', 'image', 'uploads/media/2021/', '71409', '2021-06-25 10:35:23'),
(277, 'IMG_20210625_173442', 'IMG_20210625_173442.jpg', 'jpg', 'image', 'uploads/media/2021/', '64405', '2021-06-25 10:35:50'),
(278, 'IMG_20210625_173431', 'IMG_20210625_173431.jpg', 'jpg', 'image', 'uploads/media/2021/', '69165', '2021-06-25 10:36:29'),
(279, 'IMG_20210611_084717_423', 'IMG_20210611_084717_423.png', 'png', 'image', 'uploads/media/2021/', '778189', '2021-06-25 10:57:52'),
(280, 'IMG_20210611_084843_294', 'IMG_20210611_084843_294.png', 'png', 'image', 'uploads/media/2021/', '1123231', '2021-06-25 10:58:39'),
(281, 'IMG_20210611_084732_987', 'IMG_20210611_084732_987.png', 'png', 'image', 'uploads/media/2021/', '1025759', '2021-06-25 11:00:39'),
(282, 'IMG_20210611_084710_7791', 'IMG_20210611_084710_7791.png', 'png', 'image', 'uploads/media/2021/', '828572', '2021-06-25 11:01:04'),
(283, 'IMG_20210611_084617_701', 'IMG_20210611_084617_701.png', 'png', 'image', 'uploads/media/2021/', '974405', '2021-06-25 11:02:48'),
(284, 'IMG_20210611_084455_620', 'IMG_20210611_084455_620.png', 'png', 'image', 'uploads/media/2021/', '985002', '2021-06-25 11:03:42'),
(285, 'IMG_20210611_084738_325', 'IMG_20210611_084738_325.png', 'png', 'image', 'uploads/media/2021/', '993661', '2021-06-25 11:04:53'),
(286, 'IMG_20210624_155515_887', 'IMG_20210624_155515_887.png', 'png', 'image', 'uploads/media/2021/', '948646', '2021-06-25 11:05:16'),
(287, 'IMG_20210531_103436', 'IMG_20210531_103436.jpg', 'jpg', 'image', 'uploads/media/2021/', '56198', '2021-06-25 11:06:22'),
(288, 'IMG_20210616_130422', 'IMG_20210616_130422.jpg', 'jpg', 'image', 'uploads/media/2021/', '75869', '2021-06-25 11:08:23'),
(289, 'IMG_20210616_130357', 'IMG_20210616_130357.jpg', 'jpg', 'image', 'uploads/media/2021/', '73319', '2021-06-25 11:08:42'),
(290, 'IMG_20210616_1300201', 'IMG_20210616_1300201.jpg', 'jpg', 'image', 'uploads/media/2021/', '72180', '2021-06-25 11:09:19'),
(291, 'IMG_20210623_1345501', 'IMG_20210623_1345501.jpg', 'jpg', 'image', 'uploads/media/2021/', '73534', '2021-06-25 11:09:51'),
(292, 'IMG_20210531_103414', 'IMG_20210531_103414.jpg', 'jpg', 'image', 'uploads/media/2021/', '65227', '2021-06-25 11:10:37'),
(293, 'IMG_20210531_103218', 'IMG_20210531_103218.jpg', 'jpg', 'image', 'uploads/media/2021/', '65922', '2021-06-25 11:11:40'),
(294, 'IMG_20210616_1300081', 'IMG_20210616_1300081.jpg', 'jpg', 'image', 'uploads/media/2021/', '65947', '2021-06-25 11:20:30'),
(295, 'IMG_20210616_1226591', 'IMG_20210616_1226591.jpg', 'jpg', 'image', 'uploads/media/2021/', '70425', '2021-06-26 12:50:26'),
(296, 'IMG_20210626_202233', 'IMG_20210626_202233.jpg', 'jpg', 'image', 'uploads/media/2021/', '78351', '2021-06-26 13:23:35'),
(297, 'IMG_20210626_203143', 'IMG_20210626_203143.jpg', 'jpg', 'image', 'uploads/media/2021/', '79722', '2021-06-26 13:31:57'),
(298, 'IMG_20210626_203625', 'IMG_20210626_203625.jpg', 'jpg', 'image', 'uploads/media/2021/', '76203', '2021-06-26 13:36:41'),
(299, 'IMG_20210626_204056', 'IMG_20210626_204056.jpg', 'jpg', 'image', 'uploads/media/2021/', '74645', '2021-06-26 13:41:12'),
(300, 'FB_IMG_1617690926512', 'FB_IMG_1617690926512.jpg', 'jpg', 'image', 'uploads/media/2021/', '68554', '2021-06-27 02:33:36'),
(301, 'FB_IMG_1617690928902', 'FB_IMG_1617690928902.jpg', 'jpg', 'image', 'uploads/media/2021/', '71539', '2021-06-27 02:34:15'),
(302, 'FB_IMG_1617690936450', 'FB_IMG_1617690936450.jpg', 'jpg', 'image', 'uploads/media/2021/', '64948', '2021-06-27 02:34:42'),
(304, 'sale_(3)', 'sale_(3).png', 'png', 'image', 'uploads/media/2021/', '361449', '2021-07-18 13:41:07'),
(305, 'sale_(2)', 'sale_(2).png', 'png', 'image', 'uploads/media/2021/', '344183', '2021-07-18 13:41:08'),
(306, 'sale_(1)', 'sale_(1).png', 'png', 'image', 'uploads/media/2021/', '456686', '2021-07-18 13:41:09'),
(307, 'sale', 'sale.png', 'png', 'image', 'uploads/media/2021/', '290539', '2021-07-18 13:41:10'),
(308, '212', '212.png', 'png', 'image', 'uploads/media/2021/', '314577', '2021-07-18 13:50:53'),
(309, '3', '3.png', 'png', 'image', 'uploads/media/2021/', '224770', '2021-07-18 13:50:54'),
(310, '44', '44.png', 'png', 'image', 'uploads/media/2021/', '284464', '2021-07-18 13:50:55'),
(311, '52', '52.png', 'png', 'image', 'uploads/media/2021/', '254887', '2021-07-18 13:50:56'),
(312, '62', '62.png', 'png', 'image', 'uploads/media/2021/', '336629', '2021-07-18 13:50:56'),
(313, '14', '14.png', 'png', 'image', 'uploads/media/2021/', '474039', '2021-09-16 06:32:58'),
(314, '213', '213.png', 'png', 'image', 'uploads/media/2021/', '590486', '2021-09-16 06:32:58'),
(315, '311', '311.png', 'png', 'image', 'uploads/media/2021/', '199109', '2021-09-16 06:32:59'),
(316, '16', '16.png', 'png', 'image', 'uploads/media/2021/', '373118', '2021-09-16 07:49:50'),
(317, '214', '214.png', 'png', 'image', 'uploads/media/2021/', '505551', '2021-09-16 07:49:51'),
(318, '312', '312.png', 'png', 'image', 'uploads/media/2021/', '375932', '2021-09-16 07:49:52'),
(319, '45', '45.png', 'png', 'image', 'uploads/media/2021/', '308345', '2021-09-16 07:49:53'),
(320, '53', '53.png', 'png', 'image', 'uploads/media/2021/', '290154', '2021-09-16 07:49:54'),
(321, '63', '63.png', 'png', 'image', 'uploads/media/2021/', '468937', '2021-09-16 07:49:54'),
(322, 'Wow_revised_(3)', 'Wow_revised_(3).png', 'png', 'image', 'uploads/media/2021/', '355637', '2021-09-17 17:17:35'),
(323, 'Wow_revised_(2)', 'Wow_revised_(2).png', 'png', 'image', 'uploads/media/2021/', '273066', '2021-09-17 17:17:35'),
(324, 'Wow_revised_(1)', 'Wow_revised_(1).png', 'png', 'image', 'uploads/media/2021/', '310122', '2021-09-17 17:17:36'),
(325, 'Copy_of_Facebook_Cover_(2)', 'Copy_of_Facebook_Cover_(2).png', 'png', 'image', 'uploads/media/2021/', '565501', '2021-09-17 17:17:37'),
(326, 'Copy_of_Facebook_Cover_(1)', 'Copy_of_Facebook_Cover_(1).png', 'png', 'image', 'uploads/media/2021/', '460089', '2021-09-17 17:17:37'),
(327, 'a_bOOk', 'a_bOOk.png', 'png', 'image', 'uploads/media/2021/', '283896', '2021-09-18 14:23:34'),
(328, 'Early_Reader_(3)', 'Early_Reader_(3).png', 'png', 'image', 'uploads/media/2021/', '926373', '2021-09-23 12:48:01'),
(329, 'Early_Reader_(6)', 'Early_Reader_(6).png', 'png', 'image', 'uploads/media/2021/', '779881', '2021-09-23 12:48:02'),
(330, 'Early_Reader_(5)', 'Early_Reader_(5).png', 'png', 'image', 'uploads/media/2021/', '838557', '2021-09-23 12:48:18'),
(331, 'New_Arrival_-_Knowledge_Book_(4)', 'New_Arrival_-_Knowledge_Book_(4).png', 'png', 'image', 'uploads/media/2021/', '1342092', '2021-09-23 12:48:31'),
(332, 'Science', 'Science.png', 'png', 'image', 'uploads/media/2021/', '1149218', '2021-09-23 12:48:32'),
(333, 'The_Classic_Enid_Blyton_Collection_(2)', 'The_Classic_Enid_Blyton_Collection_(2).png', 'png', 'image', 'uploads/media/2021/', '1175578', '2021-09-23 12:48:51'),
(334, 'Dr__Seuss_collection', 'Dr__Seuss_collection.png', 'png', 'image', 'uploads/media/2021/', '1388311', '2021-09-23 12:49:11'),
(335, 'The_cat_in_the_hat', 'The_cat_in_the_hat.png', 'png', 'image', 'uploads/media/2021/', '1092641', '2021-09-23 12:49:39'),
(336, 'Ready,_Freddy!_Collection_1', 'Ready,_Freddy!_Collection_1.png', 'png', 'image', 'uploads/media/2021/', '1320273', '2021-09-23 12:49:41'),
(337, 'A_Faraway_Tree_+_The_Wishing_Chair_Adventure_Set', 'A_Faraway_Tree_+_The_Wishing_Chair_Adventure_Set.png', 'png', 'image', 'uploads/media/2021/', '1406197', '2021-09-23 12:50:26'),
(338, 'Dragon', 'Dragon.png', 'png', 'image', 'uploads/media/2021/', '1346044', '2021-09-23 12:51:09'),
(339, '3D', '3D.png', 'png', 'image', 'uploads/media/2021/', '1241048', '2021-09-23 12:51:11'),
(340, 'Early_Reader_(4)', 'Early_Reader_(4).png', 'png', 'image', 'uploads/media/2021/', '1226664', '2021-09-23 12:51:23'),
(341, 'Early_Reader_(2)', 'Early_Reader_(2).png', 'png', 'image', 'uploads/media/2021/', '1080023', '2021-09-23 12:51:34'),
(342, 'Early_Reader_(1)', 'Early_Reader_(1).png', 'png', 'image', 'uploads/media/2021/', '1347052', '2021-09-23 12:51:48'),
(343, 'My_usborne_2', 'My_usborne_2.png', 'png', 'image', 'uploads/media/2021/', '1115517', '2021-09-23 12:52:15'),
(344, 'My_usborne_1', 'My_usborne_1.png', 'png', 'image', 'uploads/media/2021/', '1099609', '2021-09-23 12:52:16'),
(345, 'My_first_library2', 'My_first_library2.png', 'png', 'image', 'uploads/media/2021/', '1102617', '2021-09-23 12:52:17'),
(346, 'My_1', 'My_1.png', 'png', 'image', 'uploads/media/2021/', '1288459', '2021-09-23 12:52:45'),
(347, 'Usborne_book_collection', 'Usborne_book_collection.png', 'png', 'image', 'uploads/media/2021/', '1146259', '2021-09-23 12:52:47'),
(348, 'oxford_4', 'oxford_4.png', 'png', 'image', 'uploads/media/2021/', '1317944', '2021-09-23 12:52:48'),
(349, 'STEM', 'STEM.png', 'png', 'image', 'uploads/media/2021/', '1248002', '2021-09-23 12:52:49'),
(350, 'Storey_tree', 'Storey_tree.png', 'png', 'image', 'uploads/media/2021/', '1319124', '2021-09-23 12:52:50'),
(351, 'Chicken', 'Chicken.png', 'png', 'image', 'uploads/media/2021/', '1138440', '2021-09-23 12:53:59'),
(352, 'Charles_Dickens_(2)', 'Charles_Dickens_(2).png', 'png', 'image', 'uploads/media/2021/', '1178941', '2021-09-23 12:54:36'),
(353, 'KA', 'KA.png', 'png', 'image', 'uploads/media/2021/', '1284474', '2021-09-23 12:54:37'),
(354, 'Bad_G', 'Bad_G.png', 'png', 'image', 'uploads/media/2021/', '1194802', '2021-09-23 12:54:38'),
(355, 'Junie_B', 'Junie_B.png', 'png', 'image', 'uploads/media/2021/', '1267981', '2021-09-23 12:54:39'),
(356, 'Horrible_science', 'Horrible_science.png', 'png', 'image', 'uploads/media/2021/', '1340515', '2021-09-23 12:55:41'),
(357, 'Az_Mysteries_(2)', 'Az_Mysteries_(2).png', 'png', 'image', 'uploads/media/2021/', '1417775', '2021-09-23 12:55:42'),
(358, 'The_School_for_Good_and_Evil_(2)', 'The_School_for_Good_and_Evil_(2).png', 'png', 'image', 'uploads/media/2021/', '1190814', '2021-09-23 12:55:44'),
(359, 'Percy_Jackson', 'Percy_Jackson.png', 'png', 'image', 'uploads/media/2021/', '1281003', '2021-09-23 12:55:45'),
(360, 'Heroes_of_Olympu', 'Heroes_of_Olympu.png', 'png', 'image', 'uploads/media/2021/', '1280743', '2021-09-23 12:55:46'),
(361, 'Harry_Potter_(2)', 'Harry_Potter_(2).png', 'png', 'image', 'uploads/media/2021/', '1157941', '2021-09-23 12:55:47'),
(362, 'Usbornse_Beginner', 'Usbornse_Beginner.png', 'png', 'image', 'uploads/media/2021/', '1078513', '2021-09-23 12:55:48'),
(363, 'Nature_(2)', 'Nature_(2).png', 'png', 'image', 'uploads/media/2021/', '1267562', '2021-09-23 12:55:49'),
(364, 'Barry', 'Barry.png', 'png', 'image', 'uploads/media/2021/', '1261621', '2021-09-23 12:56:30'),
(365, 'Shakesphere', 'Shakesphere.png', 'png', 'image', 'uploads/media/2021/', '1346081', '2021-09-23 12:56:31'),
(366, 'New_Arrival_-_Knowledge_Book_(3)', 'New_Arrival_-_Knowledge_Book_(3).png', 'png', 'image', 'uploads/media/2021/', '1078513', '2021-09-23 12:56:32'),
(367, 'big_notebook', 'big_notebook.png', 'png', 'image', 'uploads/media/2021/', '1082388', '2021-09-23 12:56:33'),
(368, 'Science_c', 'Science_c.png', 'png', 'image', 'uploads/media/2021/', '1531770', '2021-09-23 12:56:34'),
(369, 'Mighty', 'Mighty.png', 'png', 'image', 'uploads/media/2021/', '1538722', '2021-09-23 12:56:35'),
(370, 'Gleny', 'Gleny.png', 'png', 'image', 'uploads/media/2021/', '1223559', '2021-09-23 12:56:36'),
(371, 'Merlin', 'Merlin.png', 'png', 'image', 'uploads/media/2021/', '1347654', '2021-09-23 12:56:37'),
(372, 'Magic', 'Magic.png', 'png', 'image', 'uploads/media/2021/', '1351003', '2021-09-23 12:56:39'),
(373, 'My_weird_school_1', 'My_weird_school_1.png', 'png', 'image', 'uploads/media/2021/', '1328350', '2021-09-23 12:57:10'),
(374, 'Gew', 'Gew.png', 'png', 'image', 'uploads/media/2021/', '1347161', '2021-09-23 12:57:11'),
(375, 'Andrew', 'Andrew.png', 'png', 'image', 'uploads/media/2021/', '1320283', '2021-09-23 12:57:12'),
(376, 'Henry', 'Henry.png', 'png', 'image', 'uploads/media/2021/', '1299430', '2021-09-23 12:57:13'),
(377, 'Oxford', 'Oxford.png', 'png', 'image', 'uploads/media/2021/', '947524', '2021-09-23 12:57:14'),
(378, 'Winnie', 'Winnie.png', 'png', 'image', 'uploads/media/2021/', '1348694', '2021-09-23 12:57:15'),
(379, 'PoV', 'PoV.png', 'png', 'image', 'uploads/media/2021/', '1153971', '2021-09-23 12:57:51'),
(380, 'New_Arrival_-_Knowledge_Book_(2)', 'New_Arrival_-_Knowledge_Book_(2).png', 'png', 'image', 'uploads/media/2021/', '1183759', '2021-09-23 12:57:53'),
(381, 'Little_guide_to_great_live', 'Little_guide_to_great_live.png', 'png', 'image', 'uploads/media/2021/', '1237317', '2021-09-23 12:57:54'),
(382, 'Little_ppl,_big_dream', 'Little_ppl,_big_dream.png', 'png', 'image', 'uploads/media/2021/', '1029865', '2021-09-23 12:57:55'),
(383, 'Comic_Book_(1)', 'Comic_Book_(1).png', 'png', 'image', 'uploads/media/2021/', '1478872', '2021-09-23 12:57:56'),
(384, 'Young_Adult_Fiction_New_Arrival_-_10+_(4)', 'Young_Adult_Fiction_New_Arrival_-_10+_(4).png', 'png', 'image', 'uploads/media/2021/', '1128286', '2021-09-23 12:57:57'),
(385, 'The_school_good', 'The_school_good.png', 'png', 'image', 'uploads/media/2021/', '1104196', '2021-09-23 12:57:59'),
(386, 'Owl_Diaries', 'Owl_Diaries.png', 'png', 'image', 'uploads/media/2021/', '1166770', '2021-09-23 12:58:00'),
(387, 'David_walliam_colour', 'David_walliam_colour.png', 'png', 'image', 'uploads/media/2021/', '1100416', '2021-09-23 12:58:01'),
(388, 'Anna_of_green_shop', 'Anna_of_green_shop.png', 'png', 'image', 'uploads/media/2021/', '1277282', '2021-09-23 12:58:38'),
(389, 'The_world_worst_children_Shop', 'The_world_worst_children_Shop.png', 'png', 'image', 'uploads/media/2021/', '1259857', '2021-09-23 12:58:39'),
(390, 'Roald_D', 'Roald_D.png', 'png', 'image', 'uploads/media/2021/', '1377025', '2021-09-23 12:58:40'),
(391, 'Kidspy', 'Kidspy.png', 'png', 'image', 'uploads/media/2021/', '1158696', '2021-09-23 12:58:41'),
(392, 'Normal_Kid_Shop', 'Normal_Kid_Shop.png', 'png', 'image', 'uploads/media/2021/', '1410929', '2021-09-23 12:58:43'),
(393, 'New_Arrival_-_8+_(Girl)_(9)', 'New_Arrival_-_8+_(Girl)_(9).png', 'png', 'image', 'uploads/media/2021/', '1306451', '2021-09-23 12:59:09'),
(394, 'Dragon_master', 'Dragon_master.png', 'png', 'image', 'uploads/media/2021/', '1272149', '2021-09-23 12:59:10'),
(395, 'Eric_Vale', 'Eric_Vale.png', 'png', 'image', 'uploads/media/2021/', '1100622', '2021-09-23 12:59:11'),
(396, 'Young_Adult_Fiction_New_Arrival_-_10+_(3)', 'Young_Adult_Fiction_New_Arrival_-_10+_(3).png', 'png', 'image', 'uploads/media/2021/', '1169525', '2021-09-23 12:59:12'),
(397, 'Wonder,_Auggie_Me,_365_Days_of_Wonder_shop', 'Wonder,_Auggie_Me,_365_Days_of_Wonder_shop.png', 'png', 'image', 'uploads/media/2021/', '1125477', '2021-09-23 12:59:54'),
(398, 'Early_Reader_(7)', 'Early_Reader_(7).png', 'png', 'image', 'uploads/media/2021/', '1107924', '2021-09-23 13:36:07'),
(399, 'Comic_Book_(3)', 'Comic_Book_(3).png', 'png', 'image', 'uploads/media/2021/', '1280469', '2021-09-23 13:52:32'),
(400, 'Wing_of_fire_(2)', 'Wing_of_fire_(2).png', 'png', 'image', 'uploads/media/2021/', '1151919', '2021-09-23 15:44:39'),
(401, '17', '17.png', 'png', 'image', 'uploads/media/2021/', '443965', '2021-09-23 16:15:34'),
(402, '215', '215.png', 'png', 'image', 'uploads/media/2021/', '368354', '2021-09-23 16:15:35'),
(405, '54', '54.png', 'png', 'image', 'uploads/media/2021/', '432945', '2021-09-23 16:15:38'),
(406, 'Comic_book_(4)', 'Comic_book_(4).png', 'png', 'image', 'uploads/media/2021/', '422152', '2021-09-23 16:21:49'),
(407, 'Knowledge', 'Knowledge.png', 'png', 'image', 'uploads/media/2021/', '369674', '2021-09-23 16:21:50'),
(408, 'Zack', 'Zack.png', 'png', 'image', 'uploads/media/2021/', '1363217', '2021-09-24 03:03:39'),
(409, 'Sendak_(2)', 'Sendak_(2).png', 'png', 'image', 'uploads/media/2021/', '892250', '2021-09-24 03:47:20'),
(410, 'Fiction', 'Fiction.png', 'png', 'image', 'uploads/media/2021/', '421283', '2021-09-24 05:54:55'),
(411, 'Early_Reader_(8)', 'Early_Reader_(8).png', 'png', 'image', 'uploads/media/2021/', '467922', '2021-09-24 05:54:56'),
(412, 'New_Arrival_-_8+_(Girl)_(13)', 'New_Arrival_-_8+_(Girl)_(13).png', 'png', 'image', 'uploads/media/2021/', '1150857', '2021-09-24 06:29:37'),
(413, 'Early_Reader_(10)', 'Early_Reader_(10).png', 'png', 'image', 'uploads/media/2021/', '1144973', '2021-09-24 08:07:27'),
(414, 'Big_Nate', 'Big_Nate.jpg', 'jpg', 'image', 'uploads/media/2021/', '216384', '2021-09-24 12:28:15'),
(415, 'andrew', 'andrew.jpg', 'jpg', 'image', 'uploads/media/2021/', '126164', '2021-09-24 12:36:13'),
(416, 'A_case_of_good_manner', 'A_case_of_good_manner.jpg', 'jpg', 'image', 'uploads/media/2021/', '75905', '2021-09-24 12:42:36'),
(417, 'Dogman11', 'Dogman11.jpg', 'jpg', 'image', 'uploads/media/2021/', '146075', '2021-09-24 12:46:52'),
(418, 'David_Walliams_1', 'David_Walliams_1.jpg', 'jpg', 'image', 'uploads/media/2021/', '430848', '2021-09-24 12:55:51'),
(419, 'Usborne_for_beginer', 'Usborne_for_beginer.jpg', 'jpg', 'image', 'uploads/media/2021/', '83579', '2021-09-24 13:32:36'),
(420, 'Who_would_win', 'Who_would_win.jpg', 'jpg', 'image', 'uploads/media/2021/', '10601', '2021-09-24 13:43:55'),
(421, 'Elephant_Piggie', 'Elephant_Piggie.jpg', 'jpg', 'image', 'uploads/media/2021/', '116314', '2021-09-24 13:47:46'),
(422, 'How_to_train_the_dragon_(2)', 'How_to_train_the_dragon_(2).png', 'png', 'image', 'uploads/media/2021/', '1338861', '2021-09-24 13:52:34'),
(423, 'Tom_gates', 'Tom_gates.jpg', 'jpg', 'image', 'uploads/media/2021/', '150561', '2021-09-24 13:55:13'),
(424, 'Timmy', 'Timmy.jpg', 'jpg', 'image', 'uploads/media/2021/', '212720', '2021-09-24 13:58:35'),
(425, 'New_Arrival_-_8+_(Girl)_(14)', 'New_Arrival_-_8+_(Girl)_(14).png', 'png', 'image', 'uploads/media/2021/', '1285794', '2021-09-24 14:06:18'),
(426, 'New_Arrival_-_8+_(Girl)_(5)', 'New_Arrival_-_8+_(Girl)_(5).png', 'png', 'image', 'uploads/media/2021/', '1289134', '2021-09-24 14:14:09'),
(427, '81wgqiGzAZL', '81wgqiGzAZL.jpg', 'jpg', 'image', 'uploads/media/2021/', '245581', '2021-09-24 14:18:34'),
(428, 'New_Arrival_-_Knowledge_Book_(5)', 'New_Arrival_-_Knowledge_Book_(5).png', 'png', 'image', 'uploads/media/2021/', '1127786', '2021-09-24 14:26:08'),
(429, 'New_Arrival_-_8+_(Girl)_(16)', 'New_Arrival_-_8+_(Girl)_(16).png', 'png', 'image', 'uploads/media/2021/', '1207232', '2021-09-24 14:43:39'),
(430, 'New_Arrival_-_8+_(Girl)_(15)', 'New_Arrival_-_8+_(Girl)_(15).png', 'png', 'image', 'uploads/media/2021/', '1267247', '2021-09-24 14:52:28'),
(431, '8c0fa43f8eb25c9738489ab76c0e4891', '8c0fa43f8eb25c9738489ab76c0e4891.jpg', 'jpg', 'image', 'uploads/media/2021/', '176045', '2021-09-24 14:54:30'),
(432, '1604645456731_grande', '1604645456731_grande.jpg', 'jpg', 'image', 'uploads/media/2021/', '75489', '2021-09-24 15:05:56'),
(433, 'a4d3890d343d9b84ae15b869dddd0113', 'a4d3890d343d9b84ae15b869dddd0113.jpg', 'jpg', 'image', 'uploads/media/2021/', '88897', '2021-09-24 15:48:10'),
(434, 'New_Arrival_-_8+_(Girl)_(17)', 'New_Arrival_-_8+_(Girl)_(17).png', 'png', 'image', 'uploads/media/2021/', '1358584', '2021-09-24 15:58:59'),
(435, 'Comic_Book_(5)', 'Comic_Book_(5).png', 'png', 'image', 'uploads/media/2021/', '1289597', '2021-09-24 16:01:44'),
(436, 'Logo-Mark', 'Logo-Mark.png', 'png', 'image', 'uploads/media/2021/', '322942', '2021-09-29 06:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_sent` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `message`, `type`, `type_id`, `image`, `date_sent`) VALUES
(1, 'testing', 'testing message from Voathnak', 'default', 0, NULL, '2021-06-20 08:31:56'),
(2, '2nd testing', 'testing push notification 2nd time', 'default', 0, NULL, '2021-06-20 08:45:21'),
(3, 'test@@@@', 'sdsadasdas', 'default', 0, NULL, '2021-07-04 09:58:41'),
(4, 'test from voathnak', 'testing push notification', 'default', 0, NULL, '2021-10-02 11:59:09'),
(5, 'test from voathnak', 'testing apple push notification', 'default', 0, NULL, '2021-10-02 12:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT 0,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `delivery_boy_id` int(11) DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` double NOT NULL,
  `delivery_charge` double DEFAULT 0,
  `wallet_balance` double DEFAULT 0,
  `total_payable` double DEFAULT NULL,
  `promo_code` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_discount` double DEFAULT NULL,
  `discount` double DEFAULT 0,
  `final_total` double DEFAULT NULL,
  `payment_method` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_time` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `status` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `otp` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `delivery_boy_id`, `mobile`, `total`, `delivery_charge`, `wallet_balance`, `total_payable`, `promo_code`, `promo_discount`, `discount`, `final_total`, `payment_method`, `latitude`, `longitude`, `address`, `delivery_time`, `delivery_date`, `status`, `active_status`, `date_added`, `otp`) VALUES
(1, 2, NULL, '89243003', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '37.785834', '-122.406417', 'PP, CA, United States, 94108', NULL, NULL, '[[\"received\",\"20-06-2021 03:26:19pm\"]]', 'received', '2021-06-20 08:26:19', 492638),
(2, 3, NULL, '087914999', 20, 0, 0, 20, ' ', 0, 0, 20, 'COD', '11.556033', '104.91218279999998', '173, Phnom Penh, Cambodia, 12253', NULL, NULL, '[[\"received\",\"20-06-2021 03:43:10pm\"]]', 'received', '2021-06-20 08:43:10', 677444),
(3, 3, NULL, '087914999', 3, 0, 0, 3, ' ', 0, 0, 3, 'COD', '11.545732352660286', '104.91705615073442', '756, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"20-06-2021 07:21:55pm\"],[\"processed\",\"2021-06-20 19:25:21\"],[\"shipped\",\"2021-06-20 19:25:21\"]]', 'shipped', '2021-06-20 12:21:55', 891059),
(4, 2, NULL, '89243003', 6, 0, 0, 6, ' ', 0, 0, 6, 'COD', '37.785834', '-122.406417', 'PP, CA, United States, 94108', NULL, NULL, '[[\"received\",\"20-06-2021 09:19:51pm\"]]', 'received', '2021-06-20 14:19:51', 804851),
(5, 3, NULL, '087914999', 3, 0, 0, 3, ' ', 0, 0, 3, 'COD', '11.556033', '104.91218279999998', '173, Phnom Penh, Cambodia, 12253', NULL, NULL, '[[\"received\",\"21-06-2021 10:22:39am\"]]', 'received', '2021-06-21 03:22:39', 751982),
(6, 3, NULL, '087914999', 15, 0, 0, 15, ' ', 0, 0, 15, 'COD', '11.556033', '104.91218279999998', '173, Phnom Penh, Cambodia, 12253', NULL, NULL, '[[\"received\",\"21-06-2021 09:50:38pm\"]]', 'received', '2021-06-21 14:50:38', 594168),
(7, 2, NULL, '89243003', 13, 0, 0, 13, ' ', 0, 0, 13, 'COD', '37.785834', '-122.406417', 'PP, CA, United States, 94108', NULL, NULL, '[[\"received\",\"22-06-2021 11:21:54am\"]]', 'received', '2021-06-22 04:21:54', 746006),
(8, 2, NULL, '89243003', 10, 0, 0, 10, ' ', 0, 0, 10, 'COD', '37.785834', '-122.406417', 'PP, CA, United States, 94108', NULL, NULL, '[[\"received\",\"22-06-2021 11:22:37am\"]]', 'received', '2021-06-22 04:22:37', 714062),
(9, 2, NULL, '89243003', 40, 0, 0, 40, ' ', 0, 0, 40, 'COD', '37.785834', '-122.406417', 'PP, CA, United States, 94108', NULL, NULL, '[[\"received\",\"22-06-2021 11:23:27am\"],[\"processed\",\"2021-06-22 12:31:36\"],[\"shipped\",\"2021-06-22 12:31:36\"],[\"delivered\",\"2021-06-22 12:31:36\"]]', 'delivered', '2021-06-22 04:23:27', 160192),
(10, 3, NULL, '087914999', 10, 0, 0, 10, ' ', 0, 0, 10, 'COD', '11.5550538', '104.9120683', 'Test, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"23-06-2021 10:04:34am\"]]', 'received', '2021-06-23 03:04:34', 243555),
(11, 2, NULL, '89243003', 6, 0, 0, 6, ' ', 0, 0, 6, 'COD', '37.785834', '-122.406417', 'PP, CA, United States, 94108', NULL, NULL, '[[\"received\",\"23-06-2021 10:04:35am\"]]', 'received', '2021-06-23 03:04:35', 913641),
(12, 7, NULL, '085896069', 45, 0, 0, 45, ' ', 0, 0, 45, 'COD', '37.4219983', '-122.084', 'Phnom Penh, California, United States, 94043', NULL, NULL, '[[\"received\",\"26-06-2021 06:20:46pm\"]]', 'received', '2021-06-26 11:20:46', 813302),
(13, 7, NULL, '085896069', 40, 0, 0, 40, ' ', 0, 0, 40, 'COD', '37.4219983', '-122.084', 'Phnom Penh, California, United States, 94043', NULL, NULL, '[[\"received\",\"27-06-2021 09:04:10am\"]]', 'received', '2021-06-27 02:04:10', 904159),
(14, 13, NULL, '967988599', 25, 0, 0, 25, ' ', 0, 0, 25, 'COD', '11.5548155', '104.91180359999998', 'pp, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"05-07-2021 04:04:49pm\"]]', 'received', '2021-07-05 09:04:49', 475124),
(15, 10, NULL, '16775063', 25, 0, 0, 25, ' ', 0, 0, 25, 'COD', '11.5552598', '104.9121017', 'kpc, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"07-07-2021 01:34:56pm\"]]', 'received', '2021-07-07 06:34:56', 296888),
(50, 16, NULL, '85896069', 35, 0, 0, 35, ' ', 0, 0, 35, 'COD', '11.555156955426488', '104.91212122142315', 'Saint 173, #1E0, Khan Chamkar Mon, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"31-08-2021 02:28:04pm\"]]', 'received', '2021-08-31 07:28:04', 289869),
(49, 10, NULL, '16775063', 20, 0, 0, 20, ' ', 0, 0, 20, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"30-08-2021 12:08:08am\"]]', 'received', '2021-08-29 17:08:08', 626269),
(48, 11, NULL, '11922024', 38, 0, 0, 38, ' ', 0, 0, 38, 'COD', '11.569522040500877', '104.88987162709236', 'Building D, Building D, Khan Tuol Kouk, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"29-08-2021 11:32:49am\"]]', 'received', '2021-08-29 04:32:49', 991702),
(20, 10, NULL, '16775063', 25, 0, 0, 25, ' ', 0, 0, 25, 'COD', '11.5552598', '104.9121017', 'kpc, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"10-07-2021 09:57:23am\"]]', 'received', '2021-07-10 02:57:23', 818053),
(21, 10, NULL, '16775063', 25, 0, 0, 25, ' ', 0, 0, 25, 'COD', '11.5552598', '104.9121017', 'kpc, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"10-07-2021 09:58:17am\"]]', 'received', '2021-07-10 02:58:17', 924370),
(39, 16, NULL, '85896069', 25, 0, 0, 25, ' ', 0, 0, 25, 'COD', '11.550061185533592', '104.9010618031025', 'Phom Penh, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"11-07-2021 04:21:13pm\"]]', 'received', '2021-07-11 09:21:13', 109245),
(24, 10, NULL, '16775063', 25, 0, 0, 25, ' ', 0, 0, 25, 'COD', '11.5552598', '104.9121017', 'kpc, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"10-07-2021 10:49:39am\"]]', 'received', '2021-07-10 03:49:39', 806960),
(45, 11, NULL, '11922024', 65, 0, 0, 65, ' ', 0, 0, 65, 'COD', '11.569522040500877', '104.88987162709236', 'Building D, Building D, Khan Tuol Kouk, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"04-08-2021 03:27:00pm\"]]', 'received', '2021-08-04 08:27:00', 718254),
(44, 11, NULL, '11922024', 14, 0, 0, 14, ' ', 0, 0, 14, 'COD', '11.568883505735437', '104.88001350313425', 'Unnamed Road, Unnamed Road, Khan Sensok, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"31-07-2021 09:51:31am\"]]', 'received', '2021-07-31 02:51:31', 684639),
(40, 17, NULL, '87914999', 18.9, 0, 0, 18.9, ' ', 0, 0, 18.9, 'COD', '11.5556871', '104.9122218', '313, 313 Preah Sihanouk Blvd (274), Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, 12253', NULL, NULL, '[[\"received\",\"18-07-2021 09:10:48pm\"]]', 'received', '2021-07-18 14:10:48', 109404),
(41, 18, NULL, '316000072', 107.8, 0, 0, 107.8, ' ', 0, 0, 107.8, 'COD', '11.5676877', '104.9064347', '500, 534 Kampuchea Krom Blvd (128), Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"18-07-2021 09:36:31pm\"]]', 'received', '2021-07-18 14:36:31', 589597),
(30, 11, NULL, '11922024', 25, 0, 0, 25, ' ', 0, 0, 25, 'COD', '11.550061185533592', '104.9010618031025', 'Phom Penh, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"10-07-2021 03:15:26pm\"]]', 'received', '2021-07-10 08:15:26', 378670),
(46, 11, NULL, '11922024', 44.8, 0, 0, 44.8, ' ', 0, 0, 44.8, 'COD', '11.569522040500877', '104.88987162709236', 'Building D, Building D, Khan Tuol Kouk, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"04-08-2021 03:37:05pm\"]]', 'received', '2021-08-04 08:37:05', 453412),
(47, 11, NULL, '11922024', 34.9, 0, 0, 34.9, ' ', 0, 0, 34.9, 'COD', '11.569522040500877', '104.88987162709236', 'Building D, Building D, Khan Tuol Kouk, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"07-08-2021 03:23:16pm\"],[\"processed\",\"2021-08-07 15:24:13\"],[\"shipped\",\"2021-08-07 15:24:13\"],[\"delivered\",\"2021-08-07 15:24:13\"]]', 'delivered', '2021-08-07 08:23:16', 211337),
(36, 11, NULL, '11922024', 10, 10, 0, 10, ' ', 0, 0, 0, 'COD', '11.550061185533592', '104.9010618031025', 'Phom Penh, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"10-07-2021 03:46:13pm\"],[\"cancelled\",\"2021-07-11 09:37:45\"]]', 'cancelled', '2021-07-10 08:46:13', 386778),
(43, 11, NULL, '11922024', 37.8, 0, 0, 37.8, ' ', 0, 0, 37.8, 'COD', '11.555094544056265', '104.91213731467724', '7a, 7a St 173, Khan Chamkar Mon, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"21-07-2021 04:50:19pm\"]]', 'received', '2021-07-21 09:50:19', 369376),
(38, 11, NULL, '11922024', 12, 0, 0, 12, ' ', 0, 0, 12, 'COD', '11.550061185533592', '104.9010618031025', 'Phom Penh, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"10-07-2021 04:49:08pm\"],[\"processed\",\"2021-07-11 08:33:19\"],[\"shipped\",\"2021-07-11 08:33:19\"],[\"delivered\",\"2021-07-11 08:33:19\"]]', 'delivered', '2021-07-10 09:49:08', 244774),
(42, 11, NULL, '11922024', 45, 0, 0, 45, ' ', 0, 0, 45, 'COD', '11.555094544056265', '104.91213731467724', '7a, 7a St 173, Khan Chamkar Mon, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"21-07-2021 04:38:44pm\"]]', 'received', '2021-07-21 09:38:44', 993937),
(51, 16, NULL, '85896069', 54.9, 0, 0, 54.9, ' ', 0, 0, 54.9, 'COD', '37.41868571646587', '-122.08339937031268', '1058, 1058 Huff Ave, , Mountain View, California, United States, California, United States, 94043', NULL, NULL, '[[\"received\",\"04-09-2021 10:54:47am\"]]', 'received', '2021-09-04 03:54:47', 761865),
(52, 21, NULL, '69850168', 71.8, 0, 0, 71.8, ' ', 0, 0, 71.8, 'COD', '11.5284388', '104.95956990000002', 'Street P-14, No.132, Khan Chbar Ampov, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"12-09-2021 11:49:00am\"]]', 'received', '2021-09-12 04:49:00', 585763),
(53, 21, NULL, '69850168', 70, 0, 0, 70, ' ', 0, 0, 70, 'COD', '11.5284608', '104.9595802', 'Gg, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"12-09-2021 11:50:31am\"]]', 'received', '2021-09-12 04:50:31', 249468),
(54, 5, NULL, '69860168', 85, 0, 0, 85, ' ', 0, 0, 85, 'COD', '11.5284728', '104.9595884', 'Street P-10B, Street P-10B, Khan Chbar Ampov, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-09-2021 01:23:44pm\"]]', 'received', '2021-09-14 06:23:44', 243940),
(55, 22, NULL, '963885245', 14, 0, 0, 14, ' ', 0, 0, 14, 'COD', '11.5557849', '104.88804970000001', 'Phnom Penh, Borey Piphop Thmey, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"19-09-2021 01:44:42pm\"]]', 'received', '2021-09-19 06:44:42', 186558),
(56, 22, NULL, '963885245', 75, 0, 0, 75, ' ', 0, 0, 75, 'COD', '11.5557849', '104.88804970000001', 'Phnom Penh, Borey Piphop Thmey, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"25-09-2021 07:10:52pm\"]]', 'received', '2021-09-25 12:10:52', 425289),
(57, 22, NULL, '963885245', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.5557849', '104.88804970000001', 'Phnom Penh, Borey Piphop Thmey, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"25-09-2021 07:13:31pm\"]]', 'received', '2021-09-25 12:13:31', 722352),
(58, 4, NULL, '12979873', 60, 0, 0, 60, ' ', 0, 0, 60, 'COD', '11.565776868794384', '104.90972768515348', 'Borei Keila Building D, Borei Keila Building D, Khan 7 Makara, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"01-10-2021 10:28:09pm\"]]', 'received', '2021-10-01 15:28:09', 351536),
(59, 23, NULL, '99914999', 60, 0, 0, 60, ' ', 0, 0, 60, 'COD', '11.555193572165926', '104.9120754731789', 'Street 173, Street 173, , Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"03-10-2021 04:16:05pm\"]]', 'received', '2021-10-03 09:16:05', 278297),
(60, 2, NULL, '89243003', 98, 0, 0, 98, ' ', 0, 0, 98, 'COD', '37.785834', '-122.406417', '1 Stockton St, 1 Stockton St, Union Square, San Francisco, CA, United States, CA, United States, 94108', NULL, NULL, '[[\"received\",\"10-10-2021 01:39:02am\"]]', 'received', '2021-10-09 18:39:02', 353214),
(61, 24, NULL, '70905565', 38, 0, 0, 38, ' ', 0, 0, 38, 'COD', '11.555845859806876', '104.88801739810647', 'Khan Tuol Kouk, , Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"12-10-2021 12:21:14am\"]]', 'received', '2021-10-11 17:21:14', 726476),
(62, 24, NULL, '70905565', 60, 0, 0, 60, ' ', 0, 0, 60, 'COD', '11.555845859806876', '104.88801739810647', 'Khan Tuol Kouk, , Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"12-10-2021 12:53:51am\"]]', 'received', '2021-10-11 17:53:51', 452467),
(63, 24, NULL, '70905565', 40, 0, 0, 40, ' ', 0, 0, 40, 'COD', '11.555845859806876', '104.88801739810647', 'Khan Tuol Kouk, , Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"13-10-2021 07:36:52pm\"]]', 'received', '2021-10-13 12:36:52', 683701),
(64, 2, NULL, '89243003', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '37.785834', '-122.406417', '1 Stockton St, 1 Stockton St, Union Square, San Francisco, CA, United States, CA, United States, 94108', NULL, NULL, '[[\"received\",\"13-10-2021 11:01:19pm\"]]', 'received', '2021-10-13 16:01:19', 730739),
(65, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:15:32am\"]]', 'received', '2021-10-14 04:15:32', 686338),
(66, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:19:15am\"]]', 'received', '2021-10-14 04:19:15', 576708),
(67, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:30:40am\"]]', 'received', '2021-10-14 04:30:40', 452806),
(68, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:34:36am\"]]', 'received', '2021-10-14 04:34:36', 580179),
(69, 10, NULL, '16775063', 60, 0, 0, 60, ' ', 0, 0, 60, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:37:27am\"]]', 'received', '2021-10-14 04:37:27', 708747),
(70, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:38:31am\"]]', 'received', '2021-10-14 04:38:31', 505751),
(71, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:39:16am\"]]', 'received', '2021-10-14 04:39:16', 307992),
(72, 10, NULL, '16775063', 60, 0, 0, 60, ' ', 0, 0, 60, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:40:19am\"]]', 'received', '2021-10-14 04:40:19', 958435),
(73, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:42:08am\"]]', 'received', '2021-10-14 04:42:08', 314047),
(74, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:43:09am\"]]', 'received', '2021-10-14 04:43:09', 723228),
(75, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 11:44:09am\"]]', 'received', '2021-10-14 04:44:09', 465095),
(76, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"14-10-2021 03:06:17pm\"]]', 'received', '2021-10-14 08:06:17', 828750),
(77, 10, NULL, '16775063', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.55736400962833', '104.91842810064554', '302a, 302a St 111, Khan 7 Makara, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"17-10-2021 09:17:12am\"]]', 'received', '2021-10-17 02:17:12', 446819),
(78, 4, NULL, '12979873', 115, 0, 0, 115, ' ', 0, 0, 115, 'COD', '11.565776868794384', '104.90972768515348', 'Borei Keila Building D, Borei Keila Building D, Khan 7 Makara, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"20-10-2021 08:40:11pm\"]]', 'received', '2021-10-20 13:40:11', 211711),
(79, 4, NULL, '12979873', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.565776868794384', '104.90972768515348', 'Borei Keila Building D, Borei Keila Building D, Khan 7 Makara, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"20-10-2021 08:41:05pm\"]]', 'received', '2021-10-20 13:41:05', 930599),
(80, 2, NULL, '89243003', 30, 0, 0, 30, ' ', 0, 0, 30, 'COD', '11.56804099235753', '104.90606881678106', 'Street 122, Street 122, Khan Tuol Kouk, Phnom Penh, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"24-10-2021 12:54:17pm\"]]', 'received', '2021-10-24 05:54:17', 733354),
(81, 4, NULL, '12979873', 98, 0, 0, 98, ' ', 0, 0, 98, 'COD', '11.565776868794384', '104.90972768515348', 'Borei Keila Building D, Borei Keila Building D, Khan 7 Makara, Phnom Penh, Cambodia, Phnom Penh, Cambodia, ', NULL, NULL, '[[\"received\",\"25-10-2021 04:55:40pm\"]]', 'received', '2021-10-25 09:55:40', 119966);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_name` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant_name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variant_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `discounted_price` double DEFAULT NULL,
  `tax_percent` double DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `discount` double DEFAULT 0,
  `sub_total` double NOT NULL,
  `deliver_by` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `user_id`, `order_id`, `product_name`, `variant_name`, `product_variant_id`, `quantity`, `price`, `discounted_price`, `tax_percent`, `tax_amount`, `discount`, `sub_total`, `deliver_by`, `status`, `active_status`, `date_added`) VALUES
(1, 2, 1, 'T - Taxing Track', '', 253, 3, 10, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"20-06-2021 03:26:19pm\"]]', 'received', '2021-06-20 08:26:19'),
(2, 3, 2, 'T - Taxing Track', '', 253, 2, 10, NULL, 0, 0, 0, 20, NULL, '[[\"received\",\"20-06-2021 03:43:10pm\"]]', 'received', '2021-06-20 08:43:10'),
(3, 3, 3, 'Terminator', '', 254, 1, 3, NULL, 0, 0, 0, 3, NULL, '[[\"received\",\"20-06-2021 07:21:55pm\"],[\"processed\",\"2021-06-20 19:25:21\"],[\"shipped\",\"2021-06-20 19:25:21\"]]', 'shipped', '2021-06-20 12:21:55'),
(4, 2, 4, 'Terminator', '', 254, 2, 3, NULL, 0, 0, 0, 6, NULL, '[[\"received\",\"20-06-2021 09:19:51pm\"]]', 'received', '2021-06-20 14:19:51'),
(5, 3, 5, 'Terminator', '', 254, 1, 3, NULL, 0, 0, 0, 3, NULL, '[[\"received\",\"21-06-2021 10:22:39am\"]]', 'received', '2021-06-21 03:22:39'),
(6, 3, 6, 'Naruto small set', '', 210, 1, 15, NULL, 0, 0, 0, 15, NULL, '[[\"received\",\"21-06-2021 09:50:38pm\"]]', 'received', '2021-06-21 14:50:38'),
(7, 2, 7, 'Terminator', '', 254, 1, 3, NULL, 1, 0, 0, 3, NULL, '[[\"received\",\"22-06-2021 11:21:54am\"]]', 'received', '2021-06-22 04:21:54'),
(8, 2, 7, 'T - Taxing Track', '', 253, 1, 10, NULL, 0, 0, 0, 10, NULL, '[[\"received\",\"22-06-2021 11:21:54am\"]]', 'received', '2021-06-22 04:21:54'),
(9, 2, 8, 'Sample - Spiderman Car', '', 240, 1, 10, NULL, 0, 0, 0, 10, NULL, '[[\"received\",\"22-06-2021 11:22:37am\"]]', 'received', '2021-06-22 04:22:37'),
(10, 2, 9, 'B - A Classic Case of Dr. Seuss 1-20', '', 3, 1, 40, NULL, 0, 0, 0, 40, NULL, '[[\"received\",\"22-06-2021 11:23:27am\"],[\"processed\",\"2021-06-22 12:31:36\"],[\"shipped\",\"2021-06-22 12:31:36\"],[\"delivered\",\"2021-06-22 12:31:36\"]]', 'delivered', '2021-06-22 04:23:27'),
(11, 3, 10, 'T - Taxing Track', '', 253, 1, 10, NULL, 0, 0, 0, 10, NULL, '[[\"received\",\"23-06-2021 10:04:34am\"]]', 'received', '2021-06-23 03:04:34'),
(12, 2, 11, 'M - 6134', '', 180, 1, 6, NULL, 0, 0, 0, 6, NULL, '[[\"received\",\"23-06-2021 10:04:35am\"]]', 'received', '2021-06-23 03:04:35'),
(13, 7, 12, 'M - Cartoon house', '', 191, 1, 15, NULL, 0, 0, 0, 15, NULL, '[[\"received\",\"26-06-2021 06:20:46pm\"]]', 'received', '2021-06-26 11:20:46'),
(14, 7, 12, 'T - Build & Play', '', 249, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"26-06-2021 06:20:46pm\"]]', 'received', '2021-06-26 11:20:46'),
(15, 7, 13, 'M - Cartoon house', '', 191, 1, 15, NULL, 0, 0, 0, 15, NULL, '[[\"received\",\"27-06-2021 09:04:10am\"]]', 'received', '2021-06-27 02:04:10'),
(16, 7, 13, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"27-06-2021 09:04:10am\"]]', 'received', '2021-06-27 02:04:10'),
(17, 13, 14, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"05-07-2021 04:04:49pm\"]]', 'received', '2021-07-05 09:04:49'),
(18, 10, 15, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"07-07-2021 01:34:56pm\"]]', 'received', '2021-07-07 06:34:56'),
(19, 11, 16, 'Magic Table', '', 208, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 09:05:21am\"]]', 'received', '2021-07-10 02:05:21'),
(20, 11, 17, 'M - Ultraman', '', 203, 1, 9, NULL, 0, 0, 0, 9, NULL, '[[\"received\",\"10-07-2021 09:55:21am\"]]', 'received', '2021-07-10 02:55:21'),
(21, 11, 18, 'M - Sambo Block Robot', '', 199, 1, 12, NULL, 0, 0, 0, 12, NULL, '[[\"received\",\"10-07-2021 09:56:04am\"]]', 'received', '2021-07-10 02:56:04'),
(22, 11, 19, 'Magic Table', '', 208, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 09:57:04am\"]]', 'received', '2021-07-10 02:57:04'),
(23, 10, 20, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"10-07-2021 09:57:23am\"]]', 'received', '2021-07-10 02:57:23'),
(24, 10, 21, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"10-07-2021 09:58:17am\"]]', 'received', '2021-07-10 02:58:17'),
(25, 11, 22, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"10-07-2021 10:09:13am\"]]', 'received', '2021-07-10 03:09:13'),
(26, 11, 23, 'T - Build & Play', '', 249, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 10:10:11am\"]]', 'received', '2021-07-10 03:10:11'),
(27, 10, 24, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"10-07-2021 10:49:39am\"]]', 'received', '2021-07-10 03:49:39'),
(28, 11, 25, 'Magic Table', '', 208, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 10:59:52am\"]]', 'received', '2021-07-10 03:59:52'),
(29, 11, 26, 'Dancing Hero - Hulk', '', 141, 1, 10, NULL, 0, 0, 0, 10, NULL, '[[\"received\",\"10-07-2021 11:03:33am\"]]', 'received', '2021-07-10 04:03:33'),
(30, 11, 27, 'M - Ultraman', '', 203, 1, 9, NULL, 0, 0, 0, 9, NULL, '[[\"received\",\"10-07-2021 11:05:11am\"]]', 'received', '2021-07-10 04:05:11'),
(31, 11, 28, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"10-07-2021 03:13:49pm\"]]', 'received', '2021-07-10 08:13:49'),
(32, 11, 29, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"10-07-2021 03:14:39pm\"]]', 'received', '2021-07-10 08:14:39'),
(33, 11, 30, 'T - Inteligence Domino', '', 252, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"10-07-2021 03:15:26pm\"]]', 'received', '2021-07-10 08:15:26'),
(34, 11, 31, 'T - Build & Play', '', 249, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 03:19:11pm\"]]', 'received', '2021-07-10 08:19:11'),
(35, 11, 32, 'Magic Table', '', 208, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 03:22:51pm\"]]', 'received', '2021-07-10 08:22:51'),
(36, 11, 33, 'Magic Table', '', 208, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 03:42:31pm\"]]', 'received', '2021-07-10 08:42:31'),
(37, 11, 34, 'Magic Table', '', 208, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 03:44:17pm\"]]', 'received', '2021-07-10 08:44:17'),
(38, 11, 35, 'T - Build & Play', '', 249, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"10-07-2021 03:45:27pm\"]]', 'received', '2021-07-10 08:45:27'),
(39, 11, 36, 'M - Sambo Block Robot', '', 199, 1, 12, NULL, 0, 0, 0, 12, NULL, '[[\"received\",\"10-07-2021 03:46:13pm\"],[\"cancelled\",\"2021-07-11 09:37:45\"]]', 'cancelled', '2021-07-10 08:46:13'),
(40, 11, 37, 'M - Ultraman', '', 203, 1, 9, NULL, 0, 0, 0, 9, NULL, '[[\"received\",\"10-07-2021 04:47:52pm\"],[\"processed\",\"2021-07-11 08:37:47\"],[\"shipped\",\"2021-07-11 08:37:47\"],[\"delivered\",\"2021-07-11 08:37:47\"]]', 'delivered', '2021-07-10 09:47:52'),
(41, 11, 38, 'M - Sambo Block Robot', '', 199, 1, 12, NULL, 0, 0, 0, 12, NULL, '[[\"received\",\"10-07-2021 04:49:08pm\"],[\"processed\",\"2021-07-11 08:33:19\"],[\"shipped\",\"2021-07-11 08:33:19\"],[\"delivered\",\"2021-07-11 08:33:19\"]]', 'delivered', '2021-07-10 09:49:08'),
(42, 16, 39, 'T - Flexible Truck (192 Pieces)', '', 250, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"11-07-2021 04:21:13pm\"]]', 'received', '2021-07-11 09:21:13'),
(43, 17, 40, 'STEM - 3D Science Animations', '', 381, 1, 18.9, NULL, 0, 0, 0, 18.9, NULL, '[[\"received\",\"18-07-2021 09:10:48pm\"]]', 'received', '2021-07-18 14:10:48'),
(44, 18, 41, 'B - Points of View (1-30)', '', 37, 1, 45, NULL, 0, 0, 0, 45, NULL, '[[\"received\",\"18-07-2021 09:36:31pm\"]]', 'received', '2021-07-18 14:36:31'),
(45, 18, 41, 'Stimulation Toolbox', '', 402, 1, 18, NULL, 0, 0, 0, 18, NULL, '[[\"received\",\"18-07-2021 09:36:31pm\"]]', 'received', '2021-07-18 14:36:31'),
(46, 18, 41, 'STEM - 54 Super Scientific set', '', 382, 1, 44.8, NULL, 0, 0, 0, 44.8, NULL, '[[\"received\",\"18-07-2021 09:36:31pm\"]]', 'received', '2021-07-18 14:36:31'),
(47, 11, 42, 'Wooden - Trian (Premium)', '', 260, 1, 20, NULL, 0, 0, 0, 20, NULL, '[[\"received\",\"21-07-2021 04:38:44pm\"]]', 'received', '2021-07-21 09:38:44'),
(48, 11, 42, 'T - Flexible Truck (192 Pieces)', '', 250, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"21-07-2021 04:38:44pm\"]]', 'received', '2021-07-21 09:38:44'),
(49, 11, 43, 'STEM - Bubble Science', '', 389, 1, 18.9, NULL, 0, 0, 0, 18.9, NULL, '[[\"received\",\"21-07-2021 04:50:19pm\"]]', 'received', '2021-07-21 09:50:19'),
(50, 11, 43, 'STEM - H2O Pump', '', 392, 1, 18.9, NULL, 0, 0, 0, 18.9, NULL, '[[\"received\",\"21-07-2021 04:50:19pm\"]]', 'received', '2021-07-21 09:50:19'),
(51, 11, 44, 'W- Frog Clock', '', 403, 2, 7, NULL, 0, 0, 0, 14, NULL, '[[\"received\",\"31-07-2021 09:51:31am\"]]', 'received', '2021-07-31 02:51:31'),
(52, 11, 45, 'B- Under 2 Ages Edition 11books', '', 360, 1, 35, NULL, 0, 0, 0, 35, NULL, '[[\"received\",\"04-08-2021 03:27:00pm\"]]', 'received', '2021-08-04 08:27:00'),
(53, 11, 45, 'B- Usborne Beginners Nature', '', 361, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"04-08-2021 03:27:00pm\"]]', 'received', '2021-08-04 08:27:00'),
(54, 11, 46, 'STEM - 38+ Piano Lab', '', 380, 1, 44.8, NULL, 0, 0, 0, 44.8, NULL, '[[\"received\",\"04-08-2021 03:37:05pm\"]]', 'received', '2021-08-04 08:37:05'),
(55, 11, 47, 'STEM - Spin N Shoot', '', 399, 1, 34.9, NULL, 0, 0, 0, 34.9, NULL, '[[\"received\",\"07-08-2021 03:23:16pm\"],[\"processed\",\"2021-08-07 15:24:13\"],[\"shipped\",\"2021-08-07 15:24:13\"],[\"delivered\",\"2021-08-07 15:24:13\"]]', 'delivered', '2021-08-07 08:23:16'),
(56, 11, 48, 'B- The Zack File', '', 358, 1, 38, NULL, 0, 0, 0, 38, NULL, '[[\"received\",\"29-08-2021 11:32:49am\"]]', 'received', '2021-08-29 04:32:49'),
(57, 10, 49, 'Wooden - Trian (Premium)', '', 260, 1, 20, NULL, 0, 0, 0, 20, NULL, '[[\"received\",\"30-08-2021 12:08:08am\"]]', 'received', '2021-08-29 17:08:08'),
(58, 16, 50, 'B- Under 2 Ages Edition 11books', '', 360, 1, 35, NULL, 0, 0, 0, 35, NULL, '[[\"received\",\"31-08-2021 02:28:04pm\"]]', 'received', '2021-08-31 07:28:04'),
(59, 16, 51, 'Wooden - Rail Overpass', '', 258, 1, 20, NULL, 0, 0, 0, 20, NULL, '[[\"received\",\"04-09-2021 10:54:47am\"]]', 'received', '2021-09-04 03:54:47'),
(60, 16, 51, 'STEM - Spin N Shoot', '', 399, 1, 34.9, NULL, 0, 0, 0, 34.9, NULL, '[[\"received\",\"04-09-2021 10:54:47am\"]]', 'received', '2021-09-04 03:54:47'),
(61, 21, 52, 'STEM - Maze Challenge', '', 393, 1, 18.9, NULL, 0, 0, 0, 18.9, NULL, '[[\"received\",\"12-09-2021 11:49:00am\"]]', 'received', '2021-09-12 04:49:00'),
(62, 21, 52, 'STEM - Space Ball Collector', '', 395, 1, 34.9, NULL, 0, 0, 0, 34.9, NULL, '[[\"received\",\"12-09-2021 11:49:00am\"]]', 'received', '2021-09-12 04:49:00'),
(63, 21, 52, 'Stimulation Toolbox', '', 402, 1, 18, NULL, 0, 0, 0, 18, NULL, '[[\"received\",\"12-09-2021 11:49:00am\"]]', 'received', '2021-09-12 04:49:00'),
(64, 21, 53, 'B - A-Z Mysteries', '', 4, 1, 40, NULL, 0, 0, 0, 40, NULL, '[[\"received\",\"12-09-2021 11:50:31am\"]]', 'received', '2021-09-12 04:50:31'),
(65, 21, 53, 'B- Winnie and Wilbur 9books', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"12-09-2021 11:50:31am\"]]', 'received', '2021-09-12 04:50:31'),
(66, 5, 54, 'B - Points of View (1-30)', '', 37, 1, 45, NULL, 0, 0, 0, 45, NULL, '[[\"received\",\"14-09-2021 01:23:44pm\"]]', 'received', '2021-09-14 06:23:44'),
(67, 5, 54, 'B - A Classic Case of Dr. Seuss 1-20', '', 3, 1, 40, NULL, 0, 0, 0, 40, NULL, '[[\"received\",\"14-09-2021 01:23:44pm\"]]', 'received', '2021-09-14 06:23:44'),
(68, 22, 55, 'Painting - Rock & Galaxy', '', 214, 1, 14, NULL, 0, 0, 0, 14, NULL, '[[\"received\",\"19-09-2021 01:44:42pm\"]]', 'received', '2021-09-19 06:44:42'),
(69, 22, 56, 'Peep Inside a Fairy Tale', '', 36, 1, 45, NULL, 0, 0, 0, 45, NULL, '[[\"received\",\"25-09-2021 07:10:52pm\"]]', 'received', '2021-09-25 12:10:52'),
(70, 22, 56, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"25-09-2021 07:10:52pm\"]]', 'received', '2021-09-25 12:10:52'),
(71, 22, 57, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"25-09-2021 07:13:31pm\"]]', 'received', '2021-09-25 12:13:31'),
(72, 4, 58, 'Science Comics', '', 344, 1, 60, NULL, 0, 0, 0, 60, NULL, '[[\"received\",\"01-10-2021 10:28:09pm\"]]', 'received', '2021-10-01 15:28:09'),
(73, 23, 59, 'Science Comics', '', 344, 1, 60, NULL, 0, 0, 0, 60, NULL, '[[\"received\",\"03-10-2021 04:16:05pm\"]]', 'received', '2021-10-03 09:16:05'),
(74, 2, 60, 'A-Z Mysteries', '', 4, 1, 38, NULL, 0, 0, 0, 38, NULL, '[[\"received\",\"10-10-2021 01:39:02am\"]]', 'received', '2021-10-09 18:39:02'),
(75, 2, 60, 'Winnie and Wilbur', '', 369, 2, 30, NULL, 0, 0, 0, 60, NULL, '[[\"received\",\"10-10-2021 01:39:02am\"]]', 'received', '2021-10-09 18:39:02'),
(76, 24, 61, 'Barbara Park', '', 6, 1, 38, NULL, 0, 0, 0, 38, NULL, '[[\"received\",\"12-10-2021 12:21:14am\"]]', 'received', '2021-10-11 17:21:14'),
(77, 24, 62, 'Science Comics', '', 344, 1, 60, NULL, 0, 0, 0, 60, NULL, '[[\"received\",\"12-10-2021 12:53:51am\"]]', 'received', '2021-10-11 17:53:51'),
(78, 24, 63, 'Captain Underpants', '', 9, 1, 40, NULL, 0, 0, 0, 40, NULL, '[[\"received\",\"13-10-2021 07:36:52pm\"]]', 'received', '2021-10-13 12:36:52'),
(79, 2, 64, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"13-10-2021 11:01:19pm\"]]', 'received', '2021-10-13 16:01:19'),
(80, 10, 65, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:15:32am\"]]', 'received', '2021-10-14 04:15:32'),
(81, 10, 66, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:19:15am\"]]', 'received', '2021-10-14 04:19:15'),
(82, 10, 67, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:30:40am\"]]', 'received', '2021-10-14 04:30:40'),
(83, 10, 68, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:34:36am\"]]', 'received', '2021-10-14 04:34:36'),
(84, 10, 69, 'Winnie and Wilbur', '', 369, 2, 30, NULL, 0, 0, 0, 60, NULL, '[[\"received\",\"14-10-2021 11:37:27am\"]]', 'received', '2021-10-14 04:37:27'),
(85, 10, 70, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:38:31am\"]]', 'received', '2021-10-14 04:38:31'),
(86, 10, 71, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:39:16am\"]]', 'received', '2021-10-14 04:39:16'),
(87, 10, 72, 'Science Comics', '', 344, 1, 60, NULL, 0, 0, 0, 60, NULL, '[[\"received\",\"14-10-2021 11:40:19am\"]]', 'received', '2021-10-14 04:40:19'),
(88, 10, 73, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:42:08am\"]]', 'received', '2021-10-14 04:42:08'),
(89, 10, 74, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:43:09am\"]]', 'received', '2021-10-14 04:43:09'),
(90, 10, 75, 'The Zack File', '', 358, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 11:44:09am\"]]', 'received', '2021-10-14 04:44:09'),
(91, 10, 76, 'The Zack File', '', 358, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"14-10-2021 03:06:17pm\"]]', 'received', '2021-10-14 08:06:17'),
(92, 10, 77, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"17-10-2021 09:17:12am\"]]', 'received', '2021-10-17 02:17:12'),
(93, 4, 78, 'Science Comics', '', 344, 1, 60, NULL, 0, 0, 0, 60, NULL, '[[\"received\",\"20-10-2021 08:40:11pm\"]]', 'received', '2021-10-20 13:40:11'),
(94, 4, 78, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"20-10-2021 08:40:11pm\"]]', 'received', '2021-10-20 13:40:11'),
(95, 4, 78, 'Owl Diary', '', 35, 1, 25, NULL, 0, 0, 0, 25, NULL, '[[\"received\",\"20-10-2021 08:40:11pm\"]]', 'received', '2021-10-20 13:40:11'),
(96, 4, 79, 'The Zack File', '', 358, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"20-10-2021 08:41:05pm\"]]', 'received', '2021-10-20 13:41:05'),
(97, 2, 80, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"24-10-2021 12:54:17pm\"]]', 'received', '2021-10-24 05:54:17'),
(98, 4, 81, 'The Zack File', '', 358, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"25-10-2021 04:55:40pm\"]]', 'received', '2021-10-25 09:55:40'),
(99, 4, 81, 'A-Z Mysteries', '', 4, 1, 38, NULL, 0, 0, 0, 38, NULL, '[[\"received\",\"25-10-2021 04:55:40pm\"]]', 'received', '2021-10-25 09:55:40'),
(100, 4, 81, 'Winnie and Wilbur', '', 369, 1, 30, NULL, 0, 0, 0, 30, NULL, '[[\"received\",\"25-10-2021 04:55:40pm\"]]', 'received', '2021-10-25 09:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `payment_requests`
--

CREATE TABLE `payment_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_address` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_requested` int(11) NOT NULL,
  `remarks` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `date_added` datetime(6) NOT NULL,
  `row_order` int(11) NOT NULL,
  `stock_type` varchar(64) DEFAULT NULL,
  `tax` double NOT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `short_description` varchar(500) NOT NULL,
  `slug` varchar(20) NOT NULL,
  `indicator` int(11) NOT NULL,
  `cod_allowed` int(11) NOT NULL,
  `minimum_order_quantity` int(11) NOT NULL,
  `quantity_step_size` int(11) NOT NULL,
  `total_allowed_quantity` int(11) DEFAULT NULL,
  `is_returnable` int(11) NOT NULL,
  `is_cancelable` int(11) NOT NULL,
  `cancelable_till` int(11) DEFAULT NULL,
  `image` varchar(500) NOT NULL,
  `other_images` varchar(500) NOT NULL,
  `video_type` varchar(20) NOT NULL,
  `video` varchar(500) NOT NULL,
  `tags` varchar(500) NOT NULL,
  `warranty_period` varchar(100) NOT NULL,
  `guarantee_period` varchar(100) NOT NULL,
  `made_in` varchar(128) NOT NULL,
  `sku` varchar(128) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `rating` double NOT NULL,
  `no_of_ratings` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `status` int(11) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `date_added`, `row_order`, `stock_type`, `tax`, `type`, `name`, `short_description`, `slug`, `indicator`, `cod_allowed`, `minimum_order_quantity`, `quantity_step_size`, `total_allowed_quantity`, `is_returnable`, `is_cancelable`, `cancelable_till`, `image`, `other_images`, `video_type`, `video`, `tags`, `warranty_period`, `guarantee_period`, `made_in`, `sku`, `stock`, `availability`, `rating`, `no_of_ratings`, `description`, `status`, `category_id`) VALUES
(3, '2021-06-18 11:11:34.210231', 0, NULL, 0, 'simple_product', 'A Classic Case of Dr. Seuss', '20 Books', 'a-classic-case-of-dr', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/New_Arrival_-_8+_(Girl)_(16).png', '[]', '', '', 'Dr Seuss,Early Reader', '', '', '', NULL, 11, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;A Classic Case of Dr. Seuss is an amazing collection of 20 Books which has all rhyming books such as popular ones the cat in the hat, the Lorax, hop on pop, Horton hears a who, green eggs and ham, and more. A wonderful slipcase collection of 20 classic Dr. Seuss titles. A classic collection of twenty of Dr. Seuss\\\'s best-loved stories - in one wonderful case! &quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot', 1, 6),
(4, '2021-06-18 11:11:34.469510', 0, NULL, 0, 'simple_product', 'A-Z Mysteries', '26 Books', 'a-z-mysteries', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Az_Mysteries_(2).png', '[]', '', '', 'Mystery', '', '', '', NULL, 14, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;This series features three smart kids -Dink Duncan, Josh Pinto, and Ruth Rose Hathaway who solve crimes and mysteries. Interesting and fun to read, this series suitable for kids from 7 - 10 years old.\\\\r\\\\n&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0', 1, 4),
(5, '2021-06-18 11:11:34.701904', 0, NULL, 0, 'simple_product', 'Bad Guys', '8 Books', 'bad-guys', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Bad_G.png', '[]', '', '', 'Comic,Dav Pikey', '', '', '', NULL, 11, 1, 5, 1, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;\\\\&quot;I wish I\\\'d had these books as a kid. Hilarious!\\\\&quot; -- Dav Pilkey, creator of Captain Underpants and Dog Man\\\\r\\\\n\\\\r\\\\nThey may look like Bad Guys, but these wannabe heroes are doing good deeds...whether you like it or not! This New York Times bestselling illustrated series is perfect for fans of Dog Man and Captain Underpants.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5', 1, 1),
(6, '2021-06-18 11:11:34.926036', 0, NULL, 0, 'simple_product', 'Barbara Park', '28 Books', 'barbara-park', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/New_Arrival_-_8+_(Girl)_(15).png', '[]', '', '', 'Light Book,Fiction', '', '', '', NULL, 9, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;BARBARA PARK is best known as the author of the wildly popular New York Timesbestselling Junie B. Jones series, which has kept kids (and their grown-ups) laughing?and reading?for over two decades. Beloved by millions, the Junie B. Jones books have been translated into multiple languages and are a time-honored staple in elementary school classrooms around the world. Barbara once said, ?I?ve never been sure whether Junie B.?s fans love', 1, 4),
(9, '2021-06-18 11:11:35.589835', 0, NULL, 0, 'simple_product', 'Captain Underpants', '8 Books', 'captain-underpants', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/8c0fa43f8eb25c9738489ab76c0e4891.jpg', '[]', '', '', 'Comics,Dav Pikey', '', '', '', NULL, 20, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;George and Harold are two imaginative pranksters who make the depressing Jerome Horwitz School a better place. They hypnotize their nemesis, the mean-spirited principal Mr. Krupp. He transforms into the incredibly clueless yet kind-hearted and enthusiastic superhero named Captain Underpants.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quo', 1, 1),
(10, '2021-06-18 11:11:35.811349', 0, NULL, 0, 'simple_product', 'Charles Dickens', '10 Books', 'charles-dickens', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Charles_Dickens_(2).png', '[]', '', '', 'Light Story,History', '', '', '', NULL, 13, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Adapted and illustrated for children aged 7+ join Ebeneezer Scrooge on his ghostly Christmas adventure, or follow orphaned Oliver Twist from rags to riches in some of literature\\\'s most famous tales from the foggy streets of Victorian London.\\\\r&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quo', 1, 4),
(11, '2021-06-18 11:11:36.019660', 0, NULL, 0, 'simple_product', 'Diary of a Wimpy Kid', '16 Books', 'diary-of-a-wimpy-kid', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Comic_Book_(5).png', '[]', '', '', 'Comics', '', '', '', NULL, 5, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;A series of fiction books written by American author and cartoonist, Jeff Kinney.[1][2] The books follow titular pre-teen named Greg Heffley, who illustrates his daily life in a diary.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&qu', 1, 1),
(12, '2021-06-18 11:11:36.231373', 0, NULL, 0, 'simple_product', 'Dirty Bertie', '20 Books', 'dirty-bertie', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/New_Arrival_-_8+_(Girl)_(17).png', '[]', '', '', 'Comics,Early Reader', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dirty Bertie – the boy with nose-pickingly disgusting habits – is back for another helping of comic chaos! With ever-increasing madcap schemes and crazy capers, Bertie continues to delight his legions of fans who revel in his revolting ways.\\\\r\\\\n\\\\r\\\\nDirty Bertie is a hugely popular series created by award-winning illustrator, David Roberts (Tyrannosaurus Drip, The Wind in the Willows, The Troll, Eddie Dickens Trilogy). With each h', 1, 4),
(16, '2021-06-18 11:11:37.076791', 0, NULL, 0, 'simple_product', 'Dork Diaries', '15 Books', 'dork-diaries', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/a4d3890d343d9b84ae15b869dddd0113.jpg', '[]', '', '', 'Comics,Dork Diaries', '', '', '', NULL, 7, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dork Diaries is a bestselling series around the world - and this collection will introduce children to Nikki Maxwell and her fun-filled diary. \\\\r\\\\n\\\\r\\\\nNikki is the exact opposite of popular - she\\\'s a total dork! When she moves to a new school, she feels it\\\'s the perfect time to leave her old lame ways behind - but making new friends and changing your ways is never that simple... \\\\r\\\\n\\\\r\\\\nThrough the fun sketches, doodles and', 1, 1),
(18, '2021-06-18 11:11:37.490912', 0, NULL, 0, 'simple_product', 'Dr. Seuss - The Cat in The Hat Learning library', '20 Books', 'dr-seuss-the-cat-in-', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/The_cat_in_the_hat.png', '[]', '', '', 'Early Reader', '', '', '', NULL, 3, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Learn to read and read to learn, with this classic case from the one and only Dr. Seuss and the Cat in the Hat! An entertaining and educational collection featuring Dr. Seuss\\\'s most beloved creation! Come and join the Cat in the Hat in these twenty books jampacked with fantastic facts and fun, and learn about everything from animals to rainforests. \\\\n&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot', 1, 6),
(19, '2021-06-18 11:11:37.705550', 0, NULL, 0, 'simple_product', 'Dr. Seuss 33​ Series', '33 Books', 'dr-seuss-33-series', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Dr__Seuss_collection.png', '[]', '', '', 'Dr Seuss,Early Reader', '', '', '', NULL, 47, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;An entertaining and educational collection featuring Dr. Seuss\\\'s most beloved creation! Come and join the Cat in the Hat in these twenty books jampacked with fantastic facts and fun, and learn about everything from animals to rainforests. With his unique combination of hilarious stories, zany pictures and riotous rhymes, Dr. Seuss has been delighting young children and helping them learn to read for over fifty years. Creator of the ', 1, 6),
(20, '2021-06-18 11:11:37.914909', 0, NULL, 0, 'simple_product', 'Dragon Masters', '16 Books', 'dragon-masters', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Dragon_master.png', '[]', '', '', 'Fantasy,Dragon,Adventure', '', '', '', NULL, 0, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;In this fantasy series, the wizard Maldred is back! And Diego is under his dark spell! Drake learns that Maldred wants to control the Naga — an enormous dragon that lives deep within the Earth. Two keys are needed to control the Naga. The first key is guarded by a Silver Dragon named Argent. Drake, Bo, and Carlos must find Argent and stop Diego before it’s too late. But will Argent’s powerful Dragon Master Jean get in their way? The ', 1, 5),
(24, '2021-06-18 11:11:38.772014', 0, NULL, 0, 'simple_product', 'Horrible Science', '20 Books', 'horrible-science', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Horrible_science.png', '[]', '', '', 'Fiction,Science', '', '', '', NULL, 6, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Horrible Science is a similar series of books to Horrible Histories, written by Nick Arnold, illustrated by Tony de Saulles and published in the UK and India by Scholastic. They are designed to get children interested in science by concentrating on the trivial, unusual, gory, or unpleasant.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot', 1, 5),
(25, '2021-06-18 11:11:38.986735', 0, NULL, 0, 'simple_product', 'I Wonder Why', '24 Books', 'i-wonder-why', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/New_Arrival_-_Knowledge_Book_(2).png', '[]', '', '', 'Knowledge', '', '', '', NULL, 2, 1, 0, 0, '<span style=\\\"font-family: Arial; font-size: 13.3333px;\\\">This highly popular and long-running series explores the questions that young readers ask about the world around them in an unrivalled child-friendly style. The conversational format is perfect for delivering solid information in a natural, amusing and imaginative way</span>', 1, 3),
(26, '2021-06-18 11:11:39.203276', 0, NULL, 0, 'simple_product', 'Magic Tree House', '28 Books', 'magic-tree-house', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Magic.png', '[]', '', '', 'Magic Tree,Early Reader', '', '', '', NULL, 8, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;This boxed set is the ultimate gift for any Magic Tree House fan. When Jack and Annie discover a mysterious tree house filled with books, they never dream that it will take them on exciting adventures around the world and throughout history! The story begins with Dinosaurs Before Dark, when Jack and Annie are whisked back to the prehistoric past. With a total of twenty-eight adventures, kids will never get tired of traveling the worl', 1, 6),
(27, '2021-06-18 11:11:39.424863', 0, NULL, 0, 'simple_product', 'Margic Tree House - Merlin Missions', '27 Books', 'margic-tree-house-me', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Merlin.png', '[]', '', '', 'Early Reader,Magic Tree', '', '', '', NULL, 11, 1, 0, 0, '<span data-sheets-value=\\\"{\\\"1\\\":2,\\\"2\\\":\\\"Magic Tree House Merlin Missions are more challenging adventures for the experienced Magic Tree House reader. In each adventure, Merlin the magician sends Jack and Annie on an adventure in the magic tree house.\\\\n\\\"}\\\" data-sheets-userformat=\\\"{\\\"2\\\":9213,\\\"3\\\":{\\\"1\\\":0},\\\"5\\\":{\\\"1\\\":[{\\\"1\\\":2,\\\"2\\\":0,\\\"5\\\":{\\\"1\\\":2,\\\"2\\\":0}},{\\\"1\\\":0,\\\"2&', 1, 6),
(28, '2021-06-18 11:11:39.641106', 0, NULL, 0, 'simple_product', 'Marvel Studios', '12 Books', 'marvel-studios', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/1604645456731_grande.jpg', '[]', '', '', 'Marvel,Fiction,Fantasy', '', '', '', NULL, 15, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Celebrate Marvel Studios\\\' 10th anniversary with this action-packed boxset containing twelve paperback middle grade novel retellings spanning all three phases of the Marvel Cinematic Universe! Join the Avengers, the Guardians of the Galaxy, and more of your favorite super heroes as they battle the forces of evil all across the universe.\\\\r&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9021,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&q', 1, 5),
(29, '2021-06-18 11:11:39.850634', 0, NULL, 0, 'simple_product', 'Mighty Robot', '8 Books', 'mighty-robot', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Mighty.png', '[]', '', '', 'Comics,Dav Pikey', '', '', '', NULL, 12, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Ricky Ricotta, a small mouse, is being bullied at school, but when he rescues a powerful robot from its evil creator, he acquires a friend and protector--and saves the city from Dr. Stinky.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3}', 1, 1),
(31, '2021-06-18 11:11:40.263164', 0, NULL, 0, 'simple_product', 'My First Reading Library - 1', '50 Books', 'my-first-reading-lib', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/My_1.png', '[]', '', '', '', '', '', '', NULL, 14, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Perfect for sharing at bedtime and also a great way for children to get to grips with reading themselves as they embark on their own personal reading journey, this massive 50-book collection will quickly become a bookshelf favourite. With gorgeous illustrations and easy-to-follow stories, the books will allow children to gradually take over a greater share of the reading, building their confidence all the time. The stories cover wish', 1, 6),
(33, '2021-06-18 11:11:40.752018', 0, NULL, 0, 'simple_product', 'My Weird School 21-Books Set', '21 Books', 'my-weird-school-21-b', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/My_weird_school_1.png', '[]', '', '', '', '', '', '', NULL, 20, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Ella Mentry School is just plain weird! From a principal who kisses pigs to a teacher who wears dresses made of pot holders, A.J. and his friends have had some wacky adventures. Now fans can have all twenty-one books in the bestselling My Weird School series on hand!\\\\r\\\\n\\\\r&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quo', 1, 4),
(34, '2021-06-18 11:11:40.964966', 0, NULL, 0, 'simple_product', 'Newbery Award', '7 Books', 'newbery-award', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Young_Adult_Fiction_New_Arrival_-_10+_(3).png', '[]', '', '', 'Romance,Fiction', '', '', '', NULL, 2, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;The Newbery Medal was named for eighteenth-century British bookseller John Newbery. It is awarded anually by the Association for Library Service to Children, a division of the American Library Association, to the author of the most distinguished contribution to American literature for children. This collection features eight favorited recipients of the esteemed Newbery Medal or Newbery Honor Awards and is perfect for young, avid read', 1, 5),
(35, '2021-06-18 11:11:41.175847', 0, NULL, 0, 'simple_product', 'Owl Diary', '12 Books', 'owl-diary', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Owl_Diaries.png', '[]', '', '', 'Comics,Short Story', '', '', '', NULL, 12, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;An adorable early chapter book series for young girls who love friendship stories starring animal characters! An adorable early chapter book series for young girls who love friendship stories starring animal characters!&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&q', 1, 4),
(36, '2021-06-18 11:11:41.380699', 0, NULL, 0, 'simple_product', 'Peep Inside a Fairy Tale', '10 Books', 'peep-inside-a-fairy-', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Early_Reader_(1).png', '[]', '', '', 'Kidbook,3D', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;A beautifully illustrated, non-fiction book to introduce children to World for the very first time. Children can lift the flaps and peep through the holes inside the books.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:897,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0}\\\" style=\\\"font-size: 10pt; font-family: Arial;\\\">A beautifully illustrated, non-fiction book to introduce children to World for', 1, 1),
(37, '2021-06-18 11:11:41.587182', 0, NULL, 0, 'simple_product', 'Points of View', '30 Books', 'points-of-view', 0, 1, 1, 1, NULL, 1, 0, 0, 'uploads/media/2021/New_Arrival_-_Knowledge_Book_(5).png', '[]', '', '', 'Knowledge', '', '', '', NULL, 3, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;30 Books POINTS OF‎ VIEW BOOKS Debate Phase 4 : Focus on 22 topics that are relevant to young people\\\'s lives&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:1021,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot', 1, 3),
(38, '2021-06-18 11:11:41.793562', 0, NULL, 0, 'simple_product', 'Roald Dahl', '16 Books', 'roald-dahl', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Roald_D.png', '[]', '', '', 'Mystery', '', '', '', NULL, 18, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dahl\\\'s children\\\'s stories contain extraordinary ingredients which make them utterly delicious to kids, and many have been adapted as screenplays. But what exactly makes the characters so unforgettable and impossible to resist? GOBBLEFUNK: Children adore this fantastic language which Roald Dahl created.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:', 1, 5),
(39, '2021-06-18 11:11:42.008115', 0, NULL, 0, 'simple_product', 'Glenn Murphy Science', '5 Books', 'glenn-murphy-science', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/81wgqiGzAZL.jpg', '[]', '', '', 'Science,Knowledge', '', '', '', NULL, 11, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;What is a black hole? How do we know that stars and galaxies are billions of years old? What is the difference between stars and planets? Glenn Murphy, author of Why is Snot Green?, answers these and a lot of other brilliant questions in this funny and informative book. Packed with doodles and information about all sorts of incredible things, like supermassive black holes, galaxies, telescopes, planets, solar flares, constellations, ', 1, 3),
(40, '2021-06-18 11:11:42.211000', 0, NULL, 0, 'simple_product', 'Usborne Science Beginner', '10 Books', 'usborne-science-begi', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Science.png', '[]', '', '', 'Usborne,Science,Knowledge', '', '', '', NULL, 24, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Amazing science from a best-loved reader series. Explore your planet while you build your confidence with books. Are you fascinated by storms, volcanoes, stars and space? With this handsome box set of quality Usborne readers, you can explore all those cool things and boost your reading skills too. Forget dull reading schemes that don’t expand your horizons. It’s time to check out the marvels of your world! Get ready for a close-up lo', 1, 3),
(44, '2021-06-18 11:11:43.050528', 0, NULL, 0, 'simple_product', 'Sherlock Holmes', '20 Books', 'sherlock-holmes', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/New_Arrival_-_8+_(Girl)_(5).png', '[]', '', '', 'Mystery,Fiction', '', '', '', NULL, 20, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sherlock Holmes is a fictional character created by the author Arthur Conan Doyle. He would chase and seek criminals tirelessly during the Victorian and Edwardian London, in Europe and southern England. Since its creation, Holmes has remained one of the most profound and recognizable characters in the detective world and genre.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;', 1, 4),
(46, '2021-06-18 11:11:43.466865', 0, NULL, 0, 'simple_product', 'Stephen Hawking', '6 Books', 'stephen-hawking-1', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Gew.png', '[]', '', '', 'Knowledge', '', '', '', NULL, 11, 1, 0, 0, '<span data-sheets-value=\\\"{\\\"1\\\":2,\\\"2\\\":\\\"Join George and Annie as they explore the galaxy in this cosmic adventure series from acclaimed scientist Stephen Hawking and his daughter Lucy Hawking.\\\\r\\\"}\\\" data-sheets-userformat=\\\"{\\\"2\\\":1021,\\\"3\\\":{\\\"1\\\":0},\\\"5\\\":{\\\"1\\\":[{\\\"1\\\":2,\\\"2\\\":0,\\\"5\\\":{\\\"1\\\":2,\\\"2\\\":0}},{\\\"1\\\":0,\\\"2\\\":0,\\\"3\\\":3},{\\\"1\\\":1,\\\"2\\\":0,', 1, 3),
(47, '2021-06-18 11:11:43.681097', 0, NULL, 0, 'simple_product', 'The Storey Treehouse', '9 Books', 'the-storey-treehouse', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Storey_tree.png', '[]', '', '', 'Light Story', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{\\\"1\\\":2,\\\"2\\\":\\\"Andy and Terry live in the WORLDS BEST treehouse! Its got a giant catapult, a secret underground laboratory, a tank of man-eating sharks and a marshmallow machine that follows you around and shoots marshmallows into your mouth whenever you are hungry. Titles in this Set contains 13 Storey Treehouse, 26 Storey Treehouse, 39 Storey Treehouse, 52 Storey Treehouse, 65 Storey Treehouse, 78 Storey Treehouse, 91 Storey Treehouse, 104 Storey', 1, 4),
(50, '2021-06-18 11:11:44.311042', 0, NULL, 0, 'simple_product', 'The Legends of King Authur', '10 Books', 'the-legends-of-king-', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/New_Arrival_-_8+_(Girl)_(14).png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;The Story of King Arthur and His Knights is a 1903 children\\\'s novel by the American illustrator and writer Howard Pyle. The book contains a compilation of various stories, adapted by Pyle, regarding the legendary King Arthur of Britain and select Knights of the Round Table.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot', 1, 4),
(52, '2021-06-18 11:11:44.729568', 0, NULL, 0, 'simple_product', 'The Usborne Reading Collection', '50 Books', 'the-usborne-reading-', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/My_usborne_2.png', '[]', '', '', 'Usborne', '', '', '', NULL, 0, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;The Usborne Reading programme has been helping children learn to read for pleasure for nearly twenty years. This comprehensive box set, featuring a selection of forty books from Young Reading Series One and Two, is perfect for launching your child into the next level of reading. Each volume has been assessed by educators, so you can be sure that your child is getting the good stuff.\\\\r\\\\n\\\\r\\\\nThis great value box set, containing for', 1, 6),
(53, '2021-06-18 11:11:44.941844', 0, NULL, 0, 'simple_product', 'The Usborne Story Collection', '20 Books', 'the-usborne-story-co', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Usborne_book_collection.png', '[]', '', '', 'Usborne', '', '', '', NULL, 0, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;There are stories about animals, dinosaurs, fairies, princesses, wizard and time travel. with comedy, mystery and just a little magic, they are sure to appeal to all kinds of young readers&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},', 1, 6),
(54, '2021-06-18 11:11:45.162121', 0, NULL, 0, 'simple_product', 'Timmy Failure', '7 Books', 'timmy-failure', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Timmy.jpg', '[]', '', '', 'Comic', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Meet Timmy Failure, the founder, president, and CEO of the best detective agency in town, probably the nation.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quo', 1, 1),
(55, '2021-06-18 11:11:45.378311', 0, NULL, 0, 'simple_product', 'Tom Gates', '10 Books', 'tom-gates', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Tom_gates.jpg', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-family: Arial; font-size: 13.3333px;\\\">A whopping pack of Tom’s super-silly stories, jokes, scribbles and doodles. We’d sell our brains for it. YEAH! Tom’s grumpy teacher Mr Fullerman might deny it, but this cheeky schoolkid has made good. He’s inspired funny pranks all over the world. (Not to mention a million doodle notebooks.) He’s a master of excuses, expert doodler and cool rock guitarist. And a Roald Dahl Funny Prize winner with millions of loyal fans. Basically, he’s AM', 1, 1),
(56, '2021-06-18 11:11:45.598343', 0, NULL, 0, 'simple_product', 'How to Train Your Dragon', '12 Books', 'how-to-train-your-dr', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/How_to_train_the_dragon_(2).png', '[]', '', '', 'Fantasy,Fiction', '', '', '', NULL, 0, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;How to Train Your Dragon chronicles the adventures and misadventures of reluctant Viking hero Hiccup and his dragon, Toothless. Join them, their friends and foes, and dragons galore in all twelve of their remarkable, beloved adventures.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9021,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&', 1, 5),
(57, '2021-06-18 11:11:45.805343', 0, NULL, 0, 'simple_product', 'Elephant & Piggie', '25 Books', 'elephant-piggie-1', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Elephant_Piggie.jpg', '[]', '', '', '', '', '', '', NULL, 0, 1, 0, 0, '<span data-sheets-value=\\\"{\\\"1\\\":2,\\\"2\\\":\\\"The Elephant & Piggie Collection contains all 25 books in the beloved children’s series by Mo Willems, as well as two Elephant and Piggie character bookends to hold them together.  \\\"}\\\" data-sheets-userformat=\\\"{\\\"2\\\":9213,\\\"3\\\":{\\\"1\\\":0},\\\"5\\\":{\\\"1\\\":[{\\\"1\\\":2,\\\"2\\\":0,\\\"5\\\":{\\\"1\\\":2,\\\"2\\\":0}},{\\\"1\\\":0,\\\"2\\\":0,\\\"3\\\":3},{&q', 1, 6),
(58, '2021-06-18 11:11:46.017231', 0, NULL, 0, 'simple_product', 'Who Would Win?', '15 Books', 'who-would-win', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Who_would_win.jpg', '[]', '', '', '', '', '', '', NULL, 7, 1, 0, 0, '<span style=\\\"font-family: Arial; font-size: 13.3333px;\\\">Which wild animals would win a deadly fight? Find out in this awesome set of 25 books from the wildly popular Who Would Win Books Series! This set features a variety of over 100 new birds, dinosaurs, insects, and mammals! Students and young readers explore the facts and characteristics between two wild animals. Facts and Photos provide a contrast between two different creatures; comparing their brain structures, bodies, behaviors, and abi', 1, 3),
(60, '2021-06-18 11:11:46.443326', 0, NULL, 0, 'simple_product', '3D Wild', '4 Books', '3d-wild', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/3D.png', '[]', '', '', 'Kidbook,3D', '', '', '', NULL, 7, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;3D Theatre: Rainforest, Animal, Ocean and Wild - by Kathryn Jewitt, illustrated by Fiametta Dogi - uses stunning 3D scenes to take the reader into the very heart of the read situation.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:897,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0}\\\" style=\\\"font-size: 10pt; font-family: Arial;\\\">3D Theatre: Rainforest, Animal, Ocean and Wild - by Kathryn Jewitt', 1, 2),
(64, '2021-06-18 11:11:58.996796', 0, NULL, 0, 'simple_product', 'Harry Potter', '8 Books + A pen set', 'harry-potter', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Harry_Potter_(2).png', '[]', '', '', 'Harry Potter,Fiction,Fantasy', '', '', '', NULL, 30, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Harry Potter is a series of seven fantasy novels written by British author J. K. Rowling. The novels chronicle the lives of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, all of whom are students at Hogwarts School of Witchcraft and Wizardry.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9021,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot', 1, 5),
(67, '2021-06-18 11:11:59.911743', 0, NULL, 0, 'simple_product', 'Heroes of Olympus', '5 Books', 'heroes-of-olympus', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Heroes_of_Olympu.png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;The Heroes of Olympus is centred around a prophecy introduced in The Last Olympian, that predicted seven demigods would unite to protect the world from an awakening new enemy, Gaia. Demigods from both the Greek camp, Camp Half-Blood, and a newfound Roman camp, Camp Jupiter, unite to save the world from being destroyed by the terrifying Earth Goddess, Gaia. &quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9021,&quot;3&quot;:{&quot;1&', 1, 5),
(68, '2021-06-18 11:12:00.420863', 0, NULL, 0, 'simple_product', 'Horrid Henry', '25 Books', 'horrid-henry', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Henry.png', '[]', '', '', 'Early Reader', '', '', '', NULL, 9, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Henry, a self-centred, naughty prankster who has issues with authority is faced with a problem. He then often retaliates in interesting ways.\\\\r&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quo', 1, 6),
(70, '2021-06-18 11:12:01.695278', 0, NULL, 0, 'simple_product', 'Incredible Peppa Pigs', '50 Books', 'incredible-peppa-pig', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Early_Reader_(5).png', '[]', '', '', '', '', '', '', NULL, 2, 1, 0, 0, '<span data-sheets-value=\\\"{\\\"1\\\":2,\\\"2\\\":\\\"Peppa Pig is a cute and loved bossy little pig that is loved by young children all over the world! She is a four year old and lives with Daddy Pig, Mummy Pig and her younger brother called George. A great collection for ages 0-5 and one of our superb bestsellers! Following this cute family is fun and all the daily activities of Peppa Pig and friends will relate to many children around the world! Lets talk about some of the books foun', 1, 2),
(72, '2021-06-18 11:12:03.360651', 0, NULL, 0, 'simple_product', 'Usborne for Beginners', '5 Books', 'usborne-for-beginner', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Usborne_for_beginer.jpg', '[]', '', '', 'Knowledge,Usborne', '', '', '', NULL, 0, 1, 0, 0, 'Empty', 1, 3),
(73, '2021-06-18 11:12:04.154059', 0, NULL, 0, 'simple_product', 'Little People Big Dreams', '13 Books', 'little-people-big-dr', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Little_ppl,_big_dream.png', '[]', '', '', 'Knowledge', '', '', '', NULL, 2, 1, 0, 0, '<p><span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Discover the lives of outstanding people, from designers and artists to scientists and activists. All of them achieved incredible things, yet each began life as a child with a dream. Told as a story, with a facts and photos section at the back, this sweet series for kids celebrates triumph over adversity through some of history’s favorite characters.\\\\r\\\\n\\\\r&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:1021,&quot;3&quot;:{&qu', 1, 3),
(76, '2021-06-18 11:12:08.381690', 0, NULL, 0, 'simple_product', 'Percy Jackson', '5 Books', 'percy-jackson', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Percy_Jackson.png', '[]', '', '', '', '', '', '', NULL, 0, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Accompany the son of the sea god Poseidon and his other demigod friends as they go on a series of quests that will have them facing monsters, gods, and conniving figures from Greek mythology. Do they have what it takes to save the Olympians from an ancient enemy?&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9021,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&q', 1, 5),
(82, '2021-06-18 11:12:16.664453', 0, NULL, 0, 'simple_product', 'The World of David Walliams', '10 Books', 'the-world-of-david-w', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/David_Walliams_1.jpg', '[]', '', '', 'David Walliams,Fiction', '', '', '', NULL, NULL, NULL, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;The World of David Walliams - The Terrific Ten! Mega-Massive 10 Books Collection Box Set:&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:11071,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16777215},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&qu', 1, 5),
(83, '2021-06-18 11:12:17.776167', 0, NULL, 0, 'simple_product', 'Ultimate Peppa Pigs', '50 Books', 'ultimate-peppa-pigs', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Early_Reader_(6).png', '[]', '', '', 'Kidbook', '', '', '', NULL, 5, 1, 0, 0, '<span data-sheets-value=\\\"{\\\"1\\\":2,\\\"2\\\":\\\"The Peppa Pig Ultimate collection features 50 amazing story books from everyone\\\'s favourite pig. Join Peppa Pig and friends on many adventures including Peppa going skiing, meeting the new neighbours and playing basketball! Based on the popular TV shows Books are about everyone\\\'s favourite piglet Peppa Pig lives with her Mummy, Daddy and baby brother George and likes to have all kinds of fun adventures but most of all she loves jum', 1, 2),
(87, '2021-06-18 11:12:21.863672', 0, NULL, 0, 'simple_product', 'Dogman', '11 Books', 'dogman-1', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Dogman11.jpg', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dog Man is a comedic graphic novel series by American cartoonist Dav Pilkey published by Scholastic Corporation that focuses on a part-dog, part-man police officer/superhero.&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:9213,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;', 1, 1),
(273, '2021-06-18 11:17:23.685888', 0, NULL, 0, 'simple_product', 'A Case of Good Manners', 'Age: 0.6 - 5', 'a-case-of-good-manne', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/A_case_of_good_manner.jpg', '[]', '', '', 'Kidbook,Cardbook', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-family: Poppins; font-size: 18.6667px;\\\">Three best selling series, Don\\\'t Forget Your Manners, Getting Along With Others and Good Habits To Have. With their bright colours and engaging editorial, these 12 little board books explore important themes and concepts which help kids build their character, habits, manners and learn to get along with others. Helpful hints for parents are included in every book.</span>', 1, 2),
(274, '2021-06-18 11:17:23.886394', 0, NULL, 0, 'simple_product', 'A Friend For Dragon', 'Age: 3 - 5', 'a-friend-for-dragon', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Dragon.png', '[]', '', '', '', '', '', '', NULL, 30, 1, 0, 0, '<p><span data-sheets-value=\\\"{&quot;1&quot;:2,&quot;2&quot;:&quot;From Dav Pilkey, author and illustrator of the worldwide bestselling Dog Man and Captain Underpants series, comes Dragon, a humble hero with a heart of gold, who always helps those who need him.\\\\r\\\\n\\\\r&quot;}\\\" data-sheets-userformat=\\\"{&quot;2&quot;:897,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0}\\\" style=\\\"font-size: 10pt; font-family: Arial;\\\"><span style=\\\"font-size: 14pt; font-family: ', 1, 2),
(277, '2021-06-18 11:17:24.530625', 0, NULL, 0, 'simple_product', 'Andrew Lost', 'Age: 6+', 'andrew-lost-1', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/andrew.jpg', '[]', '', '', 'Fiction,Science', '', '', '', NULL, 10, 1, 0, 0, '<span data-sheets-value=\\\"{\\\"1\\\":2,\\\"2\\\":\\\"Andrew Lost is a series of children\\\'s science fiction adventure novels written by J. C. Greenburg and published by Random House from 2002 to 2008. It features a boy inventor named Andrew Dubble whose inventions rarely work the way he expects them to.\\\"}\\\" data-sheets-userformat=\\\"{\\\"2\\\":9213,\\\"3\\\":{\\\"1\\\":0},\\\"5\\\":{\\\"1\\\":[{\\\"1\\\":2,\\\"2\\\":0,\\\"5\\\":{\\\"1\\\":2,\\\"', 1, 5),
(281, '2021-06-18 11:17:25.445107', 0, NULL, 0, 'simple_product', 'Big Nate', 'Age: 6+', 'big-nate', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Big_Nate.jpg', '[]', '', '', '', '', '', '', NULL, 1, 1, 0, 0, '<span style=\\\"font-family: Poppins; font-size: 18.6667px;\\\">Nate is in sixth grade and 11 years old, a talented cartoonist, drummer, and chess player. He also believes he is a natural prankster, as he attempts funny and difficult pranks on the second to last day of school (known in the book as \\\"Prank Day\\\").</span>', 1, 1),
(293, '2021-06-18 11:17:28.101549', 0, NULL, 0, 'simple_product', 'Eric Vale', 'Age: 6+', 'eric-vale', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Eric_Vale.png', '[]', '', '', 'Comic', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-family: Poppins; font-size: 14px;\\\">Eric Vale is called funny and has major child appeal, but with a few strong messages woven in for good measure. MGB has crafted a story about resilience, acceptance and inclusivity, with not a trace of the dreaded schoolyard ‘b’ word. The characters are so well portrayed that they could each have a novel of their own.</span><br style=\\\"color: rgb(228, 230, 235); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Ari', 1, 4),
(316, '2021-06-18 11:17:32.961157', 0, NULL, 0, 'simple_product', 'J.R.R Tolkien', 'N/A', 'jrr-tolkien', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Young_Adult_Fiction_New_Arrival_-_10+_(4).png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '', 1, 4),
(317, '2021-06-18 11:17:33.172853', 0, NULL, 0, 'simple_product', 'Kid Spy', 'Age: 8+\\r\\nA set of 4 Books\\r\\nPage: 120\\r\\nBook type: Comedy & adventure', 'kid-spy', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Kidspy.png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 12px; white-space: pre-wrap;\\\">James Bond meets Diary of a Wimpy Kid with this groundbreaking fully-illustrated chapter book series Mac B., Kid Spy. The precious Crown Jewels have been stolen, and there\\\'s only one person who can help the Queen of England: her newest secret agent, Mac B. Mac travels around the globe in search of the stolen treasure...but will he find it in time?\\r\\n\\r\\nFrom secret identit', 1, 4),
(318, '2021-06-18 11:17:33.386488', 0, NULL, 0, 'simple_product', 'Kids Normal', 'Age: 8+\\r\\nA set of 4 books\\r\\nPage: 350\\r\\nBook type: Comedy and adventure', 'kids-normal', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Normal_Kid_Shop.png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 12px; white-space: pre-wrap;\\\">Despite having no powers, Murph Cooper is part of the best team in the Heroes\\\' Alliance. So when supervillain Magpie declares all-out war, Kid Normal and the Super Zeroes lead the charge.</span>', 1, 4),
(321, '2021-06-18 11:17:34.032104', 0, NULL, 0, 'simple_product', 'B- Little Guides to Great Lives', 'Age: 6+\\r\\nA set of 9 Books\\r\\nPage: 80\\r\\nBook type: Knowledge', 'b-little-guides-to-g', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Little_guide_to_great_live.png', '[]', '', '', 'Knowledge,Education', '', '', '', NULL, 10, 1, 0, 0, '<ul class=\\\"_1lqw _34wy uiList _4ks _6-j _6-h _4kg\\\" style=\\\"list-style-type: none; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; font-size: 12px; line-height: 18px; color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif;\\\"><li style=\\\"border-width: 0px; border-color: rgb(235, 237, 240); border-style: solid; padding-top: 0px; padding-bottom: 10px;\\\"><div class=\\\"_1xwp\\\" style=\\\"white-space: pre-wrap; font-family: inherit;\\\"><span style=\\\"font-size: 12', 1, 3),
(325, '2021-06-18 11:17:34.882436', 0, NULL, 0, 'simple_product', 'Maisy Goes To', 'Age: 3+', 'maisy-goes-to', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Early_Reader_(2).png', '[]', '', '', 'Kidbook', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-family: Poppins; font-size: 14px;\\\">Broom, vroom, beep! Maisy and Charley are in the city, and there are many things to get used to — noisy traffic, enormous buildings, and sidewalks so crowded they have to walk very slowly. Riding the escalator and elevator — and hanging on tight in the subway — are almost as much fun as exploring the giant toy store and eating pizza in a café. Even the playground is busy in the city!</span>', 1, 2),
(329, '2021-06-18 11:17:35.705811', 0, NULL, 0, 'simple_product', 'My First Reading Library  - 2', 'Age: 6+', 'my-first-reading-lib', 0, 1, 1, 1, 1, 0, 0, 0, 'uploads/media/2021/My_first_library2.png', '[]', '', '', 'Early Reader,Education', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-family: Poppins; font-size: 14px;\\\">Perfect for sharing at bedtime and also a great way for children to get to grips with reading themselves as they embark on their own personal reading journey, this massive 50-book collection will quickly become a bookshelf favorite. With gorgeous illustrations and easy-to-follow stories, the books will allow children to gradually take over a greater share of the reading, building their confidence all the time. The stories cover wishes, magic', 1, 6),
(331, '2021-06-18 11:17:36.129548', 0, NULL, 0, 'simple_product', 'My Weird School Fast Fact', 'Age: 6+', 'my-weird-school-fast', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/New_Arrival_-_8+_(Girl)_(13).png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-size: 14px;\\\">Did you know that manatees swim by farting? Did you know that a cat named Stubbs was the mayor of Talkeetna, Alaska, for fifteen years?! Whether you’re a kid who wants to learn more about animals or simply someone who wants to know which animal is faster than an Indianapolis 500 racecar, this is the book for you!</span>', 1, 6),
(334, '2021-06-18 11:17:36.779052', 0, NULL, 0, 'simple_product', 'Pete The Cat', 'Age: 6+', 'pete-the-cat-1', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Early_Reader_(10).png', '[]', '', '', 'Education,Early Reader', '', '', '', NULL, 5, 1, 0, 0, '<div data-offset-key=\\\"8i8ap-0-0\\\" class=\\\"_1mf _1mj\\\" style=\\\"color: rgb(28, 30, 33); font-family: inherit; font-size: 14px; white-space: pre-wrap; position: relative; direction: ltr;\\\"><span data-offset-key=\\\"8i8ap-0-0\\\" style=\\\"font-family: Poppins; font-size: 14px;\\\">Pete is a special cat with blue hair and golden eyes. Unlike other cats who like to be alone, he likes to make friends and try new ones. Things, challenges new tasks, not afraid of strange environments, informal, this is Pete, a', 1, 6),
(341, '2021-06-18 11:17:38.254040', 0, NULL, 0, 'simple_product', 'Ready, Freddy! Collection', 'Age: 6+', 'ready-freddy-collect', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Ready,_Freddy!_Collection_1.png', '[]', '', '', 'Knowledge,Education', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-size: 14px;\\\">Freddy loses a tooth, battles a bully, breaks his arm, and so much more! This incredible collection of 10 very funny, easy-to-read books is sure to appeal to beginning readers and lead them to read success. Author Abby Klein, a first-grade teacher herself, models Freddy\\\'s adventures on those of real children. Perhaps that\\\'s why Freddy\\\'s concerns seem so realistic-and why young readers like his books so much.</span>', 1, 6),
(344, '2021-06-18 11:17:38.876786', 0, NULL, 0, 'simple_product', 'Science Comics', 'Age: 6+', 'science-comics', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Science_c.png', '[]', '', '', '', '', '', '', NULL, 5, 1, 0, 0, '<span style=\\\"font-size: 14px;\\\" segoe=\\\"\\\" ui=\\\"\\\" historic\\\",=\\\"\\\" \\\"segoe=\\\"\\\" ui\\\",=\\\"\\\" helvetica,=\\\"\\\" arial,=\\\"\\\" sans-serif;=\\\"\\\" font-size:=\\\"\\\" 15px;\\\"=\\\"\\\">The Science Comics book series by multiple authors includes books Science Comics: Bridges: Engineering Masterpieces, Science Comics: Dinosaurs: Fossils and Feathers, Science Comics: Coral Reefs: Cities of the Ocean, and several more.</span>', 1, 1),
(347, '2021-06-18 11:17:39.493799', 0, NULL, 0, 'simple_product', 'Maurice Sendak', 'Age: 3+', 'maurice-sendak-1', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Sendak_(2).png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-size: 14px;\\\">Enjoy this beautiful collection of books by Maurice Sendak. All four books are filled with fun graphics, silly sayings &amp; education</span>', 1, 3),
(348, '2021-06-18 11:17:39.696042', 0, NULL, 0, 'simple_product', 'Smart Smart STEM', 'Age: 6+', 'smart-smart-stem', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/STEM.png', '[]', '', '', 'Knowledge,STEM', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"color: rgb(28, 30, 33); font-family: Poppins; font-size: 14px;\\\">Engage your child in fun hands-on projects that develop problem-solving and critical thinking skills. The STEM challenges and activities encourage your child to think creatively and explore different ideas to solve problems. Parents act as facilitators, guiding their children through the problem-solving process.</span>', 1, 3),
(357, '2021-06-18 11:17:41.594021', 0, NULL, 0, 'simple_product', 'The World\\\\\\\'s Worst Children', 'Age: 8+', 'the-worlds-worst-chi', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/The_world_worst_children_Shop.png', '[]', '', '', '', '', '', '', NULL, 10, 1, 0, 0, '<span style=\\\"font-size: 14px;\\\">David Walliams has rocked the world with his funny novels about awful adults. Evil aunties. Sadistic dentists. But that’s just the start of the horror. Now it’s time to laugh and shudder at the world’s worst children in three books full of beastly boys and gruesome girls! There’s Sofia Sofa: a TV super-fan who is literally stuck to the couch. Or the gross Dribbling Drew. (Don’t sit next to him on a school trip.) Gruesome Griselda and Fussy Frankie will leave you ', 1, 3);
INSERT INTO `products` (`id`, `date_added`, `row_order`, `stock_type`, `tax`, `type`, `name`, `short_description`, `slug`, `indicator`, `cod_allowed`, `minimum_order_quantity`, `quantity_step_size`, `total_allowed_quantity`, `is_returnable`, `is_cancelable`, `cancelable_till`, `image`, `other_images`, `video_type`, `video`, `tags`, `warranty_period`, `guarantee_period`, `made_in`, `sku`, `stock`, `availability`, `rating`, `no_of_ratings`, `description`, `status`, `category_id`) VALUES
(358, '2021-06-18 11:17:41.808760', 0, NULL, 0, 'simple_product', 'The Zack File', 'Age: 6+ A set of 25 Books Page: 50 Book type: Science Fiction', 'the-zack-file', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Zack.png', '[]', '', '', '', '', '', '', NULL, 9, 1, 0, 0, '<span style=\\\"color: rgb(28, 30, 33); font-family: Arial, sans-serif; font-size: 13px;\\\">Parents need to know that Dan Greenburg\\\'s The Zack Files series is a funny, wacky look at a 10-year-old boy\\\'s improbable adventures in and out of school. The stories are simple, and the premise of each story is impossible -- Neanderthals coming to life in a museum, or a great-grandpa reincarnated as a talking cat at the animal shelter -- which makes Zack\\\'s adventures even funnier. Zack takes care to tell ', 1, 4),
(360, '2021-06-18 11:17:42.245824', 0, NULL, 0, 'simple_product', 'Baby Cardboard Books', 'Age: 0.6+', 'baby-cardboard-books', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Early_Reader_(3).png', '[]', '', '', '', '', '', '', NULL, 0, 0, 0, 0, '11 Funny Educational English Cardboard Books', 1, 2),
(361, '2021-06-18 11:17:42.507082', 0, NULL, 0, 'simple_product', 'Usborne Beginners Nature', 'Age: 6+', 'usborne-beginners-na', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Nature_(2).png', '[]', '', '', '', '', '', '', NULL, 9, 1, 0, 0, '<span style=\\\"font-size: 14px;\\\" segoe=\\\"\\\" ui=\\\"\\\" historic\\\",=\\\"\\\" \\\"segoe=\\\"\\\" ui\\\",=\\\"\\\" helvetica,=\\\"\\\" arial,=\\\"\\\" sans-serif;=\\\"\\\" font-size:=\\\"\\\" 15px;\\\"=\\\"\\\">Usborne Beginners Nature, Collection of 10 Books is designed to provide an informative introduction to trees and plant-life for young readers. The titles in this set include: Reptiles, Tadpoles and Frogs, Rainforest, Trees, How Flowers Grow, Caterpillars and Butterflies, Spiders, Bugs, Bees &amp; Wasps and Ants.</span>', 1, 3),
(368, '2021-06-18 11:17:44.057005', 0, NULL, 0, 'simple_product', 'Wings of Fire', 'Age: 10+', 'wings-of-fire-1', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Wing_of_fire_(2).png', '[]', '', '', '', '', '', '', NULL, 5, 1, 0, 0, '<span style=\\\"font-family: \\\"Segoe UI Historic\\\", \\\"Segoe UI\\\", Helvetica, Arial, sans-serif; font-size: 15px;\\\">A war has been raging between the dragon tribes of Pyrrhia for years. According to a prophecy, five dragonets will end the bloodshed and choose a new queen. But not every dragonet wants a destiny. And when Clay, Tsunami, Glory, Starflight, and Sunny discover the truth about their unusual, secret upbringing, they might choose freedom over fate–and find a way to save the', 1, 5),
(369, '2021-06-18 11:17:44.273524', 0, NULL, 0, 'simple_product', 'Winnie and Wilbur', 'Age: 6+', 'winnie-and-wilbur', 0, 1, 1, 1, NULL, 0, 0, 0, 'uploads/media/2021/Winnie.png', '[\"uploads\\/media\\/2021\\/Winnie.png\"]', '', '', '', '', '', '', NULL, 9, 1, 0, 0, '<span style=\\\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\\\">Winnie and Wilbur 9 Magical Fiction books Collection Box Set by Laura Owen and Korky Paul. This fiction set is full of humour, enchantment which will be cherished by children.</span><br style=\\\"color: rgb(228, 230, 235); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; background-color: rgb(36, 37, 38);\\\">', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_value_ids` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `attribute_value_ids`, `date_created`) VALUES
(1, 417, '', '2021-06-21 09:23:42'),
(2, 425, '', '2021-06-23 02:41:50'),
(3, 426, '', '2021-06-23 03:16:16'),
(4, 427, '', '2021-06-23 03:37:23');

-- --------------------------------------------------------

--
-- Table structure for table `product_rating`
--

CREATE TABLE `product_rating` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `rating` double NOT NULL DEFAULT 0,
  `images` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_rating`
--

INSERT INTO `product_rating` (`id`, `user_id`, `product_id`, `rating`, `images`, `comment`, `data_added`) VALUES
(1, 10, 374, 5, NULL, NULL, '2021-09-07 09:42:31'),
(2, 11, 374, 5, NULL, NULL, '2021-09-07 09:42:14'),
(3, 2, 374, 5, NULL, NULL, '2021-09-08 10:16:53'),
(4, 5, 374, 1, NULL, NULL, '2021-09-08 10:17:02'),
(5, 2, 374, 5, NULL, NULL, '2021-09-08 10:17:13'),
(6, 10, 374, 5, NULL, NULL, '2021-09-08 10:17:29'),
(7, 10, 374, 2, NULL, NULL, '2021-09-08 10:17:37'),
(8, 10, 374, 4, NULL, NULL, '2021-09-08 10:17:49'),
(9, 10, 374, 2, NULL, NULL, '2021-09-08 10:17:59'),
(10, 10, 374, 5, NULL, NULL, '2021-09-08 10:18:10'),
(11, 8, 374, 3, NULL, NULL, '2021-09-08 10:18:21');

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` bigint(20) NOT NULL,
  `attribute_value_ids` varchar(500) DEFAULT NULL,
  `attribute_set` varchar(1024) DEFAULT NULL,
  `price` double NOT NULL,
  `special_price` double NOT NULL,
  `sku` varchar(128) DEFAULT NULL,
  `images` varchar(500) DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `date_added` datetime(6) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `attribute_value_ids`, `attribute_set`, `price`, `special_price`, `sku`, `images`, `availability`, `status`, `date_added`, `product_id`, `stock`) VALUES
(3, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:34.333236', 3, NULL),
(4, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:34.584055', 4, NULL),
(5, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:34.811796', 5, NULL),
(6, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:35.040402', 6, NULL),
(9, NULL, NULL, 40, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:35.704413', 9, NULL),
(10, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:35.912798', 10, NULL),
(11, NULL, NULL, 45, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:36.122938', 11, NULL),
(12, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:36.333203', 12, NULL),
(16, NULL, NULL, 55, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:37.180803', 16, NULL),
(18, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:37.596911', 18, NULL),
(19, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:37.807628', 19, NULL),
(20, NULL, NULL, 34.99, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:38.019208', 20, NULL),
(24, NULL, NULL, 45, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:38.874413', 24, NULL),
(25, NULL, NULL, 40, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:39.091967', 25, NULL),
(26, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:39.311874', 26, NULL),
(27, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:39.533115', 27, NULL),
(28, NULL, NULL, 45, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:39.740168', 28, NULL),
(29, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:39.951050', 29, NULL),
(31, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:40.363062', 31, NULL),
(33, NULL, NULL, 25, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:40.856763', 33, NULL),
(34, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:41.065884', 34, NULL),
(35, NULL, NULL, 25, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:41.278675', 35, NULL),
(36, NULL, NULL, 45, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:41.481284', 36, NULL),
(37, NULL, NULL, 40, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:41.687259', 37, NULL),
(38, NULL, NULL, 29.96, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:41.898489', 38, NULL),
(39, NULL, NULL, 29.96, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:42.107965', 39, NULL),
(40, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:42.314180', 40, NULL),
(44, NULL, NULL, 52, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:43.152552', 44, NULL),
(46, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:43.573260', 46, NULL),
(47, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:43.782881', 47, NULL),
(50, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:44.416733', 50, NULL),
(52, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:44.832157', 52, NULL),
(53, NULL, NULL, 34.97, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:45.050679', 53, NULL),
(54, NULL, NULL, 34.97, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:45.271974', 54, NULL),
(55, NULL, NULL, 40.01, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:45.486142', 55, NULL),
(56, NULL, NULL, 45, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:45.697451', 56, NULL),
(57, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:45.909731', 57, NULL),
(58, NULL, NULL, 25, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:46.123996', 58, NULL),
(60, NULL, NULL, 40, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:46.545988', 60, NULL),
(64, NULL, NULL, 45, 0, NULL, NULL, NULL, 1, '2021-06-18 11:11:59.096632', 64, NULL),
(67, NULL, NULL, 40, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:00.015967', 67, NULL),
(68, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:00.527246', 68, NULL),
(70, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:01.795608', 70, NULL),
(72, NULL, NULL, 38, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:03.461903', 72, NULL),
(73, NULL, NULL, 45, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:04.265287', 73, NULL),
(76, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:08.486895', 76, NULL),
(82, NULL, NULL, 50, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:16.768911', 82, NULL),
(83, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:17.877302', 83, NULL),
(87, NULL, NULL, 45.03, 0, NULL, NULL, NULL, 1, '2021-06-18 11:12:21.971558', 87, NULL),
(273, NULL, NULL, 12, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:23.785408', 273, NULL),
(274, NULL, NULL, 16, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:23.988621', 274, NULL),
(277, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:24.630675', 277, NULL),
(281, NULL, NULL, 45.03, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:25.559118', 281, NULL),
(293, NULL, NULL, 22, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:28.208163', 293, NULL),
(316, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:33.061888', 316, NULL),
(317, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:33.278278', 317, NULL),
(318, NULL, NULL, 32, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:33.497979', 318, NULL),
(321, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:34.136569', 321, NULL),
(325, NULL, NULL, 18, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:34.981572', 325, NULL),
(329, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:35.810522', 329, NULL),
(331, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:36.227872', 331, NULL),
(334, NULL, NULL, 22, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:36.879716', 334, NULL),
(341, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:38.359718', 341, NULL),
(344, NULL, NULL, 60, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:38.978242', 344, NULL),
(347, NULL, NULL, 20, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:39.591209', 347, NULL),
(348, NULL, NULL, 20, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:39.793684', 348, NULL),
(357, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:41.698946', 357, NULL),
(358, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:41.913657', 358, NULL),
(360, NULL, NULL, 35, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:42.377357', 360, NULL),
(361, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:42.618842', 361, NULL),
(368, NULL, NULL, 50, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:44.165180', 368, NULL),
(369, NULL, NULL, 30, 0, NULL, NULL, NULL, 1, '2021-06-18 11:17:44.377046', 369, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` int(11) NOT NULL,
  `promo_code` varchar(28) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_users` int(11) DEFAULT NULL,
  `minimum_order_amount` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `discount_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_discount_amount` double DEFAULT NULL,
  `repeat_usage` tinyint(4) NOT NULL,
  `no_of_repeat_usage` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `return_requests`
--

CREATE TABLE `return_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_item_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `remarks` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `title` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_ids` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_order` int(11) NOT NULL DEFAULT 0,
  `categories` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title`, `short_description`, `style`, `product_ids`, `row_order`, `categories`, `product_type`, `date_added`) VALUES
(1, 'Popular Products', 'List of Product Category', 'style_1', NULL, 0, '1,4', 'products_on_sale', '2021-06-20 07:09:33'),
(2, 'Product List', 'Show Product List', 'default', NULL, 0, '10,11,18', 'most_selling_products', '2021-06-20 14:21:13');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `variable` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `variable`, `value`) VALUES
(1, 'logo', 'uploads/media/2021/Logo-Mark.png'),
(2, 'privacy_policy', '<p></p><h2><b><span xss=removed>Privacy policy</span></b></h2><span xss=removed>our products are a top-grade books that we guarantee on the price and quality. If u found the quality not right, we will refund back the money or exchange a new set for you. This is the value of our business and we hope you understand.</span><br>\\r\\n\\r\\n<p><br></p>'),
(3, 'terms_conditions', '<h3><b>Terms and conditions</b></h3><p>awesome.com is a sole proprietary firm , Juridical rights of awesome.com are reserved with eShop<br>\\r\\nPersonal Information eShop.com and the website eShop.com (”The\\r\\n Site”) . respect your privacy. This Privacy Policy succinctly provides\\r\\n the manner your data is collected and used by eShop.com. on the \\r\\nSite. As a visitor to the Site/ Customer, you are advised to please read \\r\\nthe Privacy Policy carefully.</p>\\r\\n\\r\\n<p>Services Overview As part of the registration process on the Site, \\r\\neShop.com may collect the following personally identifiable \\r\\ninformation about you: Name including first and last name, alternate \\r\\nemail address, mobile phone number and contact details, Postal code, GPS\\r\\n location, Demographic profile &#40;like your age, gender, occupation, \\r\\neducation, address, etc.&#41; and information about the pages on the site you\\r\\n visit/access, the links you click on the site, the number of times you \\r\\naccess the page and any such browsing information.</p>\\r\\n\\r\\n<p>Eligibility Services of the Site would be available to only select \\r\\ngeographies in India. Persons who are \\\"incompetent to contract\\\" within \\r\\nthe meaning of the Indian Contract Act, 1872 including un-discharged \\r\\ninsolvents etc. are not eligible to use the Site. If you are a minor \\r\\ni.e. under the age of 18 years but at least 13 years of age you may use \\r\\nthe Site only under the supervision of a parent or legal guardian who \\r\\nagrees to be bound by these Terms of Use. If your age is below 18 years,\\r\\n your parents or legal guardians can transact on behalf of you if they \\r\\nare registered users. You are prohibited from purchasing any material \\r\\nwhich is for adult consumption and the sale of which to minors is \\r\\nprohibited.</p>\\r\\n\\r\\n<p>License & Site Access eShop.com grants you a limited \\r\\nsub-license to access and make personal use of this site and not to \\r\\ndownload (other than page caching) or modify it, or any portion of it, \\r\\nexcept with express written consent of eShop.com. This license does\\r\\n not include any resale or commercial use of this site or its contents; \\r\\nany collection and use of any product listings, descriptions, or prices;\\r\\n any derivative use of this site or its contents; any downloading or \\r\\ncopying of account information for the benefit of another merchant; or \\r\\nany use of data mining, robots, or similar data gathering and extraction\\r\\n tools. This site or any portion of this site may not be reproduced, \\r\\nduplicated, copied, sold, resold, visited or otherwise exploited for any\\r\\n commercial purpose without express written consent of eShop.com. \\r\\nYou may not frame or utilize framing techniques to enclose any \\r\\ntrademark, logo, or other proprietary information (including images, \\r\\ntext, page layout, or form) of the Site or of eShop.com and its \\r\\naffiliates without express written consent. You may not use any meta \\r\\ntags or any other \\\"hidden text\\\" utilizing the Site’s or eShop.com’s\\r\\n name or eShop.com’s name or trademarks without the express written\\r\\n consent of eShop.com. Any unauthorized use, terminates the \\r\\npermission or license granted by eShop.com</p>\\r\\n\\r\\n<p>Account & Registration Obligations All shoppers have to register \\r\\nand login for placing orders on the Site. You have to keep your account \\r\\nand registration details current and correct for communications related \\r\\nto your purchases from the site. By agreeing to the terms and \\r\\nconditions, the shopper agrees to receive promotional communication and \\r\\nnewsletters upon registration. The customer can opt out either by \\r\\nunsubscribing in \\\"My Account\\\" or by contacting the customer service.</p>\\r\\n\\r\\n<p>Pricing All the products listed on the Site will be sold at MRP \\r\\nunless otherwise specified. The prices mentioned at the time of ordering\\r\\n will be the prices charged on the date of the delivery. Although prices\\r\\n of most of the products do not fluctuate on a daily basis but some of \\r\\nthe commodities and fresh food prices do change on a daily basis. In \\r\\ncase the prices are higher or lower on the date of delivery not \\r\\nadditional charges will be collected or refunded as the case may be at \\r\\nthe time of the delivery of the order.</p>\\r\\n\\r\\n<p>Cancellation by Site / Customer You as a customer can cancel your \\r\\norder anytime up to the cut-off time of the slot for which you have \\r\\nplaced an order by calling our customer service. In such a case we will \\r\\nCredit your wallet against any payments already made by you for the \\r\\norder. If we suspect any fraudulent transaction by any customer or any \\r\\ntransaction which defies the terms & conditions of using the \\r\\nwebsite, we at our sole discretion could cancel such orders. We will \\r\\nmaintain a negative list of all fraudulent transactions and customers \\r\\nand would deny access to them or cancel any orders placed by them.</p>\\r\\n\\r\\n<p>Return & Refunds We have a \\\"no questions asked return policy\\\" \\r\\nwhich entitles all our Delivery Ambassadors to return the product at the\\r\\n time of delivery if due to any reason they are not satisfied with the \\r\\nquality or freshness of the product. We will take the returned product \\r\\nback with us and issue a credit note for the value of the return \\r\\nproducts which will be credited to your account on the Site. This can be\\r\\n used to pay your subsequent shopping bills. Refund will be processed \\r\\nthrough same online mode within 7 working days.</p>\\r\\n\\r\\n<p><br>\\r\\nDelivery & Shipping Charge</p>\\r\\n\\r\\n<p>1.You can expect to receive your order depending on the delivery option you have chosen.</p>\\r\\n\\r\\n<p>2.You can order 24*7 in website & mobile application , Our \\r\\ndelivery timeings are between 06:00 AM - 02:00PM Same day delivery.</p>\\r\\n\\r\\n<p>3.You will get free shipping on order amount above Rs.150.<br>\\r\\nYou Agree and Confirm<br>\\r\\n1. That in the event that a non-delivery occurs on account of a mistake \\r\\nby you (i.e. wrong name or address or any other wrong information) any \\r\\nextra cost incurred by eShop. for redelivery shall be claimed from \\r\\nyou.<br>\\r\\n2. That you will use the services provided by the Site, its affiliates, \\r\\nconsultants and contracted companies, for lawful purposes only and \\r\\ncomply with all applicable laws and regulations while using and \\r\\ntransacting on the Site.<br>\\r\\n3. You will provide authentic and true information in all instances \\r\\nwhere such information is requested you. eShop reserves the right \\r\\nto confirm and validate the information and other details provided by \\r\\nyou at any point of time. If upon confirmation your details are found \\r\\nnot to be true (wholly or partly), it has the right in its sole \\r\\ndiscretion to reject the registration and debar you from using the \\r\\nServices and / or other affiliated websites without prior intimation \\r\\nwhatsoever.<br>\\r\\n4. That you are accessing the services available on this Site and \\r\\ntransacting at your sole risk and are using your best and prudent \\r\\njudgment before entering into any transaction through this Site.<br>\\r\\n5. That the address at which delivery of the product ordered by you is to be made will be correct and proper in all respects.<br>\\r\\n6. That before placing an order you will check the product description \\r\\ncarefully. By placing an order for a product you agree to be bound by \\r\\nthe conditions of sale included in the item\\\'s description.</p>\\r\\n\\r\\n<p>You may not use the Site for any of the following purposes:<br>\\r\\n1. Disseminating any unlawful, harassing, libelous, abusive, \\r\\nthreatening, harmful, vulgar, obscene, or otherwise objectionable \\r\\nmaterial.<br>\\r\\n2. Transmitting material that encourages conduct that constitutes a \\r\\ncriminal offense or results in civil liability or otherwise breaches any\\r\\n relevant laws, regulations or code of practice.<br>\\r\\n3. Gaining unauthorized access to other computer systems.<br>\\r\\n4. Interfering with any other person\\\'s use or enjoyment of the Site.<br>\\r\\n5. Breaching any applicable laws;<br>\\r\\n6. Interfering or disrupting networks or websites connected to the Site.<br>\\r\\n7. Making, transmitting, or storing electronic copies of materials protected by copyright without the permission of the owner.</p>\\r\\n\\r\\n<p>Colors we have made every effort to display the colors of our \\r\\nproducts that appear on the Website as accurately as possible. However, \\r\\nas the actual colors you see will depend on your monitor, we cannot \\r\\nguarantee that your monitor\\\'s display of any color will be accurate.</p>\\r\\n\\r\\n<p>Modification of Terms & Conditions of Service eShop may at \\r\\nany time modify the Terms & Conditions of Use of the Website without\\r\\n any prior notification to you. You can access the latest version of \\r\\nthese Terms & Conditions at any given time on the Site. You should \\r\\nregularly review the Terms & Conditions on the Site. In the event \\r\\nthe modified Terms & Conditions is not acceptable to you, you should\\r\\n discontinue using the Service. However, if you continue to use the \\r\\nService you shall be deemed to have agreed to accept and abide by the \\r\\nmodified Terms & Conditions of Use of this Site.</p>\\r\\n\\r\\n<p>Governing Law and Jurisdiction This User Agreement shall be construed\\r\\n in accordance with the applicable laws of India. The Courts at \\r\\nFaridabad shall have exclusive jurisdiction in any proceedings arising \\r\\nout of this agreement. Any dispute or difference either in \\r\\ninterpretation or otherwise, of any terms of this User Agreement between\\r\\n the parties hereto, the same shall be referred to an independent \\r\\narbitrator who will be appointed by eShop and his decision shall be\\r\\n final and binding on the parties hereto. The above arbitration shall be\\r\\n in accordance with the Arbitration and Conciliation Act, 1996 as \\r\\namended from time to time. The arbitration shall be held in Nagpur. The \\r\\nHigh Court of judicature at Nagpur Bench of Mumbai High Court alone \\r\\nshall have the jurisdiction and the Laws of India shall apply.</p>\\r\\n\\r\\n<p>Reviews, Feedback, Submissions All reviews, comments, feedback, \\r\\npostcards, suggestions, ideas, and other submissions disclosed, \\r\\nsubmitted or offered to the Site on or by this Site or otherwise \\r\\ndisclosed, submitted or offered in connection with your use of this Site\\r\\n (collectively, the \\\"Comments\\\") shall be and remain the property of \\r\\neShop Such disclosure, submission or offer of any Comments shall \\r\\nconstitute an assignment to eShop of all worldwide rights, titles \\r\\nand interests in all copyrights and other intellectual properties in the\\r\\n Comments. Thus, eShop owns exclusively all such rights, titles and\\r\\n interests and shall not be limited in any way in its use, commercial or\\r\\n otherwise, of any Comments. eShopwill be entitled to use, \\r\\nreproduce, disclose, modify, adapt, create derivative works from, \\r\\npublish, display and distribute any Comments you submit for any purpose \\r\\nwhatsoever, without restriction and without compensating you in any way.\\r\\n eShop is and shall be under no obligation (1) to maintain any \\r\\nComments in confidence; (2) to pay you any compensation for any \\r\\nComments; or (3) to respond to any Comments. You agree that any Comments\\r\\n submitted by you to the Site will not violate this policy or any right \\r\\nof any third party, including copyright, trademark, privacy or other \\r\\npersonal or proprietary right(s), and will not cause injury to any \\r\\nperson or entity. You further agree that no Comments submitted by you to\\r\\n the Website will be or contain libelous or otherwise unlawful, \\r\\nthreatening, abusive or obscene material, or contain software viruses, \\r\\npolitical campaigning, commercial solicitation, chain letters, mass \\r\\nmailings or any form of \\\"spam\\\". eShop does not regularly review \\r\\nposted Comments, but does reserve the right (but not the obligation) to \\r\\nmonitor and edit or remove any Comments submitted to the Site. You grant\\r\\n eShopthe right to use the name that you submit in connection with \\r\\nany Comments. You agree not to use a false email address, impersonate \\r\\nany person or entity, or otherwise mislead as to the origin of any \\r\\nComments you submit. You are and shall remain solely responsible for the\\r\\n content of any Comments you make and you agree to indemnify eShop \\r\\nand its affiliates for all claims resulting from any Comments you \\r\\nsubmit. eShop and its affiliates take no responsibility and assume \\r\\nno liability for any Comments submitted by you or any third party.</p>\\r\\n\\r\\n<p>Copyright & Trademark eShop.com and eShop.com, its \\r\\nsuppliers and licensors expressly reserve all intellectual property \\r\\nrights in all text, programs, products, processes, technology, content \\r\\nand other materials, which appear on this Site. Access to this Website \\r\\ndoes not confer and shall not be considered as conferring upon anyone \\r\\nany license under any of eShop.com or any third party\\\'s \\r\\nintellectual property rights. All rights, including copyright, in this \\r\\nwebsite are owned by or licensed to eShop.com from eShop.com. \\r\\nAny use of this website or its contents, including copying or storing it\\r\\n or them in whole or part, other than for your own personal, \\r\\nnon-commercial use is prohibited without the permission of \\r\\neShop.com and/or eShop.com. You may not modify, distribute or \\r\\nre-post anything on this website for any purpose.The names and logos and\\r\\n all related product and service names, design marks and slogans are the\\r\\n trademarks or service marks of eShop.com, eShop.com, its \\r\\naffiliates, its partners or its suppliers. All other marks are the \\r\\nproperty of their respective companies. No trademark or service mark \\r\\nlicense is granted in connection with the materials contained on this \\r\\nSite. Access to this Site does not authorize anyone to use any name, \\r\\nlogo or mark in any manner.References on this Site to any names, marks, \\r\\nproducts or services of third parties or hypertext links to third party \\r\\nsites or information are provided solely as a convenience to you and do \\r\\nnot in any way constitute or imply eShop.com or eShop.com\\\'s \\r\\nendorsement, sponsorship or recommendation of the third party, \\r\\ninformation, product or service. eShop.com or eShop.com is not\\r\\n responsible for the content of any third party sites and does not make \\r\\nany representations regarding the content or accuracy of material on \\r\\nsuch sites. If you decide to link to any such third party websites, you \\r\\ndo so entirely at your own risk. All materials, including images, text, \\r\\nillustrations, designs, icons, photographs, programs, music clips or \\r\\ndownloads, video clips and written and other materials that are part of \\r\\nthis Website (collectively, the \\\"Contents\\\") are intended solely for \\r\\npersonal, non-commercial use. You may download or copy the Contents and \\r\\nother downloadable materials displayed on the Website for your personal \\r\\nuse only. No right, title or interest in any downloaded materials or \\r\\nsoftware is transferred to you as a result of any such downloading or \\r\\ncopying. You may not reproduce (except as noted above), publish, \\r\\ntransmit, distribute, display, modify, create derivative works from, \\r\\nsell or participate in any sale of or exploit in any way, in whole or in\\r\\n part, any of the Contents, the Website or any related software. All \\r\\nsoftware used on this Website is the property of eShop.com or its \\r\\nlicensees and suppliers and protected by Indian and international \\r\\ncopyright laws. The Contents and software on this Website may be used \\r\\nonly as a shopping resource. Any other use, including the reproduction, \\r\\nmodification, distribution, transmission, republication, display, or \\r\\nperformance, of the Contents on this Website is strictly prohibited. \\r\\nUnless otherwise noted, all Contents are copyrights, trademarks, trade \\r\\ndress and/or other intellectual property owned, controlled or licensed \\r\\nby eShop.com, one of its affiliates or by third parties who have \\r\\nlicensed their materials to eShop.com and are protected by Indian \\r\\nand international copyright laws. The compilation (meaning the \\r\\ncollection, arrangement, and assembly) of all Contents on this Website \\r\\nis the exclusive property of eShop.com and eShop.com and is \\r\\nalso protected by Indian and international copyright laws.</p>\\r\\n\\r\\n<p>Objectionable Material You understand that by using this Site or any \\r\\nservices provided on the Site, you may encounter Content that may be \\r\\ndeemed by some to be offensive, indecent, or objectionable, which \\r\\nContent may or may not be identified as such. You agree to use the Site \\r\\nand any service at your sole risk and that to the fullest extent \\r\\npermitted under applicable law, eShop.com and/or eShop.com and\\r\\n its affiliates shall have no liability to you for Content that may be \\r\\ndeemed offensive, indecent, or objectionable to you.</p>\\r\\n\\r\\n<p>Indemnity You agree to defend, indemnify and hold harmless \\r\\neShop.com, eShop.com, its employees, directors, Coordinators, \\r\\nofficers, agents, interns and their successors and assigns from and \\r\\nagainst any and all claims, liabilities, damages, losses, costs and \\r\\nexpenses, including attorney\\\'s fees, caused by or arising out of claims \\r\\nbased upon your actions or inactions, which may result in any loss or \\r\\nliability to eShop.com or eShop.com or any third party \\r\\nincluding but not limited to breach of any warranties, representations \\r\\nor undertakings or in relation to the non-fulfillment of any of your \\r\\nobligations under this User Agreement or arising out of the violation of\\r\\n any applicable laws, regulations including but not limited to \\r\\nIntellectual Property Rights, payment of statutory dues and taxes, claim\\r\\n of libel, defamation, violation of rights of privacy or publicity, loss\\r\\n of service by other subscribers and infringement of intellectual \\r\\nproperty or other rights. This clause shall survive the expiry or \\r\\ntermination of this User Agreement.</p>\\r\\n\\r\\n<p>Termination This User Agreement is effective unless and until \\r\\nterminated by either you or eShop.com. You may terminate this User \\r\\nAgreement at any time, provided that you discontinue any further use of \\r\\nthis Site. eShop.com may terminate this User Agreement at any time \\r\\nand may do so immediately without notice, and accordingly deny you \\r\\naccess to the Site, Such termination will be without any liability to \\r\\neShop.com. Upon any termination of the User Agreement by either you\\r\\n or eShop.com, you must promptly destroy all materials downloaded \\r\\nor otherwise obtained from this Site, as well as all copies of such \\r\\nmaterials, whether made under the User Agreement or otherwise. \\r\\neShop.com\\\'s right to any Comments shall survive any termination of \\r\\nthis User Agreement. Any such termination of the User Agreement shall \\r\\nnot cancel your obligation to pay for the product already ordered from \\r\\nthe Website or affect any liability that may have arisen under the User \\r\\nAgreement.</p>'),
(4, 'fcm_server_key', 'AAAA1l-jsgI:APA91bEyI88vdVZfttZiivj_LFFKiTi1ACCHQtIfBjg4-UJLsvTIezSNrdzYTGeiZ-92-yjkGNFGyUNedEstVFrlNZ7fOGnxqAc4xBZelBREf1JUJ7fbCRoW0O0yYpIgAJdSSpJpiE-8'),
(5, 'contact_us', '<h2><strong>Contact Us</strong></h2>\\r\\n\\r\\n<p>For any kind of queries related to products, orders or services feel free to contact us on our official email address or phone number as given below :</p>\\r\\n\\r\\n<p> </p>\\r\\n\\r\\n<h3><strong>Areas we deliver : </strong></h3>\\r\\n\\r\\n<p> </p>\\r\\n\\r\\n<h3><strong>Delivery Timings :</strong></h3>\\r\\n\\r\\n<ol>\\r\\n <li><strong>  8:00 AM To 10:30 AM</strong></li>\\r\\n <li><strong>10:30 AM To 12:30 PM</strong></li>\\r\\n <li><strong>  4:00 PM To  7:00 PM</strong></li></ol><h3> <strong></strong>\\r\\n\\r\\n</h3><p><strong>Note : </strong>You can order for maximum 2days in advance. i.e., Today & Tomorrow only.  <br></p>'),
(6, 'system_settings', '{\"system_configurations\":\"1\",\"system_timezone_gmt\":\"+07:00\",\"system_configurations_id\":\"13\",\"app_name\":\"Awesome\",\"support_number\":\"9876543210\",\"support_email\":\"awesomestudiokh@gmail.com\",\"current_version\":\"1.0.0\",\"minimum_version_required\":\"1.0.0\",\"is_version_system_on\":\"1\",\"area_wise_delivery_charge\":\"0\",\"currency\":\"$\",\"delivery_charge\":\"0\",\"min_amount\":\"0\",\"system_timezone\":\"Asia\\/Phnom_Penh\",\"is_refer_earn_on\":\"0\",\"min_refer_earn_order_amount\":\"\",\"refer_earn_bonus\":\"\",\"refer_earn_method\":\"\",\"max_refer_earn_amount\":\"\",\"refer_earn_bonus_times\":\"\",\"minimum_cart_amt\":\"200\",\"max_items_cart\":\"12\",\"delivery_boy_bonus_percentage\":\"1\",\"max_product_return_days\":\"1\",\"is_delivery_boy_otp_setting_on\":\"1\",\"cart_btn_on_list\":\"0\",\"expand_product_images\":\"0\",\"tax_name\":\"\",\"tax_number\":\"\"}'),
(7, 'payment_method', '{\"paypal_payment_method\":\"0\",\"paypal_mode\":\"sandbox\",\"paypal_business_email\":\"seller@somedomain.com\",\"currency_code\":\"USD\",\"razorpay_payment_method\":\"0\",\"razorpay_key_id\":\"rzp_test_key\",\"razorpay_secret_key\":\"secret_key\",\"paystack_payment_method\":\"0\",\"paystack_key_id\":\"paystack_public_key\",\"paystack_secret_key\":\"paystack_secret_key\",\"stripe_payment_method\":\"0\",\"stripe_payment_mode\":\"test\",\"stripe_publishable_key\":\"test_key\",\"stripe_secret_key\":\"test_key\",\"stripe_webhook_secret_key\":\"webhook_secret\",\"stripe_currency_code\":\"INR\",\"flutterwave_payment_method\":\"0\",\"flutterwave_public_key\":\"public_key\",\"flutterwave_secret_key\":\"secret_key\",\"flutterwave_encryption_key\":\"enc_key\",\"flutterwave_currency_code\":\"NGN\",\"paytm_payment_method\":\"0\",\"paytm_payment_mode\":\"sandbox\",\"paytm_merchant_key\":\"merchant_key\",\"paytm_merchant_id\":\"merchant_id\",\"cod_method\":\"1\"}'),
(8, 'about_us', '<p>At Awesome, we believe every kid can explore the world through reading and playing. We have everything from STEM toys to a variety of books that engage your kids with knowledge and excitement.</p><div><br></div>'),
(9, 'currency', '$'),
(11, 'email_settings', '{\"email\":\"your@gmail.com\",\"password\":\"your_mail_password\",\"smtp_host\":\"smtp.gmail.com\",\"smtp_port\":\"465\",\"mail_content_type\":\"html\",\"smtp_encryption\":\"ssl\"}'),
(12, 'time_slot_config', '{\"time_slot_config\":\"1\",\"is_time_slots_enabled\":\"0\",\"delivery_starts_from\":\"4\",\"allowed_days\":\"7\"}'),
(13, 'favicon', 'uploads/media/2021/Logo-Mark.png'),
(14, 'delivery_boy_privacy_policy', '<p>ACCESSING, BROWSING OR OTHERWISE USING THE WEBSITE eShop.com, Missed Call Service or mobile application INDICATES user is in AGREEMENT with eShop vegetables & fruits Pvt Ltd for ALL THE TERMS AND CONDITIONS MENTIONED henceforth. User is requested to READ terms and conditions CAREFULLY BEFORE PROCEEDING FURTHER.<br>User is the person, group of person, company, trust, society, legal entity, legal personality or anyone who visits website, mobile app or gives missed call or places order with eShop via phone or website or mobile application or browse through website www.eShop.com.</p><p>eShop reserves the right to add, alter, change, modify or delete any of these terms and conditions at any time without prior information. The altered terms and conditions becomes binding on the user since the moment same are unloaded on the website www.eShop.com</p><p>eShop is in trade of fresh fruits and vegetables and delivers the order to home (user’s desired address) directly.</p><p>That any user who gives missed call/call for order on any number published/used by eShop.com, consents to receive, accept calls and messages or any after communication from eShop vegetables & fruits Pvt Ltd for Promotion and Telemarketing Purposes within a week.</p><p>If a customer do not wish to receive any communication from eShop, please SMS NO OFFERS to 9512512125.</p><p>eShop accept orders on all seven days and user will receive the delivery next day from date of order placement, as we at eShop procure the fresh produce from the procurement center and deliver it straight to user.</p><p>There is Minimum Order value of Rs. 200. There are no delivery charges on an order worth Rs. 200 or above. In special cases, if permitted, order value is less then Rs. 200/– , Rs. 40 as shipping charges shall be charged from user.</p><p>eShop updates the prices on daily basis and the price displayed at our website www.eShop.com, at the time of placement of order by user he/she/it will be charged as per the price listed at the website www.eShop.com.</p><p>In the event, though there are remote possibilities, of wrong invoice generation due to any reason, in case it happens eShop vegetables & fruits Pvt Ltd reserve its right to again raise the correct invoice at the revised amount and same shall be paid by user.</p><p>At times it is difficult to weigh certain vegetables or fruits exactly as per the order or desired quantity of user, hence the delivery might be with five percent variation on both higher or lower side of exact ordered quantity, user are hereby under takes to pay to eShop vegetables & fruits Pvt Ltd as per the final invoice. We at eShop understands and our endeavor is to always deliver in exact quantity in consonance with quantity ordered but every time it’s not possible but eShop guarantee the fair deal and weight to all its users. eShop further assures its users that at no instance delivery weights/quantity vary dramatically from what quantity ordered by user.</p><p>If some product is not available or is not of good quality, the same item will not be delivered and will be adjusted accordingly in the invoice; all rights in this regards are reserved with eShop. Images of Fruits & Vegetables present in the website are for demonstration purpose and may not resemble exactly in size, colour, weight, contrast etc; though we assure our best to maintain the best quality in product, which is being our foremost commitment to the customer.</p><p>All orders placed before 11 PM in the Night will be delivered next day or as per delivery date chosen.</p>'),
(15, 'delivery_boy_terms_conditions', '<p>ACCESSING, BROWSING OR OTHERWISE USING THE WEBSITE eShop.com, Missed Call Service or mobile application INDICATES user is in AGREEMENT with eShop vegetables & fruits Pvt Ltd for ALL THE TERMS AND CONDITIONS MENTIONED henceforth. User is requested to READ terms and conditions CAREFULLY BEFORE PROCEEDING FURTHER.<br>User is the person, group of person, company, trust, society, legal entity, legal personality or anyone who visits website, mobile app or gives missed call or places order with eShop via phone or website or mobile application or browse through website www.eShop.com.</p><p>eShop reserves the right to add, alter, change, modify or delete any of these terms and conditions at any time without prior information. The altered terms and conditions becomes binding on the user since the moment same are unloaded on the website www.eShop.com</p><p>eShop is in trade of fresh fruits and vegetables and delivers the order to home (user’s desired address) directly.</p><p>That any user who gives missed call/call for order on any number published/used by eShop.com, consents to receive, accept calls and messages or any after communication from eShop vegetables & fruits Pvt Ltd for Promotion and Telemarketing Purposes within a week.</p><p>If a customer do not wish to receive any communication from eShop, please SMS NO OFFERS to 9512512125.</p><p>eShop accept orders on all seven days and user will receive the delivery next day from date of order placement, as we at eShop procure the fresh produce from the procurement center and deliver it straight to user.</p><p>There is Minimum Order value of Rs. 200. There are no delivery charges on an order worth Rs. 200 or above. In special cases, if permitted, order value is less then Rs. 200/– , Rs. 40 as shipping charges shall be charged from user.</p><p>eShop updates the prices on daily basis and the price displayed at our website www.eShop.com, at the time of placement of order by user he/she/it will be charged as per the price listed at the website www.eShop.com.</p><p>In the event, though there are remote possibilities, of wrong invoice generation due to any reason, in case it happens eShop vegetables & fruits Pvt Ltd reserve its right to again raise the correct invoice at the revised amount and same shall be paid by user.</p><p>At times it is difficult to weigh certain vegetables or fruits exactly as per the order or desired quantity of user, hence the delivery might be with five percent variation on both higher or lower side of exact ordered quantity, user are hereby under takes to pay to eShop vegetables & fruits Pvt Ltd as per the final invoice. We at eShop understands and our endeavor is to always deliver in exact quantity in consonance with quantity ordered but every time it’s not possible but eShop guarantee the fair deal and weight to all its users. eShop further assures its users that at no instance delivery weights/quantity vary dramatically from what quantity ordered by user.</p><p>If some product is not available or is not of good quality, the same item will not be delivered and will be adjusted accordingly in the invoice; all rights in this regards are reserved with eShop. Images of Fruits & Vegetables present in the website are for demonstration purpose and may not resemble exactly in size, colour, weight, contrast etc; though we assure our best to maintain the best quality in product, which is being our foremost commitment to the customer.</p><p>All orders placed before 11 PM in the Night will be delivered next day or as per delivery date chosen.</p>'),
(16, 'web_logo', 'uploads/media/2020/logo-460x11411.png'),
(17, 'web_favicon', 'uploads/media/2020/favicon-64.png'),
(18, 'web_settings', '{\"site_title\":\"eShop - ecommerce\",\"support_number\":\"9876543210\",\"support_email\":\"support@eshop.com\",\"address\":\"\",\"app_short_description\":\"\",\"map_iframe\":\"\",\"logo\":\"uploads\\/media\\/2020\\/logo-460x11411.png\",\"favicon\":\"uploads\\/media\\/2020\\/favicon-64.png\",\"meta_keywords\":\"eShop - eCommerce Online Store\",\"meta_description\":\"eShop - eCommerce Online Store\",\"app_download_section\":true,\"app_download_section_title\":\"eShop Mobile App\",\"app_download_section_tagline\":\"Download eshop\",\"app_download_section_short_description\":\"Shop with us at affordable prices and get exciting cashback & offers.\",\"app_download_section_playstore_url\":\"#\",\"app_download_section_appstore_url\":\"#\",\"twitter_link\":\"#\",\"facebook_link\":\"#\",\"instagram_link\":\"#\",\"youtube_link\":\"#\",\"shipping_mode\":true,\"shipping_title\":\"Free Shipping\",\"shipping_description\":\"Free Shipping at your doorstep.\",\"return_mode\":true,\"return_title\":\"Free Returns\",\"return_description\":\"Free return if products are damaged.\",\"support_mode\":true,\"support_title\":\"Support 24\\/7\",\"support_description\":\"24\\/7 and 365 days support is available.\",\"safety_security_mode\":true,\"safety_security_title\":\"100% Safe & Secure\",\"safety_security_description\":\"100% safe & secure.\"}');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `type`, `type_id`, `image`, `date_added`) VALUES
(9, 'categories', 20, 'uploads/media/2021/a_bOOk.png', '2021-09-16 07:13:48'),
(11, 'categories', 20, 'uploads/media/2021/311.png', '2021-09-16 07:14:57'),
(10, 'categories', 20, 'uploads/media/2021/Copy_of_Facebook_Cover_(2).png', '2021-09-16 07:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percentage` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `slug` varchar(32) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `slug`, `image`, `is_default`, `status`, `created_on`) VALUES
(1, 'Classic', 'classic', 'classic.jpg', 1, 1, '2021-02-26 14:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `time_slots`
--

CREATE TABLE `time_slots` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `last_order_time` time NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `transaction_type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txn_id` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payu_txn_id` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payer_email` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_date` timestamp NULL DEFAULT current_timestamp(),
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `updates`
--

CREATE TABLE `updates` (
  `id` int(11) NOT NULL,
  `version` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `updates`
--

INSERT INTO `updates` (`id`, `version`) VALUES
(1, '1.0'),
(2, '1.1'),
(3, '1.1.1'),
(4, '1.1.2'),
(5, '2.0.0'),
(6, '2.0.1'),
(7, '2.0.2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` double DEFAULT 0,
  `activation_selector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_selector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_time` int(11) DEFAULT NULL,
  `remember_selector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bonus` int(11) DEFAULT NULL,
  `dob` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `city` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apikey` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friends_code` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `mobile`, `image`, `balance`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `address`, `bonus`, `dob`, `country_code`, `city`, `area`, `street`, `pincode`, `apikey`, `referral_code`, `friends_code`, `fcm_id`, `latitude`, `longitude`, `created_at`) VALUES
(1, '127.0.0.1', 'Administrator', '$2y$12$sBCMK.mZ/EMKmd8sIqIlnedHVbN2YTZz0hzPkfaJk49YuHR7K.bdS', '', '9876543210', NULL, 1493, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1268889823, 1636472412, 1, 'ADMIN', NULL, NULL, NULL, 91, '44', '138', NULL, NULL, NULL, 'p7qgPvqf', NULL, 'dTzJD3t9TwyYVO2pkX7jXj:APA91bGxL5FClPRxZ7hlXHEbn8lXs6qpq_BNJrHVKuOBZHETFD47xImej2bviFK9zttHa54fea5iu4U8a5wI6CifU8owaXIupT-kWJnTZwj0fPo4k528Ht4keP_dnCjptPRl9IgxLc-D', NULL, NULL, '2020-06-30 10:20:08'),
(2, '110.235.247.218', 'Chhenglin', '$2y$10$hmbcln9mGMRmP5PWx6K0Dum9VAHwXRr95kpE2ZXU2oMyKFnEBa.vy', 'horchhenglin168@gmail.com', '89243003', NULL, 0, 'f271c8f7e0075639224e', '$2y$10$QAuJ6Y737jQzDTEuYTqd5ebWnuKZHu7mg/eOH0o7HW5TNf/8Q858W', NULL, NULL, NULL, NULL, NULL, 1624177093, 1634140710, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'NRUkNgBK', '', 'cTNDIaoto0LFhk0vpqqaDf:APA91bHmeoOFJxl4YUQnMu6_w1Z5hUfc9PDxkEAXdLdPFJf4jsZZ0_v3xxoMNEmugpFX934T5Lh89JBiAnjf2RF5q0GOkIzReQ5MY7RqglwelwH05pArHiswrS1IW1MiX2pePJlI57zZ', NULL, NULL, '2021-06-20 08:18:13'),
(3, '136.228.131.121', 'Hongna', '$2y$10$fVEp2dLHNnQ5yAJSAp8XZeJyLi0wYOs64ulW4GEJmGhhTBBCj.qPC', 'hongna.chea@gmail.com ', '087914999', '20210620_161023.jpg', 0, 'deac83f3a8ecf3545aae', '$2y$10$VYFGf4c5E1ysXO6vMD6RNediRl9UpfVw9jL/2N2r6dSIDNZCdhjN.', NULL, NULL, NULL, NULL, NULL, 1624177112, 1626241859, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, '2igkwa7K', '', 'fjN1wpVsS4KqDAhVUAWGf3:APA91bFOE0d9VKcVm0jm7tTlpaAUdU9cRwf1bjSjDlbnsln98E2q6TctkkwfWpVXs3eIgKb_3m15LhFTcstc__Rmpj_v0I8iV3fPJVzgIDlrsW55TTm1hpioYZd0quZN8g8dXRwM4ulN', NULL, NULL, '2021-06-20 08:18:32'),
(4, '110.235.247.218', 'Voathnak', '$2y$10$RNNYxADYfPSjkZKXTW83EucY4lKAqvZaM9IsXDLkki3G2yBa5/Dim', 'voathnak.lim@gmail.com', '12979873', NULL, 0, 'd00fcc2cebeabd4a0580', '$2y$10$yYwqnnZapqSZH8VngFnvTufAZo53qIb.EX5cpFiClPNEOfUYxV4gK', NULL, NULL, NULL, NULL, NULL, 1624181331, 1633184858, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'QNKDftbq', '', 'fpN7641enU7DpHgWk3LllB:APA91bEkI0urNWILE1FRadsopbeOeJUHeyuWYTy3gpJ3z4Zl9OrKvdk6Me-dlLib1J7uJMAyVEc7kImGW27Jm8wGC6VTMORYCoJc9bmExmZa74BNLjlQE9q68KsCPf1lcl_-6WokHNK3', NULL, NULL, '2021-06-20 09:28:51'),
(5, '27.109.113.211', 'Chingching', '$2y$10$fJoqN3JRdG85A/g77Xp0ae/LTSzEpN7FWx90gB/MQH.fze2l2j4.m', 'chingchingpu@gmail.com', '69860168', NULL, 0, 'f2a0c21f5143d89b6e8a', '$2y$10$2aQYjclejJmv61Evkr22PeZmx2ac2Q46aacZApk3dJ/R0flsgDZTe', NULL, NULL, NULL, NULL, NULL, 1624199730, 1631600478, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'WsmIbDMf', '', 'd5W4JCnSQ5uCTRRHgZ3BFq:APA91bE9mGpOHY2XvvJa-TQvXDz_RWE-VfxZnyO7DndSLHrLs_tq3qLIPmUI1MqSnxtrjJIsL9zELuaTxxcHfxj7ZLLHxaZx7TLYxuYvkKQYktI7YuyXtVxssv4sU3y5SQ87IboKPtEF', NULL, NULL, '2021-06-20 14:35:30'),
(6, '27.109.113.211', 'Chingching ', '$2y$10$MMD7KWK8Dvcmat2kY89mlOGzHfm9FKRJbkl6KJMJWfaHBq2cpZIMK', 'chingchingpu@gmail.com', '99695788', NULL, 0, '016c670b2bc3a6087dca', '$2y$10$42BzLzPgpJXQqCLRsUtkrOh8l1uE0zjFYt.uWxahct88jxqZxZ3ba', NULL, NULL, NULL, NULL, NULL, 1624200816, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'XSwvvN3j', '', 'efPZ7VPVQf-Z-xpvrVu4ek:APA91bGP98n7QxeKyWBUGbAhj3oZbC9jbafbVbXTp7mK_wwbANqc0wDfjWF82lRXSE8swn0LRdJJBaa0GtjdfyiI4RK_DBoXUKgnFsTTdNZGSdNnbtVfH86lGXIxRVpNkoUCntSrTu71', NULL, NULL, '2021-06-20 14:53:37'),
(7, '136.228.131.121', 'pheakdey', '$2y$10$vCTLmIFDzefTh/uUTB88q.UYtutWg7MDpbIyp2.iuh8yuStvPHH2i', 'pheakdeyarn2000@gmail.com', '085896069', NULL, 0, 'ca6d5ec96a4c88bc5382', '$2y$10$d8JalIv4iVBy03HJ28LcMuAJyJdsyDgmqaJoZqAwXn.Y7NQ9Ci94a', NULL, NULL, NULL, NULL, NULL, 1624421874, 1626581694, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'XkQFKYZx', '', 'fz0KMeTPRzavti7a0fvfOr:APA91bHIE1ItoVo7OLSmjT4zAeV4pnujLLWUIW7nqlvoPwkhMFbQG1LG6tFymvP7RQyGutzs1s98PBGF_bomkgr8qi7Lr6fbMfad6BuHvKHZZsuRbiharpIfj8bvFiRuJSSEmMNHUUoB', NULL, NULL, '2021-06-23 04:17:54'),
(8, '136.228.131.121', 'Thy', '$2y$10$FGPYElj8am55TVjDiXckZuifAwlOjlJuSk0FCX8xNR/fRZwNiWm.K', 'thy@gmail.com', '012241043', NULL, 0, '07f81c5196b424c569b9', '$2y$10$mRp/V5a.H1lYlSbjcvghNu96xsz.wzebFrT/2tQ9JfPR7cpjVT4oe', NULL, NULL, NULL, NULL, NULL, 1625393032, 1625393099, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'HruhHXMq', '', 'e6OAmrqjT9i7RifhJAJL0T:APA91bEqBNPlGgyNLJTSfGg2Qi9dT0gGFBkU9GyC12J0cWGB42hCOCR6uaH1yFe2i6Pl7CL3aqan_5aSjo_YaYrgZW-IEmMcRxQhrmwBsHfZSFJq2nu6AOshlYaM9sZ53WBHX37anNYN', NULL, NULL, '2021-07-04 10:03:52'),
(9, '136.228.131.121', 'Sour', '$2y$10$qFXPHQSG3H3QfcGxpu8Z3u5k7ffMBOeXLNJOVoL6CqkipGfA7F/hu', 'sour@gmail.com', '962474218', NULL, 0, '324b5b356d475067ed8a', '$2y$10$v6jld8G2Cq4ACwDFXexqoenrkzNO6zdYT.JG/WuasIauDY2F3o7hO', NULL, NULL, NULL, NULL, NULL, 1625452337, 1625453429, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'EBhpJnu5', '', 'e6OAmrqjT9i7RifhJAJL0T:APA91bEqBNPlGgyNLJTSfGg2Qi9dT0gGFBkU9GyC12J0cWGB42hCOCR6uaH1yFe2i6Pl7CL3aqan_5aSjo_YaYrgZW-IEmMcRxQhrmwBsHfZSFJq2nu6AOshlYaM9sZ53WBHX37anNYN', NULL, NULL, '2021-07-05 02:32:17'),
(10, '136.228.131.121', 'Rethy ', '$2y$10$R/AnFnBMspXKmtATW9h5hO5PWRN7ucIyfssZTZtxdvF4vb/W0l0O.', '', '16775063', 'Screenshot_2021_0328_094526.jpg', 0, 'd367c745838312842acb', '$2y$10$vU7H13MU1AB/lYJsVucavecT/2HFMmtQrOSVApcJ6xzJdd2GRUMTy', NULL, NULL, NULL, NULL, NULL, 1625453747, 1634434553, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, '4azeZZYv', '', 'com7txzyQ8Cu-cHTrn2tG9:APA91bGk78y4idhwrHJwid4JKy5YWgocRyEjCDJ7qRZ_TF28P2mse0BCEu5CzpaAttcD48H1lbOtC3IW84pSxGjlJSBqOuoJkoht-XtAx22R6oTdQelFExIl-eb8ZDGL31HCu091vVFs', NULL, NULL, '2021-07-05 02:55:48'),
(11, '136.228.131.121', 'chithav', '$2y$10$6n3cEpGPxK9LyP7sbh/r4.ltyKp4/A2clXHZoGdS8TqVchmTCyr0e', 'chithav@example.com', '11922024', NULL, 0, 'd9e05161aa8433e8c1e6', '$2y$10$fiJfjiI5wXeXgJ3HI55JP.YwR4C41U8rVAIqX8jNEdxA1Nvr5cJL.', NULL, NULL, NULL, NULL, NULL, 1625455605, 1630390458, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'H56iIIuV', '', 'cv7TLDYES6iHg5lSz2wjeG:APA91bEb7GkGuFkGkf7dEn8XPEhTfLacIgUVc5f5Ic3_iR16d6Icl8c9E4sbbAYX_bvIEycj3Xd_qKF18YsMKyBurDXqvJ9sObmTfQXf7PZJjmR0A6aL1W0NPZUyXkj_2NJHmLz4QPA5', NULL, NULL, '2021-07-05 03:26:45'),
(12, '136.228.131.121', 'hxhxhch', '$2y$10$C07q.JNeaj7OFDs.8z5PH.Btm8LzepNEzS5xC2wPaXi.6DkQCGI.S', 'thy@gmail.com', '70404283', NULL, 0, 'abb694cac8935a9ac1d7', '$2y$10$phVoCF1S7Cw4oyLo6kujpeHC61td.A99AQ6Po7p7MeMw1iP/hnXZS', NULL, NULL, NULL, NULL, NULL, 1625469494, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'nkuwBt87', '', 'e6OAmrqjT9i7RifhJAJL0T:APA91bEqBNPlGgyNLJTSfGg2Qi9dT0gGFBkU9GyC12J0cWGB42hCOCR6uaH1yFe2i6Pl7CL3aqan_5aSjo_YaYrgZW-IEmMcRxQhrmwBsHfZSFJq2nu6AOshlYaM9sZ53WBHX37anNYN', NULL, NULL, '2021-07-05 07:18:14'),
(13, '136.228.131.121', 'Kevin', '$2y$10$2lgq.tMlMoFG5oNuwqKfxOEJHXAhGAkrNTSD6Jhu1zsifI/tPMLTC', '', '967988599', NULL, 0, '6e3ffbb041e8509965e1', '$2y$10$jjhl69w1Uz6ZgXyuinLZrOZKmBEMmGaGXMeFe.cpoY4o.eI2xHO0i', NULL, NULL, NULL, NULL, NULL, 1625473958, 1626837056, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'VmA2ZNsz', NULL, 'fSjTpOAKTl2_MJYn9VfOt8:APA91bFaUcMnd_ouy5kGuSjWisYlHfuF-nFf5VmH-Fv6_i2UjTgKDnJoHqDWZWYdrXgx6GcAl-kHMapQnw0J1NUYDpyn-N2YMDsc4L5uJTyCaXSlT6I3Jbenu82iSX1jdpVctylUZVhC', NULL, NULL, '2021-07-05 08:32:38'),
(14, '136.228.131.121', 'Tom', '$2y$10$k3Etihmdq2VG4pYhfI1c9OVp/nFWLIFk0Xydz3AyOcOc0MDe92Wbq', '', '12241043', NULL, 0, 'cba8e00d2cb757652644', '$2y$10$o4c0EVx3DUMIkmKMzs/O1eNVSp8wOr3B9MhBXLDdKMiERiHUdtK1W', NULL, NULL, NULL, NULL, NULL, 1625625291, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'nrl2afx7', NULL, 'e6OAmrqjT9i7RifhJAJL0T:APA91bEqBNPlGgyNLJTSfGg2Qi9dT0gGFBkU9GyC12J0cWGB42hCOCR6uaH1yFe2i6Pl7CL3aqan_5aSjo_YaYrgZW-IEmMcRxQhrmwBsHfZSFJq2nu6AOshlYaM9sZ53WBHX37anNYN', NULL, NULL, '2021-07-07 02:34:51'),
(15, '136.228.131.121', 'bcbfbbf', '$2y$10$O0xgLefUpaJAC3X6RgAIeuHj.HBwhFss96gO6.SnMn5hVHZT9VET6', '', '012241044', NULL, 0, 'dd5a0255498b677df13f', '$2y$10$PbHAJK.tzirBbR6AN47Be.zUYj/MToVmwjoJEhjis4FvL0Kp6E7we', NULL, NULL, NULL, NULL, NULL, 1625628132, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'okA89AAW', NULL, 'e6OAmrqjT9i7RifhJAJL0T:APA91bEqBNPlGgyNLJTSfGg2Qi9dT0gGFBkU9GyC12J0cWGB42hCOCR6uaH1yFe2i6Pl7CL3aqan_5aSjo_YaYrgZW-IEmMcRxQhrmwBsHfZSFJq2nu6AOshlYaM9sZ53WBHX37anNYN', NULL, NULL, '2021-07-07 03:22:12'),
(16, '136.228.131.121', 'Thor Odinson', '$2y$10$C/pqIuzgAp9UkCcOAtFPGuh7yKhm/VbIp5ZviTnr57XenXKTpsffq', 'odinson@gmail.com', '85896069', NULL, 0, '68658e45992ece6891f2', '$2y$10$cd.FwKyapJUmBxj.fAXRbOUo3VzTivmRGDyvc7JH1e0E3OxzNnhwi', NULL, NULL, NULL, NULL, NULL, 1625992970, 1630391836, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'iMvVc6bb', '', 'dmltlwHYTE2zmjCiCJ_lwr:APA91bGPeRnZIbeYyoVhvwm8-oTVMS738hk0-VT1ckZ4Ex9YhHFEwrWK3JXTRERIrbkkSoUKEqH53NiqjmV72MlzFSTrtAmvED2U56AoE3xbZ7ZMALidfw4rFTbixqY53_RF0nhU5rmW', NULL, NULL, '2021-07-11 08:42:50'),
(17, '27.109.113.215', 'Hongna', '$2y$10$U757EZC/n5I8aCBKnAUTGu8UxJ/VlfHb7L3GsvA.aGbG2lJyI3N.W', '', '87914999', NULL, 0, '2642675726d4e64ce4f5', '$2y$10$DfxorAAK2CrqsJ/L9IM5HeSzCFfpuPUDnRg1IzAqztFL0BJpfxsNu', NULL, NULL, NULL, NULL, NULL, 1626617337, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, '3APjSsSy', NULL, 'fuPvCiYzTmaqlctlHU-znr:APA91bH8bWzMG-8wx3rJVLLmSBJEeGDjvZ-oWX-S_P3ecvRPQpWepAe8AuRcef_NbAphiNvLv__v3vJHcIyZuptI1M21aLSMapilM4rT3YsTRd2VXOlRc1HIIiuVV8c03aOcnM95lgPT', NULL, NULL, '2021-07-18 14:08:57'),
(18, '110.235.247.218', 'Lin', '$2y$10$18Omi8GiEKtigU0QpQAh0.c5NHVwfZ4S6KHLyMVqYKB.FUIeVXXb6', '', '12312423', NULL, 0, '8db7f47440b74c6c3adc', '$2y$10$0O.9VprXkaqrYp0m.fVDbuqJsiFPJ/1R7YkWLQSzLLs7ot5996mBi', NULL, NULL, NULL, NULL, NULL, 1626618506, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'vGYzXFrB', NULL, 'esANF6YGRLaPWlxijd4d6k:APA91bEgT79oagGHD9OWwXh1zEOjM4GH3WE0oNYnZ1e0t7oLKq9HbjkpQLPZBYDpxL55AwzUoFRIsjPb56jCk5VskMzuhDYGT3jk3kVjQAuS10TigJh3YIhH2sOtgQHsus3nrg9Vink6', NULL, NULL, '2021-07-18 14:28:26'),
(19, '203.144.68.23', 'Ded Ses', '$2y$10$02rhW1p0FM7H4fD8vaEvzOGg3Kt2.8OXl4.PAK1Khanw87Cog9fmi', '', '85949242', NULL, 0, 'dbe48b2a42c3ea54f4a6', '$2y$10$eVkjrkgTrDUYoLZbv3Wx7OfzOfNW5pBFPHRa9disYQXz6xcXHbKXC', NULL, NULL, NULL, NULL, NULL, 1627523668, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, '4hcTl3Cv', NULL, 'dU_TYJVfQ7W6QTVRujfGqT:APA91bHBvFDfxphBMfIIYgSpW0VEKc9sADbrp4kgyJZCm2xx4CpRMD56u0Pm7I7He1n0iPA4tMAgOc-0DqzHhWLgCzWNwGpfDFWJ4lsxGDVV87XKQZAjsSgxEo0Dl9UJEvzJnp8V00XX', NULL, NULL, '2021-07-29 01:54:28'),
(20, '136.228.131.204', 'Visal', '$2y$10$PmK2s2.z0wR4.dTEvmiYbuy6O5POXY9ckL./MdZGOKOhwLsNb8n1O', '', '92938490', NULL, 0, '32438c9907cfde08b7cc', '$2y$10$E25i06eWT.mA/5vLyzdlNuCJYVDUPfLpFffZWo1ZE8K4gx.KBIBsK', NULL, NULL, NULL, NULL, NULL, 1627534613, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'lEjHFWOb', NULL, 'dKPRXuUPTsO4aGvliVla2o:APA91bGkwO6W5IvkFnaWBkdCchwYMj1-Rnd8r1ElKpIWCU5LEr42Y8qKpcxc2T2rYAHj_-7P-jbpRQ7EV41yqC8_1JCsyLJm97Kr72A6Hz9QsAy7BtMHcETSXTDUXfNQ3ffmNswISa0K', NULL, NULL, '2021-07-29 04:56:53'),
(21, '110.74.215.228', 'Hong', '$2y$10$1T7v7lITLbyuwGIyJ7Sgg.M91xYZnaghlFl0pZBLyuPDfFnz1P.S.', '', '69850168', NULL, 0, '024e7f8b5b9be51a7dce', '$2y$10$4ekh9mCoYB9YkkzWPbWYUeJULB9vTpJtlaRU9LZkK7ohoB97G4fZq', NULL, NULL, NULL, NULL, NULL, 1631421973, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'yDOaJach', NULL, 'c3vhYS6fT_2zkCuEtDSSD-:APA91bHHAWRYsu5ZWcspSvCKdYuPyOmayyKmJNFltn9JSsjGm1EmyJUGorRXu5YXWaW7Kk5zyEbc3Yhf22qD3CoPCtp-RZdlMKSKZ1hgEoBIi3vU0zzS9bTG2nPVjN9g3jA34MrvUqMh', NULL, NULL, '2021-09-12 04:46:13'),
(22, '167.179.38.59', 'Likun', '$2y$10$LNeCGrSiYkyyi1.gHTCGMehQiIXLvGXU9LcCO7KkHZXnn3ruoLywC', '', '963885245', NULL, 0, 'ebbe00f5f843c2984df2', '$2y$10$TcHyZjAgMQJWsRVX.lQSo.524MiTn6g4IFUPGKXHqjyqEQJbhjCDq', NULL, NULL, NULL, NULL, NULL, 1632033819, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, '9SiSxJeR', NULL, 'ccWkIFrfRoqXpaCc28slWN:APA91bEKEmWhQutiU_UFQtEfRY8C11yWTTZuf2EWt1qDkjKINnTcUEsTShs3miIxlN2E3nfvDtSu-AFEiKK-2ShuRmlkk8Pkjxm1xY8OXwxvXP8AAgJ7VnIXVvQVsROQWLMFPy3GNg7e', NULL, NULL, '2021-09-19 06:43:39'),
(23, '136.228.131.121', 'hongna', '$2y$10$dHXF/Jgpy2doRVoU/f7VC.v8Q5baUPWecIP9dDv3qmZP83j3lYA8C', '', '99914999', NULL, 0, '56edfcc81001d68dc87b', '$2y$10$dc9Y41haMxQFWurcuQYW9uy/9hCg8snKI07xvCrBaBzme0iaw8aTO', NULL, NULL, NULL, NULL, NULL, 1633252469, NULL, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'Q2atNhXw', NULL, 'eFURQUTKqUeyk75-Pptymi:APA91bGu6LtDiDU4g3ZwwValM5Pi2MV_40NLqkF92i0eMzNmtDBMlcZneelP3n8gd4XVAvHDYArqg2yGhRmJiUiXrgs6dIJUx81sON1IbsJZ0C9EJxhBSX877od-TZbcAm72id4EqAEv', NULL, NULL, '2021-10-03 09:14:29'),
(24, '167.179.38.59', 'Kun', '$2y$10$L7BML47GQ1DMZO.8YwEuNuPRJUaytFJJ8ot42hm60.NbB3.TE6MFe', '', '70905565', NULL, 0, 'b6a43239ec390366081a', '$2y$10$fYcukQUYHkksgvkt/frvRO21vny2Pg698jzi8Vmh94b3WT6PRaRZ.', NULL, NULL, NULL, NULL, NULL, 1633972791, 1634128701, 1, NULL, NULL, NULL, NULL, 855, NULL, NULL, NULL, NULL, NULL, 'juNwc2Ru', NULL, 'eX3CECbpdkFIthwRdT3Dte:APA91bFDgrkL8psD0duN9GevjvFKywPvucFFD6ki0Jke-iR4FciJWU4a_eIaTNpvLbBIZZj_CYkN6743cxQIfIXMiLuqxY2zvylakBl_k5cNnm0tVKFo0QiXwGE-k04WFM_L12zttdVR', NULL, NULL, '2021-10-11 17:19:51');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2),
(10, 10, 2),
(11, 11, 2),
(12, 12, 2),
(13, 13, 2),
(14, 14, 2),
(15, 15, 2),
(16, 16, 2),
(17, 17, 2),
(18, 18, 2),
(19, 19, 2),
(20, 20, 2),
(21, 21, 2),
(22, 22, 2),
(23, 23, 2),
(24, 24, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `permissions` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `role`, `permissions`, `created_by`) VALUES
(1, 1, 0, NULL, '2020-11-18 04:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transactions`
--

CREATE TABLE `wallet_transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'credit | debit',
  `amount` double NOT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_updated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_set_id` (`attribute_set_id`);

--
-- Indexes for table `attribute_set`
--
ALTER TABLE `attribute_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_variant_id` (`product_variant_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_fc02df82_fk_categories_id` (`parent_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_api_keys`
--
ALTER TABLE `client_api_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_boy_notifications`
--
ALTER TABLE `delivery_boy_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_boy_id` (`delivery_boy_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `fund_transfers`
--
ALTER TABLE `fund_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_boy_id` (`delivery_boy_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `delivery_boy_id` (`delivery_boy_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_variant_id` (`product_variant_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `payment_requests`
--
ALTER TABLE `payment_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_a7a3a156_fk_categories_id` (`category_id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_rating`
--
ALTER TABLE `product_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_variants_product_id_019d9f04_fk_products_id` (`product_id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_requests`
--
ALTER TABLE `return_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `variable` (`variable`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_slots`
--
ALTER TABLE `time_slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `updates`
--
ALTER TABLE `updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile` (`mobile`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attribute_set`
--
ALTER TABLE `attribute_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `client_api_keys`
--
ALTER TABLE `client_api_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `delivery_boy_notifications`
--
ALTER TABLE `delivery_boy_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `fund_transfers`
--
ALTER TABLE `fund_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=437;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `payment_requests`
--
ALTER TABLE `payment_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=445;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_rating`
--
ALTER TABLE `product_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=444;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_requests`
--
ALTER TABLE `return_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `time_slots`
--
ALTER TABLE `time_slots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `updates`
--
ALTER TABLE `updates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_fc02df82_fk_categories_id` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_a7a3a156_fk_categories_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD CONSTRAINT `product_variants_product_id_019d9f04_fk_products_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
