# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

# Install some deps
WORKDIR /code
COPY requirements.txt /code
RUN pip install -r /code/requirements.txt
COPY . /code/
