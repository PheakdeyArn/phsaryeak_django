# PR Details

<!--- Provide a general summary of your changes in the Title above -->

## Description

<!--- Describe your changes in detail -->

## Related Issue or Trello Ticket Link

<!--- Please link to the issue here: -->

## How Has This Been Tested

<!--- Please drag and drop a screenshot of what you have done here. -->

## Types of changes

<!--- What types of changes does your code introduce? Put an `x` in all the boxes that apply: -->

- ( ) Docs change / refactoring / dependency upgrade
- ( ) Bug fix (non-breaking change which fixes an issue)
- ( ) New feature (non-breaking change which adds functionality)
- ( ) Breaking change (fix or feature that would cause existing functionality to change)
- ( ) Database schema changes

## Checklist (before merging)

<!--- Go over all the following points, and put an `x` in all the boxes that apply. -->
<!--- Mention N/A if not applicable. --->

- [ ] I have moved the issue to pull request list in trello
- [ ] I have tested create and update on old admin
- [ ] I have Linked pull request with the issue
