from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from eshop.models import Product, Categories, ProductVariant
from eshop.serializers import ProductSerializer, UserSerializer, CategoriesSerializer, ProductVariantSerializer





@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'products': reverse('product-list', request=request, format=format),
        'categories': reverse('category-list', request=request, format=format)
    })