from django.contrib import admin
from eshop.models import (
    Product,
    ProductVariant,
    Categories,
    Address,
    Area,
    Cart,
    City,
    ClientApiKey,
    DeliveryBoyNotification,
    Faq,
    Favorite,
    FundTransfer,
    Group,
    Language,
    LoginAttempt,
    Media,
    Notification,
    Order,
    OrderItem,
    ProductRating,
    Section,
    Slider,
    Transaction,
    UserGroup,
    UserPermission,
    Setting,
    User,
    Offer,
)


class AddressAdmin(admin.ModelAdmin):
    list_display = (
        'address',
        'user',
        'name',
        'type',
        'mobile',
        'alternate_mobile',
        'address',
        'landmark',
        'area',
        'city',
        'pincode',
    )


class CategoriesAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'slug',
        'banner',
        'row_order',
        'status',
        'parent',
        'image'
    )


class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'date_added',
        'category',
        'row_order',
        'stock_type',
        'tax',
        'type',
        'name',
        'short_description',
        'slug',
        'indicator',
        'cod_allowed',
        'minimum_order_quantity',
        'quantity_step_size',
        'total_allowed_quantity',
        'is_returnable',
        'is_cancelable',
        'cancelable_till',
        'image',
        'other_images',
        'video_type',
        'video',
        'tags',
        'warranty_period',
        'guarantee_period',
        'made_in',
        'sku',
        'stock',
        'availability',
        'rating',
        'no_of_ratings',
        'description',
        'status',
    )


class ProductVariantAdmin(admin.ModelAdmin):
    list_display = (
        'attribute_value_ids',
        'attribute_set',
        'price',
        'special_price',
        'sku',
        'images',
        'availability',
        'status',
        'date_added',
        'product_id',
        'stock',
    )


class AreaAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'city',
        'minimum_free_delivery_order_amount',
        'delivery_charges',
    )


class CartAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'product_variant',
        'qty',
        'is_saved_for_later',
        'date_created',
    )


class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ClientApiKeyAdmin(admin.ModelAdmin):
    list_display = ('name', 'secret', 'status')


class DeliveryBoyNotificationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'delivery_boy_id',
        'order_id',
        'title',
        'message',
        'type',
        'date_created',
    )


class FaqAdmin(admin.ModelAdmin):
    list_display = ('question', 'answer', 'status')


class FavoriteAdmin(admin.ModelAdmin):
    list_display = ('user', 'product')


class FundTransferAdmin(admin.ModelAdmin):
    list_display = (
        'delivery_boy_id',
        'opening_balance',
        'closing_balance',
        'amount',
        'status',
        'message',
        'date_created',
    )


class EshopGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


class LanguageAdmin(admin.ModelAdmin):
    list_display = (
        'language',
        'code',
        'is_rtl',
        'created_on',
    )


class LoginAttemptAdmin(admin.ModelAdmin):
    list_display = ('ip_address', 'login', 'time')


class MediaAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'name',
        'extension',
        'type',
        'sub_directory',
        'size',
        'date_created',
    )


class NofitifacionAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'message',
        'type',
        'type_id',
        'image',
        'date_sent',
    )


class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'delivery_boy_id',
        'mobile',
        'total',
        'delivery_charge',
        'wallet_balance',
        'total_payable',
        'promo_code',
        'promo_discount',
        'discount',
        'final_total',
        'payment_method',
        'latitude',
        'longitude',
        'address',
        'delivery_time',
        'delivery_date',
        'status',
        'active_status',
        'date_added',
        'otp',
    )


class OrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'order',
        'product_name',
        'variant_name',
        'product_variant',
        'quantity',
        'price',
        'discounted_price',
        'tax_percent',
        'tax_amount',
        'discount',
        'sub_total',
        'deliver_by',
        'status',
        'active_status',
        'date_added',
    )


class ProductRatingAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'product',
        'rating',
        'images',
        'comment',
        'data_added',
    )


class SectionAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'short_description',
        'style',
        'product_ids',
        'row_order',
        'categories',
        'product_type',
        'date_added',
    )


class SliderAdmin(admin.ModelAdmin):
    list_display = (
        'type',
        'type_id',
        'image',
        'date_added',
    )


class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        'transaction_type',
        'user_id',
        'order_id',
        'type',
        'txn_id',
        'payu_txn_id',
        'amount',
        'status',
        'currency_code',
        'payer_email',
        'message',
        'transaction_date',
        'date_created',
    )


class UserGroupAdmin(admin.ModelAdmin):
    list_display = ('user', 'group')


class UserPermissionAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'role',
        'permissions',
        'created_by',
    )


class SettingAdmin(admin.ModelAdmin):
    list_display = (
        'variable',
        'id',
        'value'
    )


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'id',
        'ip_address',
        'email',
        'password',
        'mobile',
        'image',
        'balance',
        'activation_selector',
        'activation_code',
        'forgotten_password_selector',
        'forgotten_password_code',
        'forgotten_password_time',
        'remember_selector',
        'remember_code',
        'created_on',
        'last_login',
        'active',
        'company',
        'address',
        'bonus',
        'dob',
        'country_code',
        'city',
        'area',
        'street',
        'pincode',
        'referral_code',
        'friends_code',
        'fcm_id',
        'latitude',
        'longitude',
        'created_at',
    )


class OfferAdmin(admin.ModelAdmin):
    list_display = (
        'type',
        'id',
        'type_id',
        'image',
        'date_added',
    )


# Register models
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductVariant, ProductVariantAdmin)
admin.site.register(Categories, CategoriesAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(Cart, CartAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(ClientApiKey, ClientApiKeyAdmin)
admin.site.register(DeliveryBoyNotification, DeliveryBoyNotificationAdmin)
admin.site.register(Faq, FaqAdmin)
admin.site.register(Favorite, FavoriteAdmin)
admin.site.register(FundTransfer, FundTransferAdmin)
admin.site.register(Group, EshopGroupAdmin)
admin.site.register(Language, LanguageAdmin)
admin.site.register(LoginAttempt, LoginAttemptAdmin)
admin.site.register(Media, MediaAdmin)
admin.site.register(Notification, NofitifacionAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
admin.site.register(ProductRating, ProductRatingAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(Slider, SliderAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(UserGroup, UserGroupAdmin)
admin.site.register(UserPermission, UserPermissionAdmin)
admin.site.register(Setting, SettingAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Offer, OfferAdmin)
