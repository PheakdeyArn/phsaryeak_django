from rest_framework import viewsets
from rest_framework.response import Response
from eshop.models import Offer
from eshop.serializers.offer import OfferSerializer


class OfferViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

    def list(self, request, *args, **kwargs):

        queryset = Offer.objects.all()
        serializer = OfferSerializer(
            queryset,
            many=True,
            context={'request': request})

        data = serializer.data

        return Response({
            'error': False,
            'data': data
        })
