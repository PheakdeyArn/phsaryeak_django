from rest_framework import viewsets
from rest_framework.response import Response
from eshop.models import Transaction
from eshop.serializers import TransactionSerializer
from django.db.models import Q
from datetime import datetime


class TransactionViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

    def list(self, request, *args, **kwargs):
        query_filter = Q()

        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')
        get_type = self.request.query_params.get('type')
        transaction_type = self.request.query_params.get('transaction_type')
        user_id = self.request.query_params.get('user_id')

        if user_id:
            query_filter.add(Q(user=user_id), Q.AND)

        if transaction_type:
            query_filter.add(Q(transaction_type=transaction_type), Q.AND)

        if get_type:
            query_filter.add(Q(type=get_type), Q.AND)

        if start_date or end_date:
            if not end_date:
                end_date = datetime.now()
            query_filter.add(Q(date_created__gte=start_date, date_created__lte=end_date), Q.AND)

        if user_id or transaction_type or start_date or end_date or get_type:
            queryset = Transaction.objects.filter(query_filter)
        else:
            queryset = Transaction.objects.all()

        serializer = TransactionSerializer(
            queryset,
            many=True,
            context={'request': request})

        total = queryset.count()
        data = serializer.data

        return Response({
            'error': False,
            'message': 'Data Retrieved Successfully',
            'total': total,
            'data': data
        })
