from rest_framework import viewsets, status
from rest_framework.response import Response
from eshop.models import Section
from eshop.serializers.section import SectionSerializer


class SectionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    queryset = Section.objects.all()
    serializer_class = SectionSerializer

    def list(self, request, *args, **kwargs):

        queryset = Section.objects.all()
        serializer = SectionSerializer(
            queryset,
            many=True,
            context={
                'request': request,
                'p_limit': request.query_params.get('p_limit', 4),
                'p_offset': request.query_params.get('p_offset', 0),
            })

        data = serializer.data
        total = queryset.count()

        return Response({
            'error': False,
            'message': 'Sections retrieved successfully',
            'total': total,
            'data': data,
        }, status=status.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        """
        Retrieve a Section API
        """
        instance = self.get_object()
        serializer = self.get_serializer(
            instance,
            context={
                'request': request,
                'p_limit': request.query_params.get('p_limit', 10),
                'p_offset': request.query_params.get('p_offset', 0),
                'top_rated_product': request.query_params.get('top_rated_product'),
                'p_sort': request.query_params.get('p_sort'),
                'p_order': request.query_params.get('p_order'),
            }
        )
        return Response({
            'error': False,
            'message': 'Section retrieve successfully',
            "data": [serializer.data]
        })
