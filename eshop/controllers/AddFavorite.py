from rest_framework.views import APIView
from rest_framework.response import Response
from eshop.models import Favorite
from eshop.serializers.favorite import FavoriteSerializer


def get_favorite_product_ids(qs):
    if len(qs) == 0:
        return []
    return [x.get('product_id') for x in qs]


class AddFavoriteView(APIView):

    def post(self, request, *args, **kwargs):
        request_user = self.request.data.get('user', request.user.id)
        request_product = self.request.data.get('product')

        favorites = Favorite.objects.filter(user_id=request_user)
        favorite_products = get_favorite_product_ids(favorites.values())

        if (request_user is None) or (request_product is None):
            return Response({
                'error': True,
                'message': "User or Product can't be null !",
                'data': []
            })

        if request_product in favorite_products:
            return Response({
                'error': True,
                'message': 'Already added to favorite !',
                'data': []
            })

        serializer = FavoriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

        return Response({
            'error': False,
            'message': 'Added to favorite',
            'data': []
        })
