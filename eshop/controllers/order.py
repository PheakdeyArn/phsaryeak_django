from rest_framework import viewsets, status, response
from eshop.models import Order
from eshop.serializers import OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def list(self, request, *args, **kwargs):

        offset = int(self.request.query_params.get('offset', 0))
        limit = int(self.request.query_params.get('limit', 10))
        
        user_id = self.request.query_params.get('user_id')
        active_status = self.request.query_params.get('active_status')

        queryset = Order.objects.filter(user_id=user_id).order_by('-date_added')[offset: offset + limit]
    
        serializer = OrderSerializer(queryset, context={'request': request}, many=True)
        total = queryset.count()
        data = serializer.data
        return response.Response({
            'error': False,
            "message": "Data retrieved successfully",
            'total': total,
            'data': data,
        }, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        """
        Create Order API
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return response.Response({
            'error': False,
            'message': 'Order Added Successfully',
            'data': serializer.data
        })
