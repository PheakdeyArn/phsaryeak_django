from rest_framework import viewsets
from rest_framework.response import Response
from datetime import datetime, timedelta
import json

from eshop.models import Setting, User, Cart
from eshop.serializers import SettingSerializer


class SettingsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Setting.objects.all()
    serializer_class = SettingSerializer

    def list(self, request, *args, **kwargs):
        user_data = {}
        user_id = self.request.query_params.get('user_id', None)
        setting_type = self.request.query_params.get('type', 'all')

        settings = Setting.objects.all()
        serializers = SettingSerializer(
                settings,
                many=True,
                context={'request': request}
            )
        settings_data = serializers.data

        filter_settings = [
            'logo',
            'privacy_policy',
            'terms_conditions',
            'fcm_server_key',
            'contact_us',
            'about_us',
            'currency',
            'time_slot_config',
            'system_settings',
        ]
        final_data = {x.get('variable'): x.get('value') for x in settings_data if
                      x.get('variable') in filter_settings}
        # get user data
        if user_id is not None:
            users = User.objects.filter(id=user_id).values()
            user_cart = Cart.objects.filter(user_id=user_id)
            cart_total_items = user_cart.count()

            filter_user_data = {
                'id',
                'username',
                'email',
                'mobile',
                'balance',
                'dob',
                'referral_code',
                'friends_code',
                'city',
                'area',
                'street',
                'pincode'
            }
            user_data = {key: users[0][key] for key in filter_user_data}
            user_data.update({
                'cart_total_items': cart_total_items,
            })

        # update final data
        final_data.update({
            'time_slot_config': json.loads(final_data.get('time_slot_config')),
            'system_settings': json.loads(final_data.get('system_settings')),
            'user_data': user_data
        })

        if setting_type == 'payment_method':
            filter_settings = [
                'payment_method',
                'time_slot_config'
            ]
            final_data = {x.get('variable'): x.get('value') for x in settings_data if
                          x.get('variable') in filter_settings}

            ts_config_value = json.loads(final_data.get('time_slot_config'))
            ts_delivery_start = int(ts_config_value.get('delivery_starts_from'))

            # delivery start date
            cur_date = datetime.today().strftime('%Y-%m-%d')
            starting_date = (datetime.strptime(cur_date, '%Y-%m-%d')
                             + timedelta(days=ts_delivery_start)).strftime('%Y-%m-%d')

            ts_config_value.update({
                    'delivery_starts_from': ts_delivery_start,
                    'starting_date': starting_date,
                })

            final_data.update({
                "payment_method": json.loads(final_data.get('payment_method')),
                'time_slot_config': ts_config_value,
                'time_slots': []
            })

        return Response({
            'error': False,
            'message': 'Settings retrieved successfully',
            'data': final_data
        })
