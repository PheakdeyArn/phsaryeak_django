from rest_framework import viewsets

from eshop.serializers import MediaSerializer
from eshop.models import Media
from rest_framework.response import Response


class MediaViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides list and retrieve actions.
    """
    queryset = Media.objects.all()
    serializer_class = MediaSerializer

    def list(self, request, *args, **kwargs):

        # get request params
        req_name = request.query_params.get('name')

        if req_name:
            queryset = Media.objects.filter(name__icontains=req_name)
        else:
            queryset = Media.objects.all()

        serializer = MediaSerializer(
            queryset,
            many=True,
            context={'request': request})
        total = queryset.count()

        return Response({
            'error': False,
            'message': 'Data Retrieved Successfully',
            'total': total,
            'data': serializer.data
        })
