from rest_framework import viewsets, status
from eshop.models import Categories
from eshop.serializers import CategoriesSerializer
from rest_framework.response import Response


class CategoriesViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Categories.objects.all()
    serializer_class = CategoriesSerializer

    def list(self, request, *args, **kwargs):
        queryset = Categories.objects.all()

        # declare default request params values
        default_limit = 100
        default_offset = 0

        serializer = CategoriesSerializer(
            queryset,
            many=True,
            context={
                'request': request,
                'limit': request.query_params.get('limit'),
                'offset': request.query_params.get('offset'),
                'has_child_or_item': request.query_params.get('has_child_or_item'),
            })

        # get query params
        limit = int(self.request.query_params.get('limit', default_limit))
        offset = int(self.request.query_params.get('offset', default_offset))
        has_child_or_item = self.request.query_params.get('has_child_or_item')

        # get Total category
        total = queryset.count()
        data = serializer.data[offset: limit + offset]

        # find final data
        if has_child_or_item in ['True', 'true', '1', 1, True]:
            data = [x for x in data if x['children'] == []]

        if has_child_or_item in ['false', 'False', '0', 0, False]:
            data = [x for x in data if x['total'] > 0]

        return Response({
            'message': "Category(s) retrieved successfully!",
            'error': False,
            'total': total,
            'data': data
        }, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        """
        Update Category API:
            Method: PATCH
            Data: Partial of Category
        """
        get_cate = Categories.objects.filter(id=kwargs['pk']).values()

        if request.data.get('name') == get_cate[0].get('name'):
            request.data.pop('name')

        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

