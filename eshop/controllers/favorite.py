from rest_framework import viewsets
from eshop.models import Favorite
from eshop.serializers.favorite import FavoriteSerializer
from rest_framework.response import Response


def get_favorite_product_ids(qs):
    if len(qs) == 0:
        return []
    return [x.get('product_id') for x in qs]


class FavoriteViewSet(viewsets.ModelViewSet):
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer

    def list(self, request, *args, **kwargs):
        login_user = request.user.id
        user_id = self.request.query_params.get('user_id', login_user)
        limit = int(self.request.query_params.get('limit', 10))
        offset = int(self.request.query_params.get('offset', 0))

        if user_id is None:
            """
            Case: No user login
            """
            return Response({
                'error': True,
                'message': 'Please! Login.'
            })
        else:
            """
            Case: User login
            """
            user_favorite = Favorite.objects.filter(user=user_id)
            serializer = FavoriteSerializer(
                user_favorite,
                many=True,
                context={'request': request}
            )
            total = user_favorite.count()
            data = serializer.data[offset: limit + offset]

            return Response({
                'error': False,
                'message': 'Data Retrieved Successfully',
                'total': total,
                'data': data
            })

    def create(self, request, *args, **kwargs):
        """
        Add product to favorite
        """
        error = False
        message = "Added to favorite"

        request_user = request.data.get('user')
        request_product = request.data.get('product')

        favorites = Favorite.objects.filter(user_id=request_user)
        favorite_products = get_favorite_product_ids(favorites.values())

        """
        Case: User doesn't has favorite product 
            - Create new user favorite product
        Case: User has favorite product(s)
            - If request product in already in user favorite 
                - Delete favorite
            - If request product is not in user favorite 
                - Create new favorite
        """
        if request_product in favorite_products:
            favorite = Favorite.objects.filter(user_id=request_user, product_id=request_product)
            favorite.delete()
            message = 'Removed from favorite'
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()

        return Response({
            'error': error,
            'message': message,
            'data': []
        })

    def destroy(self, request, *args, **kwargs):
        """
        Delete Favorite by ID
        """
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({
                'error': False,
                'message': 'Address Deleted Successfully',
                'data': []
            })
