from rest_framework.views import APIView
from rest_framework.response import Response
from eshop.models import Favorite


def get_favorite_product_ids(qs):
    if len(qs) == 0:
        return []
    return [x.get('product_id') for x in qs]


class RemoveFavoriteView(APIView):

    def post(self, request, *args, **kwargs):
        request_user = self.request.data.get('user', request.user.id)
        request_product = self.request.data.get('product')

        favorites = Favorite.objects.filter(user=request_user)
        favorite_products = get_favorite_product_ids(favorites.values())

        if (request_user is None) or (request_product is None):
            return Response({
                'error': True,
                'message': "User or Product can't be null !",
                'data': []
            })

        if request_product not in favorite_products:
            return Response({
                'error': True,
                'message': 'Item not added as favorite !',
                'data': []
            })

        favorite = Favorite.objects.filter(user=request_user, product=request_product)
        favorite.delete()

        return Response({
            'error': False,
            'message': 'Removed from favorite',
            'data': []
        })
