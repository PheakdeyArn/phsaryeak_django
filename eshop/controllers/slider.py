from rest_framework import viewsets, status
from rest_framework.response import Response
from eshop.models import Slider
from eshop.serializers.slider import SliderSerializer


class SliderViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    queryset = Slider.objects.all()
    serializer_class = SliderSerializer

    def list(self, request, *args, **kwargs):

        queryset = Slider.objects.all()
        serializer = SliderSerializer(
            queryset,
            many=True,
            context={'request': request})

        data = serializer.data

        return Response({
            'error': False,
            'data': data
        }, status=status.HTTP_200_OK)
