from rest_framework import viewsets

from eshop.models import ProductVariant
from eshop.serializers.product_variant import ProductVariantSerializer


class ProductVariantViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides list, create, retrieve,
    update and destroy actions.

    Additionally we also provide an extra highlight action.
    """
    queryset = ProductVariant.objects.all()
    serializer_class = ProductVariantSerializer



