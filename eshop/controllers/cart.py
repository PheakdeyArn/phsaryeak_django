from rest_framework import viewsets
from rest_framework.response import Response
from eshop.models import Cart
from eshop.serializers import CartSerializer
from awesome.utils.cart_helpers import *


class CartViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    def list(self, request, *args, **kwargs):
        # Get query params
        user_id = self.request.query_params.get('user_id', request.user.id)
        is_save_later = self.request.query_params.get('save_for_later', '0')

        # Query Cart
        bool_is_save_later = get_bool_is_save_later(is_save_later)
        queryset = Cart.objects.filter(user_id=user_id, is_saved_for_later=bool_is_save_later)

        # Get list of special price in cart
        variant_ids = get_values(queryset.values(), 'product_variant_id')

        serializer = CartSerializer(
            queryset,
            many=True,
            context={
                'request': request,
            }
        )

        data = serializer.data
        total = get_total_quantity(queryset.values())
        sub_total = get_cart_total_price(queryset.values())

        delivery_charge_amount = int(get_system_setting_value('delivery_charge'))
        min_amount_for_free_delivery = int(get_system_setting_value('min_amount'))
        delivery_charge = get_delivery_charge(sub_total, delivery_charge_amount, min_amount_for_free_delivery)

        tax_percentage = get_tax_percentage()
        tax_amount = get_tax_amount(sub_total, tax_percentage)
        total_arr = (sub_total + delivery_charge + tax_amount)

        return Response({
            'error': False if user_id else True,
            'message': 'Cart(s) Retrieved Successfully' if user_id else 'No Details Found !',
            'total_quantity': total,
            'sub_total': sub_total,
            'delivery_charge': delivery_charge,
            'tax_percentage': tax_percentage,
            'tax_amount': tax_amount,
            'overall_amount': str(total_arr),
            'total_arr': total_arr,
            'variants_id': variant_ids,
            'data': data,
        })

    def create(self, request, *args, **kwargs):
        """
        This function perform both Create, Update User Cart, Remove Cart
            + Create Cart request body:
                - user
                - product_variant
                - qty

            + Update Cart request body:
                - user
                - product_variant
                - qty
                - is_saved_for_later

            + Remove Items from cart body:
                - qty = 0
        """
        error = False
        message = "Cart Updated !"
        # get request data
        user = request.data.get('user')
        request_variant = request.data.get('product_variant')
        qty = request.data.get('qty', 1)
        is_saved_for_later = request.data.get('is_saved_for_later', False)

        if user is None:
            """
            Case: No Request User 
            """
            return Response({
                'error': True,
                'message': 'Please login !',
                'data': {}
            })

        # get user data before request
        user_carts = Cart.objects.filter(user_id=user)
        cart_items = [x.get('product_variant_id') for x in user_carts.values() if x.get('is_saved_for_later') is False]
        saved_for_later_items = [x.get('product_variant_id') for x in user_carts.values() if
                                 x.get('is_saved_for_later') is True]

        old_total_cart_qty = sum([x.get('qty') for x in user_carts.values() if x.get('is_saved_for_later') is False])

        # Get setting Maximum Cart Available
        max_items_cart = int(get_system_setting_value('max_items_cart'))

        if request_variant in cart_items:
            """
            Case: Request Item is already in User Cart
                _ Update User Cart
            """
            cart = Cart.objects.filter(user_id=user, product_variant=request_variant)
            cart_qty = get_qs_value(cart.values(), 'qty')
            new_total_cart_qty = (old_total_cart_qty - cart_qty) + qty

            if qty <= 0:
                """ Delete Item from cart """
                cart.delete()

            # out of stock validation
            products = Product.objects.filter(id=request_variant)
            stock = get_qs_value(products.values(), 'stock')
            out_of_stock = is_out_of_stock(stock, qty)
            if out_of_stock:
                """ Item is out of stock """
                error = True
                message = "One of the product is out of stock."

            # Maximum Cart Validation
            maximum = is_maximum_cart(new_total_cart_qty, max_items_cart)
            if maximum and is_saved_for_later is False:
                """
                Case: User cart reach to maximum 
                """
                error = True
                message = f'Maximum {max_items_cart} Item(s) Can Be Added Only!'

            """ Check is variant available"""
            available = is_variant_available(out_of_stock, maximum)
            if available:
                cart.update(qty=qty, is_saved_for_later=is_saved_for_later)

        elif request_variant in saved_for_later_items:
            """
            Case: Request item is already in save for later
                _  Update User Cart
            """
            cart = Cart.objects.filter(user_id=user, product_variant=request_variant)
            cart_qty = get_qs_value(cart.values(), 'qty')
            new_total_cart_qty = old_total_cart_qty + cart_qty

            if qty <= 0:
                """ Delete Item from ( saved for later ) """
                cart.delete()

            maximum = is_maximum_cart(new_total_cart_qty, max_items_cart)
            if maximum:
                error = True
                message = f'Maximum {max_items_cart} Item(s) Can Be Added Only!'
            else:
                cart.update(qty=cart_qty, is_saved_for_later=is_saved_for_later)

        else:
            """
            Case: Request Item is not in User Cart
                _ Create a new Cart
            """
            products = Product.objects.filter(id=request_variant)
            stock = get_qs_value(products.values(), 'stock')

            # out of stock validation
            out_of_stock = is_out_of_stock(stock, qty)
            if out_of_stock:
                error = True
                message = "One of the product is out of stock."

            # Maximum Cart validation
            new_total_cart_qty = old_total_cart_qty + qty
            maximum = is_maximum_cart(new_total_cart_qty, max_items_cart)
            if maximum:
                """
                Case: User cart reach to maximum 
                """
                error = True
                message = f'Maximum {max_items_cart} Item(s) Can Be Added Only!'

            available = is_variant_available(out_of_stock, maximum)
            if available and qty > 0:
                serializer = self.get_serializer(data=request.data)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()

        # update user cart after request
        updated_carts = Cart.objects.filter(user_id=user, is_saved_for_later=False)
        total_cart = updated_carts.count()
        sub_total = get_cart_total_price(updated_carts.values())

        total_items = get_total_quantity(updated_carts.values())
        tax_percent = get_tax_percentage()
        tax_amount = get_tax_amount(sub_total, tax_percent)

        delivery_charge_amount = int(get_system_setting_value('delivery_charge'))
        min_amount_for_free_delivery = int(get_system_setting_value('min_amount'))
        delivery_charge = get_delivery_charge(sub_total, delivery_charge_amount, min_amount_for_free_delivery)
        overall_amount = (sub_total + delivery_charge + tax_amount)

        return Response({
            'error': error,
            'message': message,
            'data': {
                'total_quantity': qty,
                'sub_total': sub_total,
                'total_items': total_items,
                'tax_percentage': tax_percent,
                'tax_amount': tax_amount,
                'cart_count': total_cart,
                'max_items_cart': int(max_items_cart),
                'overall_amount': overall_amount
            }
        })
