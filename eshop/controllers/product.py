from django.db.models.query_utils import Q
from rest_framework import viewsets, status, response
from eshop.models import Product
from eshop.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides list, create, retrieve,
    update and destroy actions.

    Additionally we also provide an extra highlight action.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def list(self, request, *args, **kwargs):

        offset = int(self.request.query_params.get('offset', 0))
        limit = int(self.request.query_params.get('limit', 10))

        cat_id = request.query_params.get('category_id')
        param_product_id = request.query_params.get('id')
        search = request.query_params.get('search')
        top_rated = request.query_params.get('top_rated_product')
        get_all_products = request.query_params.get('get_all_products', False)

        if top_rated in [True, 1, '1', 'true', 'True']:
            sort, order = "p.rating", "DESC"
        else:
            sort = request.query_params.get('sort', "p.id")
            order = request.query_params.get('order', "DESC")

        model, field = sort.split(".")
        sort_combination = '{}{}'.format("" if order == "ASC" else "-",
                                         field if model == "p"
                                         else "{}__{}".format(model, field))

        if search:
            queryset = Product.objects.filter(name__icontains=search).order_by(sort_combination)[offset:limit + offset]
        elif cat_id:
            if param_product_id:
                # query products except viewing product within the same category
                queryset = Product.objects.filter((~Q(id=param_product_id)), category_id=cat_id).order_by(
                    sort_combination)[offset: limit + offset]
            else:
                queryset = Product.objects.filter(category_id=cat_id).order_by(sort_combination)[offset:limit + offset]
        else:
            if get_all_products in [True, 1, '1', 'true', 'True']:
                queryset = Product.objects.all().order_by(sort_combination)
            else:
                queryset = Product.objects.all().order_by(sort_combination)[offset:limit + offset]

        serializer = ProductSerializer(queryset, context={'request': request}, many=True)
        total = queryset.count()
        data = serializer.data
        return response.Response({
            'error': False,
            "message": "Products retrieved successfully !",
            'filters': [],
            'total': total,
            'offset': offset,
            'data': data,
        }, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return response.Response({
            "error": False,
            "message": "Delete Successfully!",
        })
