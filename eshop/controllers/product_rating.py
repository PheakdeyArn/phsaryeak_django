from eshop.models import Product, ProductRating
from rest_framework import viewsets, status, response
from eshop.serializers.product_rating import ProductRatingSerializer

class ProductRatingViewSet(viewsets.ReadOnlyModelViewSet):
    
    queryset = ProductRating.objects.all()
    serializer_class = ProductRatingSerializer

    def list(self, request, *args, **kwargs):
        product_id = request.query_params.get('product_id')
        
        product_rating = ProductRating.objects.filter(pk__gte = ProductRating.objects.count() - 10).values()
        rating_id = [x.get('id') for x in product_rating]
        queryset = ProductRating.objects.filter(product_id = product_id , id__in = rating_id).order_by('-id')
    
        serializer = ProductRatingSerializer(queryset, context={'request': request,}, many=True)

        ratings = ProductRating.objects.filter(product_id = product_id).values('id').count()
        
        product = Product.objects.filter(id = product_id).values()
        no_of_ratings = [x.get('no_of_ratings')if x else 0 for x in product]
        
        data = serializer.data
        
        return response.Response({
            'error': False,
            "message": "Rating retrieved successfully",
            'no_of_rating':no_of_ratings,
            'total': ratings,
            'data': data,
        }, status=status.HTTP_200_OK)

            

