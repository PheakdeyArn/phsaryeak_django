from rest_framework import viewsets
from rest_framework.response import Response
from eshop.models import User, UserGroup
from eshop.serializers import UserSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):

        user_group = UserGroup.objects.all().values()
        customer_id = [x.get('user_id') for x in user_group if x.get('group_id') == 2]

        req_username = request.query_params.get('username')

        if req_username:
            queryset = User.objects.filter(username__icontains=req_username)
        else:
            queryset = User.objects.filter(id__in=customer_id)

        serializer = UserSerializer(
            queryset,
            many=True,
            context={'request': request})

        total = queryset.count()
        data = serializer.data

        return Response({
            'error': False,
            'message': 'Data Retrieved Successfully',
            'total': total,
            'data': data
        })
