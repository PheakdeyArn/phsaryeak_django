from eshop.serializers.order_item import OrderItemSerializer
from rest_framework import viewsets, status, response
from eshop.models import OrderItem


class OrderItemViewSet(viewsets.ModelViewSet):
    
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer

    def list(self, request, *args, **kwargs):

        user_id = request.query_params.get('user_id')
        queryset = OrderItem.objects.filter(user_id=user_id)
        
        serializer = OrderItemSerializer(queryset, context={'request': request,}, many=True)
       
        data = serializer.data
        return response.Response({
            'data': data,
        }, status=status.HTTP_200_OK)


    def create(self, request, *args, **kwargs):
        """
        Create Order Item API
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return response.Response({
            'error': False,
            'message': 'Order Item Added Successfully',
            'data': serializer.data
        })
