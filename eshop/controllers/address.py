from rest_framework import viewsets
from rest_framework.response import Response
from eshop.models import Address
from eshop.serializers import AddressSerializer
from django.db.models import Q


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer

    def list(self, request, *args, **kwargs):
        """
        Get User Address API
        """
        query_filter = Q()

        user_id = self.request.query_params.get('user_id')
        req_name = self.request.query_params.get('name')
        limit = int(self.request.query_params.get('limit', 10))
        offset = int(self.request.query_params.get('offset', 0))

        get_type = self.request.query_params.get('type')
        get_state = self.request.query_params.get('state')

        if user_id:
            query_filter.add(Q(user=user_id), Q.AND)

        if get_type:
            query_filter.add(Q(type=get_type), Q.AND)

        if req_name:
            query_filter.add(Q(name__icontains=req_name), Q.AND)

        if get_state:
            query_filter.add(Q(state__icontains=get_state), Q.AND)

        if user_id or req_name or get_type or get_state:
            queryset = Address.objects.filter(query_filter)
        else:
            queryset = Address.objects.all()

        serializer = AddressSerializer(
            queryset,
            many=True,
            context={
                'request': request,
            }
        )

        total = queryset.count()
        data = serializer.data

        return Response({
            'error': False,
            'message': 'Address Retrieved Successfully',
            'total': total,
            'data': data
        })

    def create(self, request, *args, **kwargs):
        """
        Create Address API
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response({
            'error': False,
            'message': 'Address Added Successfully',
            'data': serializer.data
        })

    def update(self, request, *args, **kwargs):
        """
        Update Address API
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response({
            'error': False,
            'message': 'Address updated Successfully',
            'data': serializer.data
        })

    def destroy(self, request, *args, **kwargs):
        """
        Delete Address API
        """
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({
            'error': False,
            'message': 'Address Deleted Successfully',
            'data': []
        })

    def retrieve(self, request, *args, **kwargs):
        """
        Retrieve An Address API
        """
        instance = self.get_object()
        serializer = self.get_serializer(
            instance,
            context={
                'request': request,
            }
        )
        return Response({
            'error': False,
            'message': 'Address retrieve successfully',
            "data": serializer.data
        })
