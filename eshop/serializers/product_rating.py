from rest_framework import serializers
from eshop.models import ProductRating
from django.contrib.auth.models import User

class ProductRatingSerializer(serializers.ModelSerializer):

    images = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()

    def get_user_name(self, obj):
        rating = ProductRating.objects.filter(id = obj.id, product_id = obj.product_id).values()
        user_id = [x.get('user_id') for x in rating]
        user = User.objects.filter(id__in = user_id).values()
        user_name = [x.get('username') for x in user]
        return (''.join((data for data in user_name)))

    def get_images(self, obj):
        image = ProductRating.objects.filter(id = obj.id).values('images')
        return [x.get('images') for x in image if x.get('images')]
    
    class Meta:
        model = ProductRating
        fields = [
           'id',
           'user_id',
           'product_id',
           'rating',
           'images',
           'comment',
           'data_added',
           'user_name'
        ]

        