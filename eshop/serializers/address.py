from rest_framework import serializers
from eshop.models import Address, Area, City


def get_value(model, model_id, key):
    if model_id in [0, None]:
        return None

    value = model.objects.filter(id=model_id).values()
    return value[0].get(key)


class AddressSerializer(serializers.ModelSerializer):
    area_name = serializers.SerializerMethodField()
    minimum_free_delivery_order_amount = serializers.SerializerMethodField()
    delivery_charges = serializers.SerializerMethodField()
    city_name = serializers.SerializerMethodField()

    @staticmethod
    def get_area_name(obj):
        return get_value(Area, obj.area_id, 'name')

    @staticmethod
    def get_minimum_free_delivery_order_amount(obj):
        return get_value(Area, obj.area_id, 'minimum_free_delivery_order_amount')

    @staticmethod
    def get_delivery_charges(obj):
        return get_value(Area, obj.area_id, 'delivery_charges')

    @staticmethod
    def get_city_name(obj):
        return get_value(City, obj.city_id, 'name')

    class Meta:
        model = Address
        fields = [
            'id',
            'user',
            'name',
            'type',
            'mobile',
            'alternate_mobile',
            'address',
            'landmark',
            'area',
            'city',
            'pincode',
            'country_code',
            'state',
            'country',
            'latitude',
            'longitude',
            'is_default',
            'area_name',
            'minimum_free_delivery_order_amount',
            'delivery_charges',
            'city_name',
        ]
