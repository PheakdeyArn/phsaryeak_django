from rest_framework import serializers
from eshop.models import Favorite, Product
from eshop.serializers.product import ProductSerializer


class FavoriteSerializer(serializers.ModelSerializer):
    product_details = serializers.SerializerMethodField()

    def get_product_details(self, obj):
        request = self.context.get('request')
        product_id = obj.product.id

        product = Product.objects.filter(id=product_id)
        serializer = ProductSerializer(
            product,
            many=True,
            context={'request': request}
        )
        data = serializer.data
        return data

    class Meta:
        model = Favorite
        fields = [
            'id',
            'user',
            'product',
            'product_details'
        ]
