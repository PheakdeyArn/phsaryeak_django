from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from eshop.business_logic.unique_together_fk import validate_category_name_parent
from eshop.models import Categories, Product


class CategoriesSerializer(serializers.ModelSerializer):
    text = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()
    icon = serializers.SerializerMethodField()
    level = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()

    def validate(self, attrs):
        print("attrs:", attrs)
        validate_category_name_parent(ValidationError, self.Meta.model.objects, attrs.get('name'), attrs.get('parent'))
        return attrs

    @staticmethod
    def get_text(obj):
        category_name = obj.name
        return category_name

    @staticmethod
    def get_total(obj):
        data = Product.objects.filter(category_id=obj.id)
        return data.count()

    @staticmethod
    def get_icon(obj):
        return 'jstree-folder'

    @staticmethod
    def get_level(obj):
        return 0

    @staticmethod
    def get_children(obj):
        children = Categories.objects.filter(parent_id=obj.id).values()
        return [x.get('id') for x in children]

    @staticmethod
    def get_state(obj):
        status = obj.status
        state = {
            'opened': bool(status == 1)
        }
        return state

    class Meta:
        model = Categories
        fields = [
            'id',
            'name',
            'slug',
            'banner',
            'row_order',
            'status',
            'parent',
            'image',
            'children',
            'text',
            'state',
            'icon',
            'level',
            'total',
        ]

        extra_kwargs = {'parent': {'required': False}}
