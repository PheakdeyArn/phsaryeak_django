from rest_framework import serializers
from eshop.models import Setting


class SettingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Setting
        fields = [
            'id',
            'variable',
            'value'
        ]

