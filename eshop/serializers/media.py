from rest_framework import serializers
from eshop.models import Media


class MediaSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        host = 'http://34.126.158.141:8800/'
        return host + obj.sub_directory + obj.name

    class Meta:
        model = Media
        fields = [
            'id',
            'title',
            'name',
            'image',
            'extension',
            'type',
            'sub_directory',
            'size',
            'date_created',
        ]

