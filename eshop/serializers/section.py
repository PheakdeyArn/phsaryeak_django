from rest_framework import serializers
from eshop.serializers import ProductSerializer
from eshop.models import Section, Product


class SectionSerializer(serializers.ModelSerializer):

    tota_sections = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    filters = serializers.SerializerMethodField()
    product_details = serializers.SerializerMethodField()

    @staticmethod
    def get_tota_sections(obj):
        """
        find total section
        """
        sections = Section.objects.all()
        count_section = sections.count()
        return count_section

    @staticmethod
    def get_total(obj):
        """
        Find total product in each section
            1. find total products each category in the section
            2. sum total products from 1
        """
        categories = obj.categories

        if categories is not None:
            list_cate = categories.split(',')
            total_product_list = [Product.objects.filter(category_id=x).count() for x in list_cate]
            return sum(total_product_list)

        return 0

    @staticmethod
    def get_filters(obj):
        """
        Find section filter if there is
        """
        sections = Section.objects.all().values()
        filters = [x['filters'] for x in sections if 'filters' in x]
        return filters

    def get_product_details(self, obj):
        """
        Find product detail in each section
            - Get request and request params
            - get section category
            - get products that have category id in section category
            - serialize products using ProductSerializer that we have
        """
        # get request
        request = self.context.get('request')
        # get product limit
        product_limit = int(self.context.get('p_limit'))
        # get product offset
        product_offset = int(self.context.get('p_offset'))
        # find Section category
        category_ids = obj.categories

        top_rated_product = self.context.get('top_rated_product')
        p_sort = self.context.get('p_sort')
        p_order = self.context.get('p_order')

        if category_ids is not None:
            category_list = category_ids.split(',')

            def get_product_by_order(sort):
                return Product.objects.filter(category_id__in=category_list).order_by(sort)[
                       product_offset: product_limit + product_offset]

            if top_rated_product == '1':
                products = get_product_by_order('-rating')

            elif p_sort == "p.date_added" and p_order == 'DESC':
                products = get_product_by_order('-date_added')

            elif p_sort == "p.date_added" and p_order == 'ASC':
                products = get_product_by_order('date_added')

            elif p_sort == "pv.price" and p_order == 'DESC':
                products = get_product_by_order('-pv__special_price')

            elif p_sort == "pv.price" and p_order == 'ASC':
                products = get_product_by_order('pv__special_price')

            else:
                products = Product.objects.filter(category_id__in=category_list)[
                           product_offset: product_limit + product_offset]

            products_serializer = ProductSerializer(
                products,
                many=True,
                context={'request': request})
            return products_serializer.data

        return []

    class Meta:
        model = Section
        fields = [
            'id',
            'title',
            'short_description',
            'style',
            'product_ids',
            'row_order',
            'categories',
            'product_type',
            'date_added',
            'tota_sections',
            'total',
            'filters',
            'product_details',
        ]
