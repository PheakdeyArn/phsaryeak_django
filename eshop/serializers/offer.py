from rest_framework import serializers
from eshop.models import Offer, Categories
from eshop.serializers import CategoriesSerializer


class OfferSerializer(serializers.ModelSerializer):
    data = serializers.SerializerMethodField()

    def get_data(self, obj):
        request = self.context.get("request")
        data = []

        if obj.type == "categories":
            category = Categories.objects.filter(id= obj.type_id)
            category_serializer = CategoriesSerializer(
                category,
                many=True,
                context={'request': request}
            )
            category_data = category_serializer.data
            data = [x for x in category_data if not x['children']]

        return data

    class Meta:
        model = Offer
        fields = [
            'id',
            'type',
            'type_id',
            'image',
            'date_added',
            'data'
        ]
