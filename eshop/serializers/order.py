from eshop.serializers.order_item import OrderItemSerializer
from rest_framework import serializers
from awesome.utils import helper
from eshop.models import Order, OrderItem, Product
from django.contrib.auth.models import User


class OrderSerializer(serializers.ModelSerializer):
    
    username = serializers.SerializerMethodField()
    is_returnable = serializers.SerializerMethodField()
    is_cancelable = serializers.SerializerMethodField()
    is_already_returned = serializers.SerializerMethodField()
    is_already_cancelled = serializers.SerializerMethodField()
    return_request_submitted = serializers.SerializerMethodField()
    total_tax_percent = serializers.SerializerMethodField()
    total_tax_amount = serializers.SerializerMethodField()
    invoice_html = serializers.SerializerMethodField()
    order_item = serializers.SerializerMethodField()

    def get_username(self, obj):
        user = User.objects.filter(id=obj.user_id).values()
        usernames = [x for x in user]
        return helper.get_is_return_null(usernames, user, 'username')

    def get_is_returnable(self, obj):
        order_item = OrderItem.objects.filter(order_id = obj.id).values()
        product_variant_ids = [x.get('product_variant_id') for x in order_item]
        product = Product.objects.filter(id__in = product_variant_ids).values()
        product_list = [x for x in product]
        return helper.get_is_return_null(product_list, product, 'is_returnable')

    def get_is_cancelable(self, obj):
        order_item = OrderItem.objects.filter(order_id = obj.id).values()
        product_variant_ids = [x.get('product_variant_id') for x in order_item]
        product = Product.objects.filter(id__in = product_variant_ids).values()
        product_list = [x for x in product]
        return helper.get_is_return_null(product_list, product, 'is_cancelable')
        
    def get_is_already_returned(self, obj):
        order_item = OrderItem.objects.filter(order_id = obj.id).values()
        order_item_list= [x for x in order_item]
        active_status = helper.get_is_return_null(order_item_list, order_item, 'active_status')
        is_already_returned = 1 if active_status == 'returned' else 0
        return is_already_returned

    def get_is_already_cancelled(self, obj):
        order_item = OrderItem.objects.filter(order_id = obj.id).values()
        order_item_list= [x for x in order_item]
        active_status = helper.get_is_return_null(order_item_list, order_item, 'active_status')
        is_already_cancelled = 1 if active_status == 'cancelled' else 0
        return is_already_cancelled

    def get_return_request_submitted(self, obj):
        return None

    def get_total_tax_percent(self, obj):
        order_item = OrderItem.objects.filter(order_id = obj.id).values()
        return sum([x.get('tax_percent') for x in order_item])

    def get_total_tax_amount(self, obj):
        order_item = OrderItem.objects.filter(order_id = obj.id).values()
        return sum([x.get('tax_amount') for x in order_item])

    def get_invoice_html(self, obj):
        return None

    def get_order_item(self, obj):
        request = self.context.get("request")
        order_item = OrderItem.objects.filter(order_id=obj.id) 
        order_item_serializer = OrderItemSerializer(order_item, many=True, context={'request': request},) 
        return order_item_serializer.data

    class Meta:
        model = Order
        fields = [
            'id',
            'user',
            'delivery_boy_id',
            'mobile',
            'total',
            'delivery_charge',
            'wallet_balance',
            'total_payable',
            'promo_code',
            'promo_discount',
            'discount',
            'final_total',
            'payment_method',
            'latitude',
            'longitude',
            'address',
            'delivery_time',
            'delivery_date',
            'status',
            'active_status',
            'date_added',
            'otp',
            'username',
            'is_returnable',
            'is_cancelable',
            'is_already_returned',
            'is_already_cancelled',
            'return_request_submitted',
            'total_tax_percent',
            'total_tax_amount',
            'invoice_html',        
            'order_item'
        ]
