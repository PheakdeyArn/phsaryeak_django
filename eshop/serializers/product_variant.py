from rest_framework import serializers
from awesome.utils import helper
from eshop.models import ProductVariant, Cart

class ProductVariantSerializer(serializers.ModelSerializer):

    availability = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    images_md = serializers.SerializerMethodField()
    images_sm = serializers.SerializerMethodField()
    cart_count = serializers.SerializerMethodField()
    variant_ids = serializers.SerializerMethodField()
    variant_value = serializers.SerializerMethodField()
    attr_name = serializers.SerializerMethodField()

    def get_availability(self, obj):
        availability = obj.availability
        if availability is None:
            availability = 0
        return availability

    def get_images(self, obj):
        productvariant = ProductVariant.objects.filter(id=obj.id).values()
        return [x.get('images') for x in productvariant]

    def get_images_md(self, obj):
        product_variants = ProductVariant.objects.filter(id=obj.id).values()
        images = [x.get('images') for x in product_variants]
        return [helper.get_md_sm_image(product_variants)[0] for product_variants in images if product_variants]

    def get_images_sm(self, obj):
        product_variants = ProductVariant.objects.filter(id=obj.id).values()
        images = [x.get('images') for x in product_variants]
        return [helper.get_md_sm_image(product_variants)[1] for product_variants in images if product_variants]

    def get_cart_count(self, obj):
        request = self.context.get("request")
        cart = Cart.objects.filter(product_variant_id = obj.id, user_id = request.user.id, is_saved_for_later = 0).values('product_variant_id')
        return cart.count()
        
    def get_variant_ids(self, obj):
        return obj.attribute_value_ids

    def get_variant_value(self, obj):
        variant_value = None
        return variant_value

    def get_attr_name(self, obj):
        attr_name = None
        return attr_name

    class Meta:
        model = ProductVariant
        fields = [
            'id',
            'attribute_value_ids',
            'attribute_set',
            'price',
            'special_price',
            'sku',
            'images',
            'availability',
            'status',
            'date_added',
            'product',
            'stock',
            'variant_ids',
            'attr_name',
            'variant_value',
            'images_md',
            'images_sm',
            'cart_count',
        ]
