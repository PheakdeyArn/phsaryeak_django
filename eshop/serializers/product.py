from eshop.serializers.product_variant import ProductVariantSerializer
from rest_framework import serializers
from awesome.utils import helper
from eshop.models import Product, order_item, Favorite, ProductVariant, OrderItem


class ProductSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    attr_value_ids = serializers.SerializerMethodField()
    attributes = serializers.SerializerMethodField()
    variant_attributes = serializers.SerializerMethodField()
    sales = serializers.SerializerMethodField()
    other_images_md = serializers.SerializerMethodField()
    other_images_sm = serializers.SerializerMethodField()
    image_md = serializers.SerializerMethodField()
    image_sm = serializers.SerializerMethodField()
    variants = serializers.SerializerMethodField()
    is_favorite = serializers.SerializerMethodField()
    is_purchased = serializers.SerializerMethodField()
    min_max_price = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    category_name = serializers.SerializerMethodField()
    special_price = serializers.SerializerMethodField()

    def get_price(self, obj):
        product_variants = ProductVariant.objects.filter(product_id=obj.id).values()
        return helper.get_value(product_variants, 'price')

    def get_special_price(self, obj):
        product_variants = ProductVariant.objects.filter(product_id=obj.id).values()
        return helper.get_value(product_variants, 'special_price')

    def get_attr_value_ids(self, obj):
        product_variants = ProductVariant.objects.filter(product_id=obj.id).values()
        return [x.get('attribute_value_ids') for x in product_variants]

    def get_attributes(self, obj):
        return []

    def get_variant_attributes(self, obj):
        return []

    def get_sales(self, obj):
        request = self.context.get("request")
        order_items = OrderItem.objects.filter(product_variant_id=obj.id, user_id=request.user.id).values()
        return sum([x.get('quantity') for x in order_items])

    def get_other_images_md(self, obj):
        replace = obj.other_images.replace("[]", '')
        image_md = []
        if replace:
            imagemd, imagesm = helper.get_md_sm_image(obj.other_images)
            image_md.append(imagemd)

        return image_md

    def get_other_images_sm(self, obj):
        replace = obj.other_images.replace("[]", '')
        image_sm = []
        if replace:
            imagemd, imagesm = helper.get_md_sm_image(obj.other_images)
            image_sm.append(imagesm)

        return image_sm

    def get_image_md(self, obj):
        image_md, image_sm = helper.get_md_sm_image(obj.image)
        return image_md

    def get_image_sm(self, obj):
        image_md, image_sm = helper.get_md_sm_image(obj.image)
        return image_sm

    def get_variants(self, obj):
        request = self.context.get("request")
        products = ProductVariant.objects.filter(product_id=obj.id)  # QuerySet Field in Product_Variant
        products_variant_serializer = ProductVariantSerializer(products, many=True, context={
            'request': request}, )  # Model of Product_Variant
        return products_variant_serializer.data

    def get_is_favorite(self, obj):
        request = self.context.get("request")
        fav = Favorite.objects.filter(user_id=request.user.id).values()
        list_fav = [x.get('product_id') for x in fav]
        return helper.get_favorite(list_fav, obj.id)

    def get_is_purchased(self, obj):
        request = self.context.get("request")
        orderitem = order_item.OrderItem.objects.filter(user_id=request.user.id).values()
        list_order_item = [x.get('product_variant_id') for x in orderitem]
        return helper.get_is_purchase(list_order_item, obj.id)

    def get_min_max_price(self, obj):
        product_variants = ProductVariant.objects.filter(id=obj.id).values()
        price = [x.get('price') for x in product_variants]
        special_price = [x.get('special_price') for x in product_variants]
        min_price, max_price, min_special_price, max_special_price, percentage = helper.get_min_max_price(price,
                                                                                                          special_price)
        min_max_price = {
            "min_price": min_price,
            "max_price": max_price,
            "special_price": min_special_price,
            "max_special_price": max_special_price,
            "discount_in_percentage": percentage,
        }
        return min_max_price

    def get_total(self, obj):
        return Product.objects.filter(category_id=obj.category_id).count()

    def get_category_name(self, obj):
        return obj.category.name

    class Meta:
        model = Product
        fields = [
            'id',
            'category',
            'category_name',
            'row_order',
            'stock_type',
            'tax',
            'type',
            'name',
            'short_description',
            'slug',
            'indicator',
            'cod_allowed',
            'minimum_order_quantity',
            'quantity_step_size',
            'total_allowed_quantity',
            'is_returnable',
            'is_cancelable',
            'cancelable_till',
            'image',
            'other_images',
            'video_type',
            'video',
            'tags',
            'warranty_period',
            'guarantee_period',
            'made_in',
            'sku',
            'stock',
            'availability',
            'rating',
            'no_of_ratings',
            'description',
            'status',
            'total',
            'sales',
            'attr_value_ids',
            'attributes',
            'variants',
            'is_purchased',
            'is_favorite',
            'image_md',
            'image_sm',
            'other_images_sm',
            'other_images_md',
            'variant_attributes',
            'price',
            'special_price',
            'min_max_price',
        ]
