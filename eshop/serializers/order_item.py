from rest_framework import serializers
from eshop.models import OrderItem, Product
from awesome.utils import helper

class OrderItemSerializer(serializers.ModelSerializer):
    
    return_request_submitted = serializers.SerializerMethodField()
    is_already_cancelled = serializers.SerializerMethodField()
    is_already_returned = serializers.SerializerMethodField()
    image_md =  serializers.SerializerMethodField()
    image_sm = serializers.SerializerMethodField()
    attr_name = serializers.SerializerMethodField()
    variant_values = serializers.SerializerMethodField()
    variant_ids = serializers.SerializerMethodField()
    order_return_counter = serializers.SerializerMethodField()
    order_cancel_counter = serializers.SerializerMethodField()
    order_counter = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    is_returnable = serializers.SerializerMethodField()
    is_cancelable = serializers.SerializerMethodField()
    product_id = serializers.SerializerMethodField()

    def get_return_request_submitted(self, obj):
        return None

    def get_is_already_cancelled(self, obj):
        active_status = obj.active_status
        is_already_cancelled = 1 if active_status == 'cancelled' else 0
        return is_already_cancelled

    def get_is_already_returned(self, obj):
        active_status = obj.active_status
        is_already_return = 1 if active_status == 'returned' else 0
        return is_already_return

    def get_image_md(self, obj):
        product = Product.objects.filter(id = obj.product_variant_id).values()
        product_list = [x.get('image') for x in product]
        image = helper.get_is_return_null(product_list, product, 'image')
        image_md, image_sm = helper.get_md_sm_image(image)
        return image_md

    def get_image_sm(self, obj):
        product = Product.objects.filter(id = obj.product_variant_id).values()
        product_list = [x.get('image') for x in product]
        image = helper.get_is_return_null(product_list, product, 'image')
        image_md, image_sm = helper.get_md_sm_image(image)
        return image_sm

    def get_attr_name(self, obj):
        #don't have model
        return None

    def get_variant_values(self, obj):
        #don't have model
        return None

    def get_variant_ids(self, obj):
        #don't have model
        return None

    def get_order_return_counter(self, obj):
        return OrderItem.objects.filter(active_status = 'returned', order_id = obj.order_id).values('active_status').count()

    def get_order_cancel_counter(self, obj):
        return OrderItem.objects.filter(active_status = 'cancelled', order_id = obj.order_id).values('active_status').count()

    def get_order_counter(self, obj):
        return OrderItem.objects.filter(order_id = obj.order_id).values('id').count()

    def get_type(self, obj):
        product = Product.objects.filter(id = obj.product_variant_id).values()
        product_list = [x for x in product]
        return helper.get_is_return_null(product_list, product, 'type')

    def get_name(self, obj):
        return obj.product_name

    def get_image(self, obj):
        product = Product.objects.filter(id = obj.product_variant_id).values()
        product_list = [x.get('image') for x in product]
        host = 'http://34.126.158.141:8800/'
        get_image = helper.get_is_return_null(product_list, product, 'image')
        if get_image is None:
            get_image = None
        else:
            get_image = host + str(get_image)
        return get_image

    def get_is_returnable(self, obj):
        product = Product.objects.filter(id = obj.product_variant_id).values()
        product_list = [x for x in product]
        return helper.get_is_return_null(product_list, product, 'is_returnable')

    def get_is_cancelable(self, obj):
        product = Product.objects.filter(id = obj.product_variant_id).values()
        product_list = [x for x in product]
        return helper.get_is_return_null(product_list, product, 'is_cancelable')

    def get_product_id(self, obj):
        product = Product.objects.filter(id = obj.product_variant_id).values()
        product_list = [x for x in product]
        return helper.get_is_return_null(product_list, product, 'id')
        
    class Meta:
        model = OrderItem
        fields = [
            'id',
            'user',
            'order',
            'product_name',
            'variant_name',
            'product_variant',
            'quantity',
            'price',
            'discounted_price',
            'tax_percent',
            'tax_amount',
            'discount',
            'sub_total',
            'deliver_by',
            'status',
            'active_status',
            'date_added',
            'product_id',
            'is_cancelable',
            'is_returnable',
            'image',
            'name',
            'type',
            'order_counter',
            'order_cancel_counter',
            'order_return_counter',
            'variant_ids',
            'variant_values',
            'attr_name',
            'image_sm',
            'image_md',
            'is_already_returned',
            'is_already_cancelled',
            'return_request_submitted'
        ]
