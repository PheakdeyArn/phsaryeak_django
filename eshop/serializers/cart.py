from rest_framework import serializers

from eshop.models import Cart, Product, ProductVariant
from eshop.serializers import ProductSerializer


def get_value(model, product_id, key):
    objs = model.objects.filter(id=product_id).values()
    if len(objs) > 0:
        return objs[0].get(key)
    return None


class CartSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    short_description = serializers.SerializerMethodField()
    minimum_order_quantity = serializers.SerializerMethodField()
    quantity_step_size = serializers.SerializerMethodField()
    total_allowed_quantity = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    special_price = serializers.SerializerMethodField()
    product_variants = serializers.SerializerMethodField()
    product_details = serializers.SerializerMethodField()

    @staticmethod
    def get_name(obj):
        return get_value(Product, obj.product_variant_id, 'name')

    @staticmethod
    def get_image(obj):
        return get_value(Product, obj.product_variant_id, 'image')

    @staticmethod
    def get_short_description(obj):
        return get_value(Product, obj.product_variant_id, 'short_description')

    @staticmethod
    def get_minimum_order_quantity(obj):
        return get_value(Product, obj.product_variant_id, 'minimum_order_quantity')

    @staticmethod
    def get_quantity_step_size(obj):
        return get_value(Product, obj.product_variant_id, 'quantity_step_size')

    @staticmethod
    def get_total_allowed_quantity(obj):
        return get_value(Product, obj.product_variant_id, 'total_allowed_quantity')

    @staticmethod
    def get_price(obj):
        return get_value(ProductVariant, obj.product_variant_id, 'price')

    @staticmethod
    def get_special_price(obj):
        return get_value(ProductVariant, obj.product_variant_id, 'special_price')

    @staticmethod
    def get_product_variants(obj):
        return []

    def get_product_details(self, obj):
        product_qs = Product.objects.filter(productvariant=obj.product_variant_id)
        request = self.context.get('request')

        serializer = ProductSerializer(
            product_qs,
            many=True,
            context={
                'request': request
            }
        )
        return serializer.data

    class Meta:
        model = Cart
        fields = [
            'id',
            'user',
            'product_variant',
            'qty',
            'is_saved_for_later',
            'date_created',
            'name',
            'image',
            'short_description',
            'minimum_order_quantity',
            'quantity_step_size',
            'total_allowed_quantity',
            'price',
            'special_price',
            'product_variants',
            'product_details',
        ]
