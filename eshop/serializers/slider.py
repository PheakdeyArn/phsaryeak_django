from django.core.serializers import serialize
from rest_framework import serializers

from eshop.models import Slider, Categories, Product
from eshop.serializers import CategoriesSerializer, ProductSerializer

env = {
    'categories': Categories,
    'products': Product,
}


class SliderSerializer(serializers.ModelSerializer):
    data = serializers.SerializerMethodField()

    def get_data(self, obj):
        request = self.context.get("request")

        def custom_serializer(env, obj, _serializerClass):
            qs = env[obj.type].objects.filter(id=obj.type_id)
            serializer = _serializerClass(
                qs,
                many=True,
                context={'request': request}
            )
            return serializer.data

        if obj.type == 'categories':
            """
            Case: slider type is category
            """
            return custom_serializer(env, obj, CategoriesSerializer)

        elif obj.type == 'products':
            """
            Case: slider type is product
            """
            return custom_serializer(env, obj, ProductSerializer)
        else:
            """
            Case: Slider type is default
            """
            return []

    class Meta:
        model = Slider

        fields = [
            'id',
            'type',
            'type_id',
            'image',
            'date_added',
            'data',
        ]
