from rest_framework import serializers
from eshop.models import Transaction


class TransactionSerializer(serializers.ModelSerializer):

    username = serializers.SerializerMethodField()

    def get_username(self, obj):
        return obj.user.username

    class Meta:
        model = Transaction
        fields = [
            'id',
            'user',
            'username',
            'order',
            'transaction_type',
            'type',
            'txn_id',
            'payu_txn_id',
            'amount',
            'status',
            'currency_code',
            'payer_email',
            'message',
            'transaction_date',
            'date_created',
        ]
