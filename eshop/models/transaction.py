from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import User
from .order import Order


MAX_VALUE = 9e+11
MIN_VALUE = 0


class Transaction(models.Model):
    transaction_type = models.CharField(default='', max_length=16)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    type = models.CharField(blank=True, null=True, max_length=64)
    txn_id = models.CharField(blank=True, null=True, max_length=256)
    payu_txn_id = models.CharField(blank=True, null=True, max_length=512)
    amount = models.FloatField(default=0, validators=[
        MinValueValidator(MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    status = models.CharField(blank=True, null=True, max_length=12)
    currency_code = models.CharField(blank=True, null=True, max_length=5)
    payer_email = models.CharField(blank=True, null=True, max_length=64)
    message = models.CharField(default='', max_length=128)
    transaction_date = models.DateTimeField(
        blank=True, null=True, auto_now=True)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Transaction {id}'

    class Meta:
        ordering = ['id']
        db_table = 'transactions'
