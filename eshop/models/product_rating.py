from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from .product import Product

MAX_VALUE = 5
MIN_VALUE = 0


class ProductRating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    rating = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    images = models.TextField(blank=True, null=True)
    comment = models.CharField(blank=True, null=True, max_length=1024)
    data_added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'product rating {id}'

    class Meta:
        ordering = ['id']
        db_table = 'product_rating'
