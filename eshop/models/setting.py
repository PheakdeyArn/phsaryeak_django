from django.db import models


class Setting(models.Model):
    variable = models.CharField(max_length=512)
    value = models.TextField(default='')

    def __str__(self):
        return self.variable

    class Meta:
        ordering = ['id']
        db_table = 'settings'
