from django.db import models
from django.contrib.auth.models import User
from .group import Group


class UserGroup(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    def __str__(self):
        return f'User Group {id}'

    class Meta:
        ordering = ['id']
        db_table = 'users_groups'
