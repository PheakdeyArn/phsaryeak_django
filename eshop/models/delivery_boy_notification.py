from django.db import models


class DeliveryBoyNotification(models.Model):
    delivery_boy_id = models.IntegerField(default=0)
    order_id = models.IntegerField(default=0)
    title = models.TextField(default='')
    message = models.TextField(default='')
    type = models.CharField(max_length=56, default='')
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['id']
        db_table = 'delivery_boy_notifications'
        verbose_name = 'DeliveryBoyNotification'
        verbose_name_plural = 'DeliveryBoyNotifications'
