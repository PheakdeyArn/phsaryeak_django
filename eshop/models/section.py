from django.db import models


class Section(models.Model):
    title = models.CharField(default='', max_length=512)
    short_description = models.CharField(default='', max_length=512)
    style = models.CharField(blank=True, null=True, max_length=512)
    product_ids = models.CharField(default='', max_length=1024)
    row_order = models.IntegerField(default=0)
    categories = models.TextField(blank=True, null=True, )
    product_type = models.CharField(blank=True, null=True, max_length=1024)
    date_added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']
        db_table = 'sections'
