from django.db import models


class Theme(models.Model):
    name = models.CharField(max_length=50, default="classic")
    slug = models.CharField(max_length=20, default="classic")
    image = models.CharField(max_length=500, default="classic.jpg")
    status = models.IntegerField(default=1)
    created_on = models.DateTimeField(auto_now_add=True)
    is_default = models.IntegerField(default=1)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['created_on']
        db_table = 'themes'
