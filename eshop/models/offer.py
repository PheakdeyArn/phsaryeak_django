from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


MAX_VALUE = 9e+11
MIN_VALUE = 0


class Offer(models.Model):
    type = models.CharField(default='', max_length=16)
    type_id = models.IntegerField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    image = models.CharField(default='', max_length=16)
    date_added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.type

    class Meta:
        ordering = ['id']
        db_table = 'offers'
