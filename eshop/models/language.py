from django.db import models


class Language(models.Model):
    language = models.CharField(default='', max_length=11)
    code = models.CharField(default='', max_length=128)
    is_rtl = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.language

    class Meta:
        ordering = ['language']
        db_table = 'languages'
