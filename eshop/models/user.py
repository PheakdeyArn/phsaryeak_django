from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.core.validators import MinValueValidator, MaxValueValidator


MAX_INTEGER_VALUE = 9e+11
MIN_INTEGER_VALUE = 1


class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='email', max_length=60, unique=True, blank=True, null=True)
    username = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=255)
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    mobile = models.CharField("mobile", max_length=24, unique=True)
    image = models.CharField(max_length=500, blank=True, null=True)
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    activation_selector = models.CharField(max_length=30, blank=True, null=True)
    activation_code = models.CharField(max_length=100, blank=True, null=True)
    forgotten_password_selector = models.CharField(max_length=255, blank=True, null=True)
    forgotten_password_code = models.CharField(max_length=255, blank=True, null=True)
    forgotten_password_time = models.IntegerField(blank=True, null=True)
    remember_selector = models.CharField(max_length=255, blank=True, null=True)
    remember_code = models.CharField(max_length=255, blank=True, null=True)
    created_on = models.IntegerField()
    last_login = models.IntegerField(blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    company = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    bonus = models.IntegerField(blank=True, null=True)
    dob = models.CharField(max_length=16, blank=True, null=True)
    country_code = models.IntegerField("Country Code",
                                       validators=[MinValueValidator(MIN_INTEGER_VALUE),
                                                   MaxValueValidator(MAX_INTEGER_VALUE)],
                                       blank=True, null=True)
    area = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    street = models.CharField(max_length=100, blank=True, null=True)
    pincode = models.IntegerField("Pincode",
                                  validators=[MinValueValidator(MIN_INTEGER_VALUE),
                                              MaxValueValidator(MAX_INTEGER_VALUE)],
                                  blank=True, null=True)
    apikey = models.CharField(max_length=32, blank=True, null=True)
    referral_code = models.CharField(max_length=32, blank=True, null=True)
    friends_code = models.CharField(max_length=28, blank=True, null=True)
    fcm_id = models.TextField(blank=True, null=True)
    latitude = models.CharField(max_length=64, blank=True, null=True)
    longitude = models.CharField(max_length=64, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='created at', auto_now_add=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['username', 'mobile']

    def _str_(self):
        return self.username

    class Meta:
        ordering = ['id']
        db_table = 'users'
