from django.db import models
from django.contrib.auth.models import User

from eshop.models import ProductVariant


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product_variant = models.ForeignKey(ProductVariant, on_delete=models.CASCADE, default=1)
    qty = models.IntegerField(blank=False, default=0)
    is_saved_for_later = models.BooleanField(default=False, blank=False)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user.username} cart"

    class Meta:
        ordering = ['id']
        db_table = 'cart'
        verbose_name = 'cart'
        verbose_name_plural = 'carts'
