from django.db import models
from .category import Categories


class Product(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Categories, on_delete=models.CASCADE)
    row_order = models.IntegerField(default=0)
    stock_type = models.CharField(max_length=64, default='0', blank=True)
    tax = models.FloatField(default=0)
    type = models.CharField(max_length=20, default='', blank=True)
    name = models.CharField(max_length=100)
    short_description = models.CharField(max_length=500, default='', blank=True)
    slug = models.CharField(max_length=120, default='', blank=True)
    indicator = models.IntegerField(default=0)
    cod_allowed = models.IntegerField(default=1)
    minimum_order_quantity = models.IntegerField(default=1)
    quantity_step_size = models.IntegerField(default=1)
    total_allowed_quantity = models.IntegerField(default=0)
    is_returnable = models.IntegerField(default=0)
    is_cancelable = models.IntegerField(default=0)
    cancelable_till = models.IntegerField(default=0)
    image = models.CharField(max_length=500, default='', blank=True)
    other_images = models.CharField(max_length=500, default='', blank=True)
    video_type = models.CharField(max_length=20,  default='', blank=True)
    video = models.CharField(max_length=500,  default='', blank=True)
    tags = models.CharField(max_length=500,  default='', blank=True)
    warranty_period = models.CharField(max_length=100,  default='', blank=True)
    guarantee_period = models.CharField(max_length=100, default='', blank=True)
    made_in = models.CharField(max_length=128, default='', blank=True)
    sku = models.CharField(max_length=128, blank=True, null=True)
    stock = models.IntegerField(default=0)
    availability = models.IntegerField(default=0)
    rating = models.FloatField(default=0)
    no_of_ratings = models.IntegerField(default=0)
    description = models.CharField(max_length=500, default='', blank=True)
    status = models.IntegerField(default=1)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['date_added']
        db_table = 'products'
