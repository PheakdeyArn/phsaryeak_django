from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator


MAX_VALUE = 9e+11
MIN_VALUE = 0
MIN_ID_VALUE = 1

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    delivery_boy_id = models.IntegerField(validators=[MinValueValidator(
        MIN_ID_VALUE), MaxValueValidator(MAX_VALUE)])
    mobile = models.CharField(default='', max_length=11)
    total = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    delivery_charge = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    wallet_balance = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    total_payable = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    promo_code = models.CharField(blank=True, null=True, max_length=28)
    promo_discount = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    discount = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    final_total = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    payment_method = models.CharField(default='', max_length=16)
    latitude = models.CharField("Latitude", max_length=256,
                                blank=True, null=True)
    longitude = models.CharField("Longitude", max_length=256,
                                 blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    delivery_time = models.CharField(default='', max_length=128)
    delivery_date = models.DateField(blank=True, null=True)
    status = models.CharField(default='', max_length=11)
    active_status = models.CharField(default='', max_length=16)
    date_added = models.DateTimeField(auto_now=True)
    otp = models.IntegerField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])

    def __str__(self):
        return f'Order {self.id}'

    class Meta:
        ordering = ['id']
        db_table = 'orders'
        verbose_name = 'order'
        verbose_name_plural = 'orders'
