from django.db import models


class Faq(models.Model):
    question = models.TextField(null=True, blank=True)
    answer = models.TextField(null=True, blank=True)
    status = models.CharField(default='1', max_length=1)

    def __str__(self):
        return self.question or ''

    class Meta:
        ordering = ['id']
        db_table = 'faqs'
        verbose_name = 'Faq'
        verbose_name_plural = 'Faqs'
