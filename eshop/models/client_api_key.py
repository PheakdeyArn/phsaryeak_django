from django.db import models


class ClientApiKey(models.Model):
    name = models.TextField(null=True, blank=True)
    secret = models.TextField(default='')
    status = models.IntegerField(default=1)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['id']
        db_table = 'client_api_keys'
        verbose_name = 'ClientApiKey'
        verbose_name_plural = 'ClientApiKeys'
