from django.db import models
from .delivery_boy_notification import DeliveryBoyNotification


class FundTransfer(models.Model):
    delivery_boy_id = models.ForeignKey(
        DeliveryBoyNotification, on_delete=models.CASCADE)
    opening_balance = models.FloatField(default=0)
    closing_balance = models.FloatField(default=0)
    amount = models.FloatField(default=0)
    status = models.CharField(blank=True, null=True, max_length=28)
    message = models.CharField(blank=True, null=True, max_length=28)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.delivery_boy_id)

    class Meta:
        ordering = ['delivery_boy_id']
        db_table = 'fund_transfers'
        verbose_name = 'Fund Transfer'
        verbose_name_plural = 'Fund Transfers'
