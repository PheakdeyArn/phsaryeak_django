from eshop.models import ProductVariant
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from .order import Order


MAX_VALUE = 9e+11
MIN_VALUE = 0
MIN_ID_VALUE = 1


class OrderItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product_name = models.CharField(default='', max_length=512)
    variant_name = models.CharField(blank=True, null=True, max_length=256)
    product_variant = models.ForeignKey(
        ProductVariant, on_delete=models.CASCADE)
    quantity = models.IntegerField(validators=[MinValueValidator(
        MIN_ID_VALUE), MaxValueValidator(MAX_VALUE)])
    price = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    discounted_price = models.FloatField(blank=True, null=True, validators=[
        MinValueValidator(MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    tax_percent = models.FloatField(blank=True, null=True, validators=[
        MinValueValidator(MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    tax_amount = models.FloatField(blank=True, null=True, validators=[
        MinValueValidator(MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    discount = models.FloatField(blank=True, null=True,
                                 default=0, validators=[MinValueValidator(MIN_VALUE),
                                                        MaxValueValidator(MAX_VALUE)])
    sub_total = models.FloatField(default=0, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    deliver_by = models.CharField(blank=True, null=True, max_length=128)
    status = models.CharField(default='', max_length=1024)
    active_status = models.CharField(default='', max_length=16)
    date_added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'order item {id}'

    class Meta:
        ordering = ['id']
        db_table = 'order_items'
