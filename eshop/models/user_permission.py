from django.db import models
from django.contrib.auth.models import User, Group
from django.core.validators import MinValueValidator, MaxValueValidator


MAX_VALUE = 9e+11
MIN_VALUE = 1


class UserPermission(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.IntegerField(default=1, validators=[MinValueValidator(
        MIN_VALUE), MaxValueValidator(MAX_VALUE)])
    permissions = models.TextField(default='')
    created_by = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.permissions

    class Meta:
        ordering = ['id']
        db_table = 'user_permissions'
