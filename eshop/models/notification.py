from django.db import models


class Notification(models.Model):
    title = models.CharField(default='', max_length=128)
    message = models.CharField(default='', max_length=512)
    type = models.CharField(default='', max_length=12)
    type_id = models.IntegerField()
    image = models.CharField(blank=True, null=True, max_length=128)
    date_sent = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['id']
        db_table = 'notifications'
        verbose_name = 'notification'
        verbose_name_plural = 'notifications'
