from django.db import models

from eshop.models import Product


class ProductVariant(models.Model):
    attribute_value_ids = models.CharField(max_length=500, null=True)
    attribute_set = models.CharField(max_length=1024, null=True)
    price = models.FloatField(default=0)
    special_price = models.FloatField(default=0)
    sku = models.CharField(max_length=128, null=True)
    images = models.CharField(max_length=500, null=True)
    availability = models.IntegerField(null=True)
    stock = models.IntegerField(null=True)
    status = models.IntegerField(default=1)
    date_added = models.DateTimeField(auto_now=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='pv')

    def __str__(self):
        return str(self.price)

    class Meta:
        ordering = ['price']
        db_table = 'product_variants'
