from django.db import models
from .city import City


class Area(models.Model):
    name = models.CharField("Name", max_length=64, blank=False)
    city = models.ForeignKey(
        City, on_delete=models.CASCADE, blank=True, null=True)
    minimum_free_delivery_order_amount = models.FloatField(
        blank=False, default=100)
    delivery_charges = models.FloatField(blank=False, default=0)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        db_table = 'areas'
        verbose_name = 'area'
        verbose_name_plural = 'areas'
