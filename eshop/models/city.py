from django.db import models


class City(models.Model):
    name = models.TextField('name')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        db_table = 'cities'
        verbose_name = 'city'
        verbose_name_plural = 'cities'
