from .city import City
from .area import Area
from django.contrib import admin
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator


MAX_INTEGER_VALUE = 9e+11
MIN_INTEGER_VALUE = 1


class Address(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField("Name", max_length=64, blank=True, null=True)
    type = models.CharField("Type", max_length=32, blank=True, null=True)
    mobile = models.CharField("mobile", max_length=24, blank=True, null=True)
    alternate_mobile = models.CharField("Alternate Mobile", max_length=24, 
        blank=True, null=True)
    address = models.CharField("Address",max_length=512,blank=True,null=True)
    landmark = models.CharField("Landmark", max_length=128, blank=True, null=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    pincode = models.IntegerField("Pincode",
        validators=[MinValueValidator(MIN_INTEGER_VALUE), MaxValueValidator(MAX_INTEGER_VALUE)], 
        blank=True, null=True)
    country_code = models.IntegerField("Country Code",
        validators=[MinValueValidator(MIN_INTEGER_VALUE), MaxValueValidator(MAX_INTEGER_VALUE)],
        blank=True, null=True)
    state = models.CharField("State", max_length=64, blank=True, null=True)
    country = models.CharField("Country", max_length=128, blank=True, null=True)
    latitude = models.CharField("Latitude", max_length=64, blank=True, null=True)
    longitude = models.CharField("Longitude", max_length=64, blank=True, null=True)
    is_default = models.BooleanField("Is Default", default=False)
    
    def __str__(self) -> str:
        return self.name or ''

    class Meta:
        ordering = ['id']
        db_table = 'addresses'
        verbose_name = 'address'
        verbose_name_plural= 'addresses'
