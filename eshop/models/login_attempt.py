from django.db import models


class LoginAttempt(models.Model):
    ip_address = models.CharField(default='', max_length=45)
    login = models.CharField(default='', max_length=100)
    time = models.IntegerField(default=1)

    def __str__(self):
        return self.ip_address

    class Meta:
        ordering = ['ip_address']
        db_table = 'login_attempts'
