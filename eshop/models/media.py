from django.db import models


class Media(models.Model):
    title = models.TextField(default='')
    name = models.TextField(default='')
    extension = models.CharField(default='', max_length=16)
    type = models.CharField(default='', max_length=16)
    sub_directory = models.TextField(default='')
    size = models.TextField(default='')
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']
        db_table = 'media'
        verbose_name = 'media'
        verbose_name_plural = 'media'
