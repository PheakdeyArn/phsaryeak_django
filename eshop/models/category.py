from django.db import models


class Categories(models.Model):
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=20, null=True, blank=True)
    banner = models.CharField(max_length=500, null=True, blank=True)
    row_order = models.IntegerField(default=0)
    status = models.IntegerField(default=1)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children',
                               on_delete=models.CASCADE)
    image = models.CharField(max_length=500, default='')  # required field

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['row_order']
        db_table = 'categories'
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
