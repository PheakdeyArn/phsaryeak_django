from django.db import models


class Group(models.Model):
    name = models.CharField(default='', max_length=20)
    description = models.CharField(default='', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        db_table = 'groups'
