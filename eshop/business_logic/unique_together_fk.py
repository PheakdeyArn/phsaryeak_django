from django.utils.translation import gettext_lazy as _


def validate_category_name_parent(validation_error, objects, name, parent):
    if objects.filter(name=name, parent=parent).exists():
        raise validation_error({
            'name': _('Record with name and parent together already exists.'),
            'parent': _('Record with name and parent together already exists.'),
        }, code='unique_together')
