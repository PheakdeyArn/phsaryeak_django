from eshop.controllers.product_rating import ProductRatingViewSet
from eshop.serializers.product_rating import ProductRatingSerializer
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from eshop.controllers import ProductViewSet, ProductVariantViewSet, CategoriesViewSet, UserViewSet, SliderViewSet, ProductRatingViewSet
from eshop.controllers import (
    ProductViewSet,
    ProductVariantViewSet,
    CategoriesViewSet,
    UserViewSet,
    SliderViewSet,
    SectionViewSet,
    FavoriteViewSet,
    OfferViewSet,
    CartViewSet,
    AddressViewSet,
    MediaViewSet,
    OrderItemViewSet,
    OrderViewSet,
    CustomerViewSet,
    TransactionViewSet
)
from eshop.controllers import SettingsViewSet
from eshop.controllers.AddFavorite import AddFavoriteView
from eshop.controllers.RemoveFavorite import RemoveFavoriteView

router = DefaultRouter()
router.register(r'product', ProductViewSet)
router.register(r'product-variant', ProductVariantViewSet)
router.register(r'categories', CategoriesViewSet)
router.register(r'users', UserViewSet)
router.register(r'slider', SliderViewSet)
router.register(r'product-rating', ProductRatingViewSet)
router.register(r'section', SectionViewSet)
router.register(r'get_settings', SettingsViewSet)
router.register(r'favorite', FavoriteViewSet)
router.register(r'get_offer_images', OfferViewSet)
router.register(r'cart', CartViewSet)
router.register(r'address', AddressViewSet)
router.register(r'media', MediaViewSet)
router.register(r'order_item', OrderItemViewSet)
router.register(r'order', OrderViewSet)
router.register(r'customer', CustomerViewSet)
router.register(r'transaction', TransactionViewSet)


# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
    path('add_favorite/', AddFavoriteView.as_view()),
    path('remove_favorite/', RemoveFavoriteView.as_view()),
]
