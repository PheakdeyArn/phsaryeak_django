def get_md_sm_image(str_image):
    image_md = None
    image_sm = None
    
    if str_image is not None: 
        split_image = str(str_image).split('/', -1)[-1]

        sub_list = []
        list_dir = str_image.split('/')
        for i in range(len(list_dir) - 1):
            sub_list.append(list_dir[i])
        my_str = '/'.join([str(item) for item in sub_list])

        dir_sm = 'thumd-sm'
        dir_md = 'thumd-md'
        http = 'http://34.126.158.141:8800/'
        image_sm = http + my_str + '/' + dir_sm + '/' + split_image
        image_md = http + my_str + '/' + dir_md + '/' + split_image
    return image_md, image_sm


def get_favorite(list_fav, product_id):
    is_favorite = 0
    for i in list_fav:
        if product_id == i:
            is_favorite = "1"
            break
        else:
            is_favorite = "0"
            break
    return is_favorite


def get_is_purchase(list_product_order, product_id):
    is_purchase = False
    for i in list_product_order:
        if product_id == i:
            is_purchase = True
            break
        else:
            is_purchase = False
            continue
    return is_purchase


def get_min_max_price(price, special_price):
    min_price = min(price, default=0)
    max_price = max(price, default=0)
    min_special_price = min(special_price, default=0)
    max_special_price = max(special_price, default=0)
    percentage = 0 if min_special_price == 0 else (min_special_price * 100) / min_price
    
    return min_price, max_price, min_special_price, max_special_price, percentage


def get_is_return_null(list, qs, item):
    if len(list) > 0:
        return qs[0].get(item)
    return None


def get_value(qs, key):
    if len(qs) > 0:
        return qs[0].get(key)
    return None
