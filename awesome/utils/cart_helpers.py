from eshop.models import ProductVariant, Setting
import json


def get_system_setting_value(key):
    system_setting = Setting.objects.filter(variable='system_settings').values()
    system_setting_value = system_setting[0].get('value')
    json_system_setting = json.loads(system_setting_value)
    return json_system_setting[key]


def get_delivery_charge(sub_total, delivery_charge_amount, free_amount):
    """
    Delivery charge is free when sub total >= free amount
    """
    return delivery_charge_amount if sub_total < free_amount else 0


def get_values(qs_value, key):
    return [x.get(key) for x in qs_value]


def get_qs_value(qs, key):
    if len(qs) > 0:
        return qs[0].get(key)
    return None


def get_tax_percentage():
    return 0


def get_tax_amount(sub_total, tax_percent):
    return (sub_total * tax_percent) / 100


def get_bool_is_save_later(is_save_later):
    return is_save_later in ['true', 1, '1', True]


def get_total_quantity(qs_value):
    return sum([int(x.get('qty')) for x in qs_value])


def get_cart_total_price(qs_value):
    list_total_price = []
    for x in qs_value:
        qty = x.get('qty')
        variant_id_value = ProductVariant.objects.filter(id=x.get('product_variant_id')).values()
        sp = int(variant_id_value[0].get('special_price'))
        list_total_price.append(qty*sp)

    return sum(list_total_price)


def is_maximum_cart(total_qty, max_cart):
    return total_qty > max_cart


def is_out_of_stock(stock, update_qty):
    return stock is None or update_qty > stock


def is_variant_available(out_of_stock, is_maximum):
    return not bool(out_of_stock or is_maximum)
